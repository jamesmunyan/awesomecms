var $ = jQuery;

function addContact(otype,id) {
	$('#vendorAssPop').empty();
	$('#addContactInfo').remove();	
	jQuery('#loader_block').slideDown('fast',function() {
		$.ajax({
			type:'GET',
			url:'/admin/contacts/add?otype='+otype+'&oid='+id,
			success:function(code) {
				if(code == '0') {
					jAlert('The Contact can not be found. Please try again','Error',function() {
						jQuery('#loader_block').slideUp('fast');	
					});
				}else {					
					jQuery('#loader_block').slideUp('fast',function() {
						$('#vendorAssPop').html(code);
					});
				}
			}
		});
	});
}



function addVendorContact(id) { 
	$('#association').remove();
	$('#loader_block').slideDown('fast',function() {
		$.ajax({
			type:'GET',
			url:'/admin/clients/associate_vendors_form?cid='+id,
			success:function(data) {
				if(data) {
					$('#loader_block').slideUp('fast',function() {
						$('#vendorAssPop').html(data);
					});
				}else {
					jAlert('There was a problem finding the Client requested. Please try again.','Error',function() {
						$('#loader_block').slideUp('fast');
					});
				}
			}
		});
	});
}

function editClientContact(id) {
	$('#addContactInfo').remove();
	$('#editContactInfo').remove();
	$('#viewContactInfo').remove();

	jQuery('#loader_block').slideDown('fast',function() {
		$.ajax({
			type:'GET',
			url:'/admin/contacts/edit?did='+id,
			success:function(code) {
				if(code == '0') {
					jAlert('The Contact can not be found. Please try again','Error',function() {
						jQuery('#loader_block').slideUp('fast');	
					});
				}else {
					jQuery('#loader_block').slideUp('fast',function() {
						$('#editContactPop').html(code);
					});
				}
			}
		});
	});

}

function viewVendorContact(id,type) {
	$('#addContactInfo').remove();
	$('#editContactInfo').remove();
	$('#viewContactInfo').remove();

	jQuery('#loader_block').slideDown('fast',function() {
		$.ajax({
			type:'GET',
			url:'/admin/clients/view_contact?did='+id,
			success:function(code) {
				if (code == '0') {
					jAlert('The Contact can not be found. Please try again','Error',function() {
						jQuery('#loader_block').slideUp('fast');
					});
				}else {
					jQuery('#loader_block').slideUp('fast',function() {
						$('#editContactPop').html(code);
					});
				}
			}
		});
	});
}



