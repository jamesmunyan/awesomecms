function editUserAvatar(id) {
	if(jQuery('#fileInput') != '') {
		jQuery("#editUserCustomAvatar").dialog({
			autoOpen: true,
			modal: true,
			buttons: {
				Upload: function() {
					jQuery('#uploadAvatar').submit();
				}
			}
		});
	}else {
		jAlert('Please select a file on your machine to upload.','Error');	
	}
}
$("#uploadAvatar").validationEngine({promptPosition : "right", scroll: true});
//add styling to the custom popup
jQuery("input:file").uniform();