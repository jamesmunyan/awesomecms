<?php if(isset($web_id)) { ?>
<div class="hor_nav nav">
    <ul>
        <li><a href="javascript:void(0);">Visitors</a>
            <ul class="fallback">
                <li><a href="<?= base_url(); ?>reports/analytics/visitors?web_id=<?= $web_id; ?>&r=visitors<?=((isset($_GET['to'])) ? '&to=' . $_GET['to'] : ''); ?><?= ((isset($_GET['from'])) ? '&from=' . $_GET['from'] : ''); ?>">Visitors</a></li>
                <li><a href="<?= base_url(); ?>reports/analytics/traffic_sources?web_id=<?= $web_id; ?>&r=traffic_sources<?=((isset($_GET['to'])) ? '&to=' . $_GET['to'] : ''); ?><?= ((isset($_GET['from'])) ? '&from=' . $_GET['from'] : ''); ?>">Traffic Sources</a></li>
                <li><a href="<?= base_url(); ?>reports/analytics/bounce_rate?web_id=<?= $web_id; ?>&r=bounce_rate<?=((isset($_GET['to'])) ? '&to=' . $_GET['to'] : ''); ?><?= ((isset($_GET['from'])) ? '&from=' . $_GET['from'] : ''); ?>">Bounce Rate</a></li>
            </ul>
        </li>
        <li><a href="javascript:void(0);">Pages</a>
                <ul class="fallback">
                    <li><a href="<?= base_url(); ?>reports/analytics/page_views?web_id=<?= $web_id; ?>&r=page_views<?=((isset($_GET['to'])) ? '&to=' . $_GET['to'] : ''); ?><?= ((isset($_GET['from'])) ? '&from=' . $_GET['from'] : ''); ?>">Page Views</a></li>
                    <li><a href="<?= base_url(); ?>reports/analytics/average_time_on_page?web_id=<?= $web_id; ?>&r=average_time_on_page<?=((isset($_GET['to'])) ? '&to=' . $_GET['to'] : ''); ?><?= ((isset($_GET['from'])) ? '&from=' . $_GET['from'] : ''); ?>">Average Time On Page</a></li>
                    <li><a href="<?= base_url(); ?>reports/analytics/entry_pages?web_id=<?= $web_id; ?>&r=entry_pages<?=((isset($_GET['to'])) ? '&to=' . $_GET['to'] : ''); ?><?= ((isset($_GET['from'])) ? '&from=' . $_GET['from'] : ''); ?>">Entry Pages</a></li>
                    <li><a href="<?= base_url(); ?>reports/analytics/exit_pages?web_id=<?= $web_id; ?>&r=exit_pages<?=((isset($_GET['to'])) ? '&to=' . $_GET['to'] : ''); ?><?= ((isset($_GET['from'])) ? '&from=' . $_GET['from'] : ''); ?>">Exit Pages</a></li>
                </ul>
        </li>
        <li><a href="javascript:void(0);">Browser/Device</a>
            <ul class="fallback">
                <li><a href="<?= base_url(); ?>reports/analytics/browser?web_id=<?= $web_id; ?>&r=browser<?= ((isset($_GET['to'])) ? '&to=' . $_GET['to'] : ''); ?><?= ((isset($_GET['from'])) ? '&from=' . $_GET['from'] : ''); ?>">Browser</a></li>
                <li><a href="<?= base_url(); ?>reports/analytics/operating_system?web_id=<?= $web_id; ?>&r=operating_system<?= ((isset($_GET['to'])) ? '&to=' . $_GET['to'] : ''); ?><?= ((isset($_GET['from'])) ? '&from=' . $_GET['from'] : ''); ?>">Operating System</a></li>
                <li><a href="<?= base_url(); ?>reports/analytics/mobile_devices?web_id=<?= $web_id; ?>&r=mobile_devices<?= ((isset($_GET['to'])) ? '&to=' . $_GET['to'] : ''); ?><?= ((isset($_GET['from'])) ? '&from=' . $_GET['from'] : ''); ?>">Mobile Devices</a></li>
            </ul>
        </li>
        <!--
        <li><a href="#">Custom Reports</a>
                <ul class="fallback">
                    <li><a href="#">View Existing</a></li>
                    <li><a href="#">Create Custom Report</a></li>
                </ul>
        </li>
        -->
    </ul>
    <div class="fix"></div>
</div>
<script type="text/javascript">
	var $ = jQuery;
	$('.nav li ul').hide().removeClass('fallback');
	$('.nav li').hover(
		function () {
			$(this).addClass('active');
			$('ul', this).stop().slideDown(100);
		},
		function () {
			$(this).removeClass('active');
			$('ul', this).stop().slideUp(100);
		}
	);
</script>
<?php } ?>