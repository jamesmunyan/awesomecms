<div class="content hideTagFilter">
    <div class="title">
    	<h5>Heat Mapping</h5>
        <style type="text/css">
			div#uniform-switcher,#uniform-group_switch{display:none !important;}
			div#group_switch_chzn{width:200px;margin-top:5px;float:left;}
        </style>
        <?php if(count($websites) > 1) { ?>
        <div class="websiteSwitch">
        	<select name="websiteSwitch" class="chzn-select" id="switcher" placeholder="Switch Website">
            	<option value="">Select a Website</option>
                <?php foreach($websites as $website) : ?><option <?= (($web_id == $website->WEB_ID) ? 'selected="selected"' : ''); ?> value="<?= $website->WEB_ID; ?>"><?= str_replace('http://','',$website->WEB_Url); ?></option><? endforeach; ?>
            </select>
            <div class="fix"></div>
        </div>
        <?php } ?>
    </div>
    <?php notifyError(); ?>
    <?php include FCPATH . 'html/global/breadcrumb.php'; ?>
    <div id="analytic_reports">
    	<div id="top" class="widget full" style="margin-top:5px">
        	<div class="head">
        		<h5 class="iChart7">Click Mapping</h5>
                <div class="groupSwitcher">
                	<select name="groupSwitch" class="chzn-select" id="group_switch" placeholder="Switch Page" style="margin-top:5px;width:200px;float:right;">
                    	<option value="">Select a Page</option>
                        <?php foreach($groups as $group) : ?><option <?= (($group->PATH == '/') ? 'selected="selected"' : ''); ?> value="<?= $group->PATH; ?>"><?= str_replace('http://','',$activeSiteURL); ?><?= $group->PATH; ?></option><?php endforeach; ?>
                    </select>
            		<label style="margin-left:10px;margin-top:10px;float:left;">* Only pages with activity are viewable</label>
                </div>
            </div>
            <div id="heatmap" style="position:relative;">
                <div id="heatmaps"></div>
            </div>
        </div>
        <span class="formNote">Activity for the selected date range is displayed over currently hosted pages. Past pages are not viewable.</span>

    </div>
    <div class="fix"></div>
</div>
<div class="fix"></div>
<script type="text/javascript">
	var $ = jQuery;
	
	var width = $('#heatmap').width();
	var height = $('#heatmap').height();
	
	$('#switcher').change(function() {
		var web_id = $(this).val();
		$.ajax({
			type:'GET',
			url:'<?= base_url(); ?>heatmap/generate?w='+width+'&h='+height+'&web_id='+web_id+'&p=/',
			success:function(data) {
				$('#heatmaps').html(data);
				$('#heatmaps').slideDown('fast');	
			}
		});
	});
	
	$('#group_switch').change(function() {
		var page = $(this).val();
		$.ajax({
			type:'GET',
			url:'<?= base_url(); ?>heatmap/generate?w='+width+'&h='+height+'&p='+page+'&web_id=<?= $web_id; ?>',
			success:function(data) {
				$('#heatmaps').slideUp('fast',function() {
					$('#heatmaps').html(data);
					$('#heatmaps').slideDown('fast');
				});
			}
		});
	});
	$( ".mydatepicker" ).datepicker({ 
		defaultDate: +7,
		autoSize: true,
		dateFormat: 'mm/dd/yy',
	});	
	
	$.ajax({
		type:'GET',
		url:'<?= base_url(); ?>heatmap/generate?w='+width+'&h='+height+'&web_id=<?= $web_id; ?>',
		success:function(data){
			if(data) {
				$('#heatmaps').slideUp('fast',function() {
					$('#heatmaps').html(data);
					$('#heatmaps').slideDown('fast');	
				});	
			}
		}
	});
</script>