<div class="content hideTagFilter">
    <div class="title">
    	<h5>Heatmapping</h5>
        <style type="text/css">div#uniform-switcher{display:none !important;}</style>
    </div>
    <?php notifyError(); ?>
    <?php include FCPATH . 'html/global/breadcrumb.php'; ?>
    <div id="analytic_reports">
    	<div id="top" class="widget full" style="margin-top:5px">
        	<div class="head">
        		<h5 class="iChart7">Heatmapping</h5>
            </div>
            <div id="heatmap" style="position:relative;;">
                <div id="heatmaps">
                	<?php print_object($websites); ?>
                	<div class="nNote nFailure" style="margin:0;">
                    	<p style=" font-size:12px;font-family:Arial, Helvetica, sans-serif;"><strong>No Heatmaping Data Found:</strong> We're sorry, but the clients website that is selected currently has no heatmapping data in our system. Please pick another website or contact us for any questions.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="fix"></div>
</div>
<div class="fix"></div>