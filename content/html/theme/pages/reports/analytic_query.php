<div id="analytic_reports_query"></div>
<script type="text/javascript" language="javascript">
	var $ = jQuery;
	var innerHTML = '';
	oo.setOOId("<?= ReturnOOId(); ?>");
	oo.load(function() {
		<?php if(isset($report)) { ?>
			<?php foreach($report->Reports as $metricDimension) {
						//This is the custom report...only shows an icon. not really a report. It links to a reporting page with more advanced reporting
						if($metricDimension->REPORT_Type == 'Dummy') { ?>
							//create our html for the report
							var dummyHTML = '<div class="stats dummy <?= $metricDimension->REPORT_ClassName; ?>" id="<?= $metricDimension->REPORT_ContainerID; ?>">';
								dummyHTML +=	'<a href="#" class="dummyReportIcon">&nbsp;</a><span>Custom Reports</span>';
								dummyHTML +=	'<div class="fix"></div>';
								dummyHTML += '</div>';
								
							$('#analytic_reports_query').append(dummyHTML);
						<?php } 
						
						//this will take care of any Timeline chart
						if($metricDimension->REPORT_Type == 'Timeline') { ?>
							var timelineHTML = '<div class="widget <?= $metricDimension->REPORT_ClassName; ?>">';
								timelineHTML +=		'<div class="head">';
								timelineHTML += 		'<h5 class="<?= $metricDimension->REPORT_IconClass; ?>"><?= $metricDimension->REPORT_Title; ?></h5>';
								timelineHTML +=			'<div class="timePicker">';
								timelineHTML += 			'<input type="text" class="<?= $metricDimension->REPORT_ContainerID; ?>datepicker from" />';
								timelineHTML +=				'<span>to</span>';
								timelineHTML += 			'<input type="text" class="<?= $metricDimension->REPORT_ContainerID; ?>datepicker to" />';
								timelineHTML += 			'<div class="fix"></div>';
								timelineHTML +=			'</div>';
								timelineHTML +=		'</div>';
								timelineHTML +=		'<div class="google_chart" id="<?= $metricDimension->REPORT_ContainerID; ?>"></div>';
								timelineHTML +=		'<div class="fix"></div>';
								timelineHTML +=	'</div>';
												
							$('#analytic_reports_query').append(timelineHTML);
												
							//Create a new timeline (aid, startDate, endDate)
							var tl = new oo.Timeline("<?= $report->AnalyticsProfileID; ?>",new Date("<?= $metricDimension->REPORT_StartDate; ?>"), new Date("<?= $metricDimension->REPORT_EndDate; ?>"));
							
							<?php if(isset($metricDimension->REPORT_Metric) AND count($metricDimension->REPORT_Metric) > 0) { ?>
								<?php foreach($metricDimension->REPORT_Metric as $metric) { ?>
								//Add the metric to pull from the visitor count
								tl.addMetric('<?= $metric->Code; ?>', '<?= $metric->Label; ?>');
								<?php } ?>
							<?php } ?>
							
							
							<?php if(isset($metricDimension->REPORT_Options)) { ?>
								//Set Google visualization options for line colors
								tl.setOption(<?= $metricDimension->REPORT_Options; ?>);
							<?php } ?>
							//Set Google visualization option for chart title
							tl.setOption('title', '<?= $metricDimension->REPORT_Title; ?>');
							//draw in the div element with id 'timeline'
							tl.draw('<?= $metricDimension->REPORT_ContainerID; ?>');
								
							$( ".<?= $metricDimension->REPORT_ContainerID; ?>datepicker" ).datepicker({ 
								defaultDate: +7,
								autoSize: true,
								dateFormat: 'mm-dd-yy',
							});	
							
							$('.<?=$metricDimension->REPORT_ContainerID; ?>datepicker').change(function() {
								redrawChart(divid,from,to,type)
							});

					 <?php }//endif ?>
					 
				 //This is for any basic metric report. 
				 <?php if($metricDimension->REPORT_Type == 'Metric') { ?>
				 
				 		var MetricHTML = '<div class="stats <?= $metricDimension->REPORT_ClassName; ?>">';
							MetricHTML +=	'<ul>';
							MetricHTML +=		'<li>';
							MetricHTML +=			'<a href="#" class="count grey" id="<?= $metricDimension->REPORT_ContainerID; ?>"></a>';
							MetricHTML +=			'<span class="text"><?= $metricDimension->REPORT_Title; ?></span>';
							MetricHTML +=			'<span class="image">&nbsp;</span>';
							MetricHTML +=		'</li>';
							MetricHTML +=	'</ul>';
							MetricHTML += '</div>';
							
						$('#analytic_reports_query').append(MetricHTML);
				 		
						var m = new oo.Metric("<?= $report->AnalyticsProfileID; ?>", new Date('<?= $metricDimension->REPORT_StartDate; ?>'), new Date('<?= $metricDimension->REPORT_EndDate; ?>'));
						<?php if(isset($metricDimension->REPORT_Metric) AND count($metricDimension->REPORT_Metric) > 0) { ?>
							<?php foreach($metricDimension->REPORT_Metric as $metric) { ?>
								<?php if(isset($metric->Code)) { ?>
									m.setMetric('<?= $metric->Code; ?>');
								<?php } ?>
							<?php } ?>
						<?php } ?>
						m.draw('<?= $metricDimension->REPORT_ContainerID; ?>');
				<?php } ?>
				 
				 //This is for any basic metric report. 
				 <?php if($metricDimension->REPORT_Type == 'Query') { ?>
				 
				 	var QueryHTML = '<div class="stats <?= $metricDimension->REPORT_ClassName; ?>">';
						QueryHTML +=	'<ul>';
						QueryHTML +=		'<li>';
						QueryHTML +=			'<a href="#" class="count grey" id="<?= $metricDimension->REPORT_ContainerID; ?>"></a>';
						QueryHTML +=			'<span class="text"><?= $metricDimension->REPORT_Title; ?></span>';
						QueryHTML +=			'<span class="image">&nbsp;</span>';
						QueryHTML +=		'</li>';
						QueryHTML +=	'</ul>';
						QueryHTML +=	'<div class="fix"></div>';
						QueryHTML += '</div>';
						
					$('#analytic_reports').append(QueryHTML);
				 
					var m = new oo.Query("<?= $report->AnalyticsProfileID; ?>", new Date('<?= $metricDimension->REPORT_StartDate; ?>'), new Date('<?= $metricDimension->REPORT_EndDate; ?>'));
					var time = false;
					<?php if(isset($metricDimension->REPORT_Metric) AND count($metricDimension->REPORT_Metric) > 0) { ?>
						<?php foreach($metricDimension->REPORT_Metric as $metric) { ?>
							<?php if(isset($metric->Code)) { ?>
								<?php if($metric->Code == 'ga:avgTimeOnSite') { ?>
									time = true;
								<?php } ?>
								m.addMetric('<?= $metric->Code; ?>');
							<?php } ?>
						<?php } ?>
					<?php } ?>
					
					<?php if(isset($metricDimension->REPORT_Dimension) AND count($metricDimension->REPORT_Dimension) > 0) { ?>
						<?php foreach($metricDimension->REPORT_Dimension as $dimension) { ?>
							<?php if(isset($dimension->Code)) { ?>
								m.addDimension('<?= $dimension->Code; ?>');
							<?php } ?>
						<?php } ?>
					<?php } ?>
						m.execute(function(data) {
							var num = ((time == false) ? Math.round(data) : getTime(data));
							$('#<?= $metricDimension->REPORT_ContainerID; ?>').text(num);
						});
				<?php } ?>
				
				//write out pie chart reports
				<?php if($metricDimension->REPORT_Type == 'StaticPie') { ?>
					
					var PieHTML = '<div class="stats staticpie <?= $metricDimension->REPORT_ClassName; ?>">';
						PieHTML +=		'<div class="pieImage"><img src="<?= base_url(); ?>imgs/<?= ((isset($metricDimension->REPORT_Options)) ? $metricDimension->REPORT_Options : 'pie02.png'); ?>" alt="Pie Chart" /></div>';
						PieHTML +=		'<span class="text"><?= $metricDimension->REPORT_Title; ?></span>';
						PieHTML +=		'<div class="fix"></div>';
						PieHTML += '</div>';
						
					$('#analytic_reports_query').append(PieHTML);
					
				<?php } ?>
				
				//write out pie chart reports
				<?php if($metricDimension->REPORT_Type == 'Custom') { ?>
					
					var PieHTML = '<div class="stats custom <?= $metricDimension->REPORT_ClassName; ?>">';
						PieHTML +=		'<div class="pieImage"><img src="<?= base_url(); ?>imgs/<?= ((isset($metricDimension->REPORT_Options)) ? $metricDimension->REPORT_Options : 'pie02.png'); ?>" alt="Pie Chart" /></div>';
						PieHTML +=		'<span class="text"><?= $metricDimension->REPORT_Title; ?></span>';
						PieHTML +=		'<div class="fix"></div>';
						PieHTML += '</div>';
						
					$('#analytic_reports_query').append(PieHTML);
					
				<?php } ?>
				
				//write out pie chart reports
				<?php if($metricDimension->REPORT_Type == 'Pie') { ?>
					
					var PieHTML = '<div class="stats pie <?= $metricDimension->REPORT_ClassName; ?>">';
						PieHTML +=		'<div class="google_chart" id="<?= $metricDimension->REPORT_ContainerID; ?>"></div>';
						PieHTML +=		'<div class="myWhiteout">&nbsp;</div>';
						PieHTML +=		'<span class="text"><?= $metricDimension->REPORT_Title; ?></span>';
						PieHTML +=		'<div class="fix"></div>';
						PieHTML += '</div>';
						
					$('#analytic_reports_query').append(PieHTML);
					
					var p = new oo.Pie("<?= $report->AnalyticsProfileID; ?>", new Date('<?= $metricDimension->REPORT_StartDate; ?>'), new Date('<?= $metricDimension->REPORT_EndDate; ?>'));
					
					<?php if(isset($metricDimension->REPORT_Metric) AND !empty($metricDimension->REPORT_Metric)) { ?>
						<?php foreach($metricDimension->REPORT_Metric as $metric) { ?>
							<?php if(isset($metric->Code)) { ?>
								p.setMetric('<?= $metric->Code; ?>','<?= $metric->Label; ?>');
							<?php } ?>
						<?php } ?>
					<?php } ?>
					
					<?php if(isset($metricDimension->REPORT_Dimension) AND count($metricDimension->REPORT_Dimension) > 0) { ?>
						<?php foreach($metricDimension->REPORT_Dimension as $dimension) { ?>
							<?php if(isset($dimension->Code)) { ?>
								p.setDimension('<?= $dimension->Code; ?>');
							<?php } ?>
						<?php } ?>
					<?php } ?>
					
					p.draw('<?= $metricDimension->REPORT_ContainerID; ?>');
				<?php } ?>
				
				//write out table reports
				<?php if($metricDimension->REPORT_Type == 'Table') { ?>
				
					var TableHTML = '<div class="stats table <?= $metricDimension->REPORT_ClassName; ?>">';
						TableHTML +=	'<div class="google_chart" id="<?= $metricDimension->REPORT_ContainerID; ?>"></div>';
						TableHTML +=	'<div class="fix"></div>';
						TableHTML += '</div>';
						
					$('#analytic_reports').append(TableHTML);
				
					var ta = new oo.Table("<?= $report->AnalyticsProfileID; ?>", new Date('<?= $metricDimension->REPORT_StartDate; ?>'), new Date('<?= $metricDimension->REPORT_EndDate; ?>')); 
					<?php if(isset($metricDimension->REPORT_Metric) AND count($metricDimension->REPORT_Metric) > 0) { ?>
						<?php foreach($metricDimension->REPORT_Metric as $metric) { ?>
							<?php if(isset($metric->Code)) { ?>
								ta.addMetric('<?= $metric->Code; ?>','<?= $metric->Label; ?>');
							<?php } ?>
						<?php } ?>
					<?php } ?>
					
					<?php if(isset($metricDimension->REPORT_Dimension) AND count($metricDimension->REPORT_Dimension) > 0) { ?>
						<?php foreach($metricDimension->REPORT_Dimension as $dimension) { ?>
							<?php if(isset($dimension->Code)) { ?>
								ta.addDimension('<?= $dimension->Code; ?>');
							<?php } ?>
						<?php } ?>
					<?php } ?>
					
					ta.setOption('page','enable');
					ta.setOption('pageSize', '<?= $metricDimension->REPORT_Options; ?>');
					
					ta.draw('<?= $metricDimension->REPORT_ContainerID; ?>');
				<?php } ?>
				 
			<?php }//end ->Reports foreach ?>
			$('div#analytic_reports_query').find('div.widget:first-child').addClass('removeMargin');
			$('div#analytic_reports_query').find('div.widget:first-child').next().addClass('noborder');
			//$('div#analytic_reports').find('div.quarter:first-child').addClass('noborder');

		<?php }else { ?>
			jQuery('#analytic_reports_query').append('<div id="error">There was an error pulling the analytic data for this website. Please try again</div>');
		<?php } ?>
	});
	
	function getTime(time) {
		var minutes = Math.floor(time / 60);
		var seconds = time - minutes * 60;
			seconds = Math.ceil(seconds / 10) * 10;
		var hours = Math.floor(time / 3600);
		return minutes + ':' + seconds;
	}
</script>
