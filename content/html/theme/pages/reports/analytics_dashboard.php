<div class="content hideTagFilter">
    <div class="title">
    	<h5>Analytics</h5>
        <style type="text/css">div#uniform-switcher{display:none !important;}</style>
        <?php if(count($websites) > 1) { ?>
        <div class="websiteSwitch">
        	<select name="websiteSwitch" class="chzn-select" id="switcher" placeholder="Switch Website">
            	<option value=""></option>
                <?php foreach($websites as $website) : ?><option <?= (($report->WebID == $website->WebID) ? 'selected="selected"' : ''); ?> value="<?= $website->WebID; ?>"><?= str_replace('http://','',$website->URL); ?></option><? endforeach; ?>
            </select>
            <div class="fix"></div>
        </div>
        <?php } ?>
    </div>
    <?php notifyError(); ?>
    <?php include FCPATH . 'html/global/breadcrumb.php'; ?>
    <?php include FCPATH . 'html/theme/incl/analytics_nav.php'; ?>
    <div id="analytic_reports">
    </div>
    <div id="modules">
    	<table cellspacing="20" cellpadding="0" width="100%;">
        	<tr class="row_one">
            	<td id="visitors_td" style="width:25%;">
                	
                </td>
                <td id="traffic_sources_td" style="width:25%;">
                
                </td>
                <td id="bounce_rate_td" style="width:25%;">
                
                </td>
                <td id="page_views_td" style="width:25%;">
                
                </td>
            </tr>
        	<tr class="row_two">
            	<td id="average_time_on_page_td" style="width:25%;"></td>
                <td id="entry_pages_td" style="width:25%;"></td>
                <td id="exit_pages_td" style="width:25%;"></td>
                <td id="browser_td" style="width:25%;"></td>
            </tr>
        	<tr class="row_three">
            	<td id="operating_system_td" style="width:25%;"></td>
                <td id="mobile_devices_td" style="width:25%;"></td>
                <td style="width:25%;border-left:0;"></td>
                <td style="width:25%;border-left:0;"></td>
            </tr>
        </table>
    </div>
    <div class="fix"></div>
</div>
<div class="fix"></div>
<script type="text/javascript" language="javascript">
	var $ = jQuery;
	
	$('#switcher').change(function() {
		var web_id = $(this).val();
		document.location.href = '<?= base_url(); ?>reports/analytics?web_id='+web_id;
	});
	
	var innerHTML = '';
	oo.setOOId("<?= ReturnOOId(); ?>");
	oo.load(function() {
		<?php if(isset($report)) { ?>
			<?php foreach($report->Reports as $metricDimension) {
						//This is the custom report...only shows an icon. not really a report. It links to a reporting page with more advanced reporting
						if($metricDimension->REPORT_Type == 'Dummy') { ?>
							//create our html for the report
							var dummyHTML =  '<div class="stats dummy <?= $metricDimension->REPORT_ClassName; ?>" id="<?= $metricDimension->REPORT_ContainerID; ?>">';
								dummyHTML +=	'<a href="#" class="dummyReportIcon">&nbsp;</a><span>Custom Reports</span>';
								dummyHTML +=	'<div class="fix"></div>';
								dummyHTML += '</div>';
							$('#analytic_reports').append(dummyHTML);
						<?php } 
						
						//this will take care of any Timeline chart
						if($metricDimension->REPORT_Type == 'Timeline') { ?>
							var timelineHTML = '<div class="widget <?= $metricDimension->REPORT_ClassName; ?>">';
								timelineHTML +=		'<div class="head">';
								timelineHTML += 		'<h5 class="<?= $metricDimension->REPORT_IconClass; ?>"><?= $metricDimension->REPORT_Title; ?></h5>';
								timelineHTML +=			'<div class="timePicker">';
								timelineHTML += 			'<input type="text" class="<?= $metricDimension->REPORT_ContainerID; ?>datepicker from" value="<?= ((isset($_GET['from'])) ? $_GET['from'] : date('m/d/Y',strtotime(getOneMonthAgo()))); ?>" />';
								timelineHTML +=				'<span>to</span>';
								timelineHTML += 			'<input type="text" class="<?= $metricDimension->REPORT_ContainerID; ?>datepicker to" value="<?= ((isset($_GET['to'])) ? $_GET['to'] : date('m/d/Y')); ?>" />';
								timelineHTML +=				'<a class="button greenBtn changeDate" href="javascript:changeDate();">Change Date</a>';
								timelineHTML += 			'<div class="fix"></div>';
								timelineHTML +=			'</div>';
								timelineHTML +=		'</div>';
								timelineHTML +=		'<div class="google_chart" id="<?= $metricDimension->REPORT_ContainerID; ?>"></div>';
								timelineHTML +=		'<div class="fix"></div>';
								timelineHTML +=	'</div>';
												
							$('#analytic_reports').append(timelineHTML);
												
							//Create a new timeline (aid, startDate, endDate)
							var tl = new oo.Timeline("<?= $report->AnalyticsProfileID; ?>",new Date("<?= ((isset($_GET['from'])) ? $_GET['from'] : $metricDimension->REPORT_StartDate); ?>"), new Date("<?= ((isset($_GET['to'])) ? $_GET['to'] : $metricDimension->REPORT_EndDate); ?>"));
							
							<?php if(isset($metricDimension->REPORT_Metric) AND count($metricDimension->REPORT_Metric) > 0) { ?>
								<?php foreach($metricDimension->REPORT_Metric as $metric) { ?>
								//Add the metric to pull from the visitor count
								tl.addMetric('<?= $metric->Code; ?>', '<?= $metric->Label; ?>');
								<?php } ?>
							<?php } ?>
							
							
							<?php if(isset($metricDimension->REPORT_Options)) { ?>
								//Set Google visualization options for line colors
								tl.setOption(<?= $metricDimension->REPORT_Options; ?>);
							<?php } ?>
							//Set Google visualization option for chart title
							tl.setOption('title', '<?= $metricDimension->REPORT_Title; ?>');
							//draw in the div element with id 'timeline'
							tl.draw('<?= $metricDimension->REPORT_ContainerID; ?>');
								
							$( ".<?= $metricDimension->REPORT_ContainerID; ?>datepicker" ).datepicker({ 
								defaultDate: +7,
								autoSize: true,
								dateFormat: 'mm/dd/yy',
							});	

					 <?php }//endif ?>
					 
				 //This is for any basic metric report. 
				 <?php if($metricDimension->REPORT_Type == 'Metric') { ?>
				 
				 		var MetricHTML = '<div class="stats <?= $metricDimension->REPORT_ClassName; ?>">';
							MetricHTML +=	'<ul>';
							MetricHTML +=		'<li>';
							MetricHTML +=			'<a href="<?= base_url(); ?>reports/analytics/<?= $metricDimension->REPORT_QueryString; ?>?web_id=<?= $web_id; ?><?= ((isset($_GET['from'])) ? '&from=' . $_GET['from'] : '');?><?= ((isset($_GET['to'])) ? '&to=' . $_GET['to'] : ''); ?>&r=<?= $metricDimension->REPORT_QueryString; ?>" class="count grey" id="<?= $metricDimension->REPORT_ContainerID; ?>"></a>';
							MetricHTML +=			'<span class="text"><a href="<?= base_url(); ?>reports/analytics/<?= $metricDimension->REPORT_QueryString; ?>?web_id=<?= $web_id; ?><?= ((isset($_GET['from'])) ? '&from=' . $_GET['from'] : '');?><?= ((isset($_GET['to'])) ? '&to=' . $_GET['to'] : ''); ?>&r=<?= $metricDimension->REPORT_QueryString; ?>"><?= $metricDimension->REPORT_Title; ?></a></span>';
							MetricHTML +=			'<div class="image">&nbsp;</div>';
							MetricHTML +=		'</li>';
							MetricHTML +=	'</ul>';
							MetricHTML += '</div>';
							
						$('#modules table tr td#<?= $metricDimension->REPORT_ContainerID; ?>_td').append(MetricHTML);
						var m = new oo.Metric("<?= $report->AnalyticsProfileID; ?>", new Date("<?= ((isset($_GET['from'])) ? $_GET['from'] : $metricDimension->REPORT_StartDate); ?>"), new Date("<?= ((isset($_GET['to'])) ? $_GET['to'] : $metricDimension->REPORT_EndDate); ?>"));
						<?php if(isset($metricDimension->REPORT_Metric) AND count($metricDimension->REPORT_Metric) > 0) { ?>
							<?php foreach($metricDimension->REPORT_Metric as $metric) { ?>
								<?php if(isset($metric->Code)) { ?>
									m.setMetric('<?= $metric->Code; ?>');
								<?php } ?>
							<?php } ?>
						<?php } ?>
						m.draw('#<?= $metricDimension->REPORT_ContainerID; ?>');
				<?php } ?>
				 
				 //This is for any basic metric report. 
				 <?php if($metricDimension->REPORT_Type == 'Query') { ?>
				 	var QueryHTML = '<div class="stats <?= $metricDimension->REPORT_ClassName; ?>">';
						QueryHTML +=	'<ul>';
						QueryHTML +=		'<li>';
						QueryHTML +=			'<a href="<?= base_url(); ?>reports/analytics/<?= $metricDimension->REPORT_QueryString; ?>?web_id=<?= $web_id; ?><?= ((isset($_GET['from'])) ? '&from=' . $_GET['from'] : '');?><?= ((isset($_GET['to'])) ? '&to=' . $_GET['to'] : ''); ?>&r=<?= $metricDimension->REPORT_QueryString; ?>" class="count grey" id="<?= $metricDimension->REPORT_ContainerID; ?>"></a>';
						QueryHTML +=			'<span class="text"><a href="<?= base_url(); ?>reports/analytics/<?= $metricDimension->REPORT_QueryString; ?>?web_id=<?= $web_id; ?><?= ((isset($_GET['from'])) ? '&from=' . $_GET['from'] : '');?><?= ((isset($_GET['to'])) ? '&to=' . $_GET['to'] : ''); ?>&r=<?= $metricDimension->REPORT_QueryString; ?>"><?= $metricDimension->REPORT_Title; ?></a></span>';
						QueryHTML +=			'<div class="image">&nbsp;</div>';
						QueryHTML +=		'</li>';
						QueryHTML +=	'</ul>';
						QueryHTML +=	'<div class="fix"></div>';
						QueryHTML += '</div>';
						
					$('td#<?= $metricDimension->REPORT_ContainerID; ?>_td').append(QueryHTML);
					
					var m = new oo.Query("<?= $report->AnalyticsProfileID; ?>",new Date("<?= ((isset($_GET['from'])) ? $_GET['from'] : $metricDimension->REPORT_StartDate); ?>"), new Date("<?= ((isset($_GET['to'])) ? $_GET['to'] : $metricDimension->REPORT_EndDate); ?>"));
					
					<?php if(isset($metricDimension->REPORT_Metric) AND count($metricDimension->REPORT_Metric) > 0) { ?>
						<?php foreach($metricDimension->REPORT_Metric as $metric) { ?>
							<?php if(isset($metric->Code)) { ?>
								m.addMetric('<?= $metric->Code; ?>');
							<?php } ?>
						<?php } ?>
					<?php } ?>
					
					<?php if(isset($metricDimension->REPORT_Dimension) AND count($metricDimension->REPORT_Dimension) > 0) { ?>
						<?php foreach($metricDimension->REPORT_Dimension as $dimension) { ?>
							<?php if(isset($dimension->Code)) { ?>
								m.addDimension('<?= $dimension->Code; ?>');
							<?php } ?>
						<?php } ?>
					<?php } ?>
					
					m.execute(function(data) {
						var type = '<?= $metricDimension->REPORT_ReturnType; ?>';
						switch(type) {
							case 'num' :
								var num = Math.round(data);
								$('#<?= $metricDimension->REPORT_ContainerID; ?>').text(num);
							break;	
							case 'percent' :
								var num = Math.round(data / 10) * 10 + '%';
								$('#<?= $metricDimension->REPORT_ContainerID; ?>').text(num);
							break;
							case 'time':
								if(data > 0) {
									var seconds = parseInt(data,10);
									var hours = Math.floor(seconds / 3600);
									var minutes = Math.floor((seconds - (hours * 3600)) / 60);
									var secs = seconds - (hours * 3600) - (minutes * 60);
									if(hours < 10) { hours = "0"+hours;}
									if(minutes < 10) {minutes = "0" + minutes;}
									if(seconds < 10) {secs = "0" + secs;}
									var time = hours + ':' + minutes +':'+seconds;
									$('#<?= $metricDimension->REPORT_ContainerID; ?>').text(time);
								}else {
									$('#<?= $metricDimension->REPORT_ContainerID; ?>').text('00:00:00');	
								}
							break;
						}
					});
				<?php } ?>
				
				//write out pie chart reports
				<?php if($metricDimension->REPORT_Type == 'StaticPie') { ?>
					var PieHTML = '<div class="stats staticpie <?= $metricDimension->REPORT_ClassName; ?>">';
						PieHTML +=		'<div class="pieImage"><a href="<?= base_url(); ?>reports/analytics/<?= $metricDimension->REPORT_QueryString; ?>?web_id=<?= $web_id; ?><?= ((isset($_GET['from'])) ? '&from=' . $_GET['from'] : '');?><?= ((isset($_GET['to'])) ? '&to=' . $_GET['to'] : ''); ?>&r=<?= $metricDimension->REPORT_QueryString; ?>"><img src="<?= base_url(); ?>imgs/<?= ((isset($metricDimension->REPORT_Options)) ? $metricDimension->REPORT_Options : 'pie02.png'); ?>" alt="Pie Chart" /></a></div>';
						PieHTML +=		'<span class="text"><a href="<?= base_url(); ?>reports/analytics/<?= $metricDimension->REPORT_QueryString; ?>?web_id=<?= $web_id; ?><?= ((isset($_GET['from'])) ? '&from=' . $_GET['from'] : '');?><?= ((isset($_GET['to'])) ? '&to=' . $_GET['to'] : ''); ?>&r=<?= $metricDimension->REPORT_QueryString; ?>"><?= $metricDimension->REPORT_Title; ?></a></span>';
						PieHTML +=		'<div class="fix"></div>';
						PieHTML += '</div>';
					$('#modules td#<?= $metricDimension->REPORT_ContainerID; ?>_td').append(PieHTML);
					
				<?php } ?>
				
				//write out pie chart reports
				<?php if($metricDimension->REPORT_Type == 'Custom') { ?>
					var PieHTML = '<div class="stats custom <?= $metricDimension->REPORT_ClassName; ?>">';
						PieHTML +=		'<div class="pieImage"><a href="<?= base_url(); ?>reports/analytics/<?= $metricDimension->REPORT_QueryString; ?>?web_id=<?= $web_id; ?><?= ((isset($_GET['from'])) ? '&from=' . $_GET['from'] : '');?><?= ((isset($_GET['to'])) ? '&to=' . $_GET['to'] : ''); ?>&r=<?= $metricDimension->REPORT_QueryString; ?>"><img src="<?= base_url(); ?>imgs/<?= ((isset($metricDimension->REPORT_Options)) ? $metricDimension->REPORT_Options : 'pie02.png'); ?>" alt="Pie Chart" /></a></div>';
						PieHTML +=		'<span class="text"><?= $metricDimension->REPORT_Title; ?></span>';
						PieHTML +=		'<div class="fix"></div>';
						PieHTML += '</div>';
					$('#analytic_reports').append(PieHTML);
				<?php } ?>
				
				//write out pie chart reports
				<?php if($metricDimension->REPORT_Type == 'Pie') { ?>
					var PieHTML = '<div class="stats pie <?= $metricDimension->REPORT_ClassName; ?>" style="cursor:pointer;" rel="<?= base_url(); ?>reports/analytics/<?= $metricDimension->REPORT_QueryString; ?>?web_id=<?= $web_id; ?><?= ((isset($_GET['from'])) ? '&from=' . $_GET['from'] : '');?><?= ((isset($_GET['to'])) ? '&to=' . $_GET['to'] : ''); ?>&r=<?= $metricDimension->REPORT_QueryString; ?>">';
						PieHTML +=		'<div class="google_chart" id="<?= $metricDimension->REPORT_ContainerID; ?>"></div>';
						PieHTML +=		'<div class="myWhiteout">&nbsp;</div>';
						PieHTML +=		'<span class="text"><?= $metricDimension->REPORT_Title; ?></span>';
						PieHTML +=		'<div class="fix"></div>';
						PieHTML += '</div>';
					$('#analytic_reports').append(PieHTML);
					$('#analytic_reports div.stats.pie.<?=$metricDimension->REPORT_ClassName; ?>').click(function() {
						var url = $(this).attr('rel');
						window.location.href = url;
					});
					
					var p = new oo.Pie("<?= $report->AnalyticsProfileID; ?>",new Date("<?= ((isset($_GET['from'])) ? $_GET['from'] : $metricDimension->REPORT_StartDate); ?>"), new Date("<?= ((isset($_GET['to'])) ? $_GET['to'] : $metricDimension->REPORT_EndDate); ?>"));
					
					<?php if(isset($metricDimension->REPORT_Metric) AND !empty($metricDimension->REPORT_Metric)) { ?>
						<?php foreach($metricDimension->REPORT_Metric as $metric) { ?>
							<?php if(isset($metric->Code)) { ?>
								p.setMetric('<?= $metric->Code; ?>','<?= $metric->Label; ?>');
							<?php } ?>
						<?php } ?>
					<?php } ?>
					
					<?php if(isset($metricDimension->REPORT_Dimension) AND count($metricDimension->REPORT_Dimension) > 0) { ?>
						<?php foreach($metricDimension->REPORT_Dimension as $dimension) { ?>
							<?php if(isset($dimension->Code)) { ?>
								p.setDimension('<?= $dimension->Code; ?>');
							<?php } ?>
						<?php } ?>
					<?php } ?>
					
					p.draw('<?= $metricDimension->REPORT_ContainerID; ?>');
				<?php } ?>
				
				//write out table reports
				<?php if($metricDimension->REPORT_Type == 'Table') { ?>
				
					var TableHTML = '<div class="stats table <?= $metricDimension->REPORT_ClassName; ?>">';
						TableHTML +=	'<div class="google_chart" id="<?= $metricDimension->REPORT_ContainerID; ?>"></div>';
						TableHTML +=	'<div class="fix"></div>';
						TableHTML += '</div>';
						
					$('#analytic_reports').append(TableHTML);
					var ta = new oo.Table("<?= $report->AnalyticsProfileID; ?>", new Date("<?= ((isset($_GET['from'])) ? $_GET['from'] : $metricDimension->REPORT_StartDate); ?>"), new Date("<?= ((isset($_GET['to'])) ? $_GET['to'] : $metricDimension->REPORT_EndDate); ?>")); 
					<?php if(isset($metricDimension->REPORT_Metric) AND count($metricDimension->REPORT_Metric) > 0) { ?>
						<?php foreach($metricDimension->REPORT_Metric as $metric) { ?>
							<?php if(isset($metric->Code)) { ?>
								ta.addMetric('<?= $metric->Code; ?>','<?= $metric->Label; ?>');
							<?php } ?>
						<?php } ?>
					<?php } ?>
					
					<?php if(isset($metricDimension->REPORT_Dimension) AND count($metricDimension->REPORT_Dimension) > 0) { ?>
						<?php foreach($metricDimension->REPORT_Dimension as $dimension) { ?>
							<?php if(isset($dimension->Code)) { ?>
								ta.addDimension('<?= $dimension->Code; ?>');
							<?php } ?>
						<?php } ?>
					<?php } ?>
					
					ta.setOption('page','enable');
					ta.setOption('pageSize', '<?= $metricDimension->REPORT_Options; ?>');
					ta.draw('<?= $metricDimension->REPORT_ContainerID; ?>');
				<?php } ?>
				 
			<?php }//end ->Reports foreach ?>
			$('div#analytic_reports').find('div.widget:first-child').addClass('removeMargin');
			$('div#analytic_reports').find('div.widget:first-child').next().addClass('noborder');

		<?php }else { ?>
			jQuery('#analytic_reports').append('<div id="error">There was an error pulling the analytic data for this client. Please try again</div>');
		<?php } ?>
	});
	
	function getTime(time) {
		var minutes = Math.floor(time / 60);
		var seconds = time - minutes * 60;
			seconds = Math.ceil(seconds / 10) * 10;
		var hours   = Math.floor(time / 3600);
		return minutes + ':' + seconds;
	}
	
	function changeDate() {
		var web_id = '<?= $web_id; ?>';
		var to     = $('input.to').val();
		var from   = $('input.from').val();	
		document.location.href = '<?= base_url(); ?>reports/analytics?web_id='+web_id+'&from='+from+'&to='+to;
	}

</script>
