<div class="content hideTagFilter">
    <div class="title">
    	<h5>Analytics</h5>
        <style type="text/css">div#uniform-switcher{display:none !important;}</style>
        <?php if(count($websites) > 1) { ?>
            <div class="websiteSwitch">
                <select name="websiteSwitch" class="chzn-select" id="switcher" placeholder="Switch Website">
                    <option value=""></option>
                    <?php foreach($websites as $site) : ?><option <?= (($website->WebID == $site->WebID) ? 'selected="selected"' : ''); ?> value="<?= $site->WebID; ?>"><?= str_replace('http://','',$site->URL); ?></option><? endforeach; ?>
                </select>
                <div class="fix"></div>
            </div>
        <?php } ?>
    </div>
    <?php notifyError(); ?>
    <?php include FCPATH . 'html/global/breadcrumb.php'; ?>
    <?php include FCPATH . 'html/theme/incl/analytics_nav.php'; ?>
    <div id="analytic_reports">
    	<div id="top" class="widget full" style="margin-top:5px">
        	<div class="head">
        		<h5 class="iChart7">Analytics</h5>
            </div>
            <div id="timeline"></div>
        </div>
    	<div id="bot" class="widget full" style="margin-top:5px;">
        	<div class="head">
        		<h5 class="iChart7">Error</h5>
            </div>
            <div id="table"><p>There is no analytic data detected for this website. Please select another website and try again.</p></div>
        </div>
    </div>
    <div class="fix"></div>
</div>
<div class="fix"></div>
<script type="text/javascript" language="javascript">
	var $ = jQuery;
	$('#switcher').change(function() {
		var web_id = $(this).val();
		var report = '<?= $_GET['r']; ?>';
		
		var from = $('input.from').val();
		var to = $('input.to').val();
		
		if(from != '' && to != '') {
			document.location.href = '<?= base_url(); ?>reports/analytics/'+report+'?web_id='+web_id+'&from='+from+'&to='+to+'&r='+report;
		}else {
			document.location.href = '<?= base_url(); ?>reports/analytics/'+report+'?web_id='+web_id+'&r='+report;
		}
	});
	
</script>
