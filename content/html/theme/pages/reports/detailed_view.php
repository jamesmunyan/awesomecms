<div class="content hideTagFilter">
    <div class="title">
    	<h5>Analytics</h5>
        <style type="text/css">div#uniform-switcher{display:none !important;}</style>
        <?php if(count($websites) > 1) { ?>
            <div class="websiteSwitch">
                <select name="websiteSwitch" class="chzn-select" id="switcher" placeholder="Switch Website">
                    <option value=""></option>
                    <?php foreach($websites as $site) : ?><option <?= (($website->WebID == $site->WebID) ? 'selected="selected"' : ''); ?> value="<?= $site->WebID; ?>"><?= str_replace('http://','',$site->URL); ?></option><? endforeach; ?>
                </select>
                <div class="fix"></div>
            </div>
        <?php } ?>
    </div>
    <?php notifyError(); ?>
    <?php include FCPATH . 'html/global/breadcrumb.php'; ?>
    <?php include FCPATH . 'html/theme/incl/analytics_nav.php'; ?>
    <div id="analytic_reports">
    	<div id="top" class="widget full" style="margin-top:5px">
        	<div class="head">
        		<h5 class="iChart7"><?= $pageTitle; ?></h5>
                <div class="timePicker">
                    <input type="text" class="mydatepicker from" value="<?= ((isset($_GET['from'])) ? $_GET['from'] : date('m/d/Y', strtotime(getOneMonthAgo()))); ?>" />
                    <span>to</span>
                    <input type="text" class="mydatepicker to" value="<?= ((isset($_GET['to'])) ? $_GET['to'] : date('m/d/Y')); ?>" />
                    <a class="button greenBtn changeDate" href="javascript:changeDate();">Change Date Range</a>
                    <div class="fix"></div>
                </div>
            </div>
            <div id="timeline"></div>
        </div>
    	<div id="bot" class="widget full" style="margin-top:5px;">
        	<div class="head">
        		<h5 class="iChart7"><?= $tableTitle; ?></h5>
            </div>
            <div id="table"></div>
        </div>
    </div>
    <div class="fix"></div>
</div>
<div class="fix"></div>
<script type="text/javascript" language="javascript">
	var $ = jQuery;
	$('#switcher').change(function() {
		var web_id = $(this).val();
		var report = '<?= $_GET['r']; ?>';
		
		var from = $('input.from').val();
		var to = $('input.to').val();
		
		if(from != '' && to != '') {
			document.location.href = '<?= base_url(); ?>reports/analytics/'+report+'?web_id='+web_id+'&from='+from+'&to='+to+'&r='+report;
		}else {
			document.location.href = '<?= base_url(); ?>reports/analytics/'+report+'?web_id='+web_id+'&r='+report;
		}
	});
	
	var innerHTML = '';
	oo.setOOId("<?= ReturnOOId(); ?>");
	oo.load(function() {
		<?php if($type == 'Timeline') : ?>
		var tl = new oo.Timeline("<?= $id; ?>",new Date("<?= ((isset($_GET['from'])) ? $_GET['from'] :  date('m/d/Y',strtotime(getOneMonthAgo()))); ?>"), new Date("<?= ((isset($_GET['to'])) ? $_GET['to'] : date('m/d/Y')); ?>"));
 		
		<?php if(isset($timeline['metrics']) AND !empty($timeline['metrics'])) : ?>
			<?php foreach($timeline['metrics'] as $metric) : ?>
				tl.addMetric('<?= $metric['code']; ?>', '<?= $metric['label']; ?>');
			<?php endforeach; ?>
		<?php endif; ?>
		
		<?php if(isset($timeline['dimensions']) AND !empty($timeline['dimensions'])) : ?>
			<?php foreach($timeline['dimensions'] as $d) : ?>
				tl.addDimension('<?= $d['code']; ?>', '<?= $d['label']; ?>');
			<?php endforeach; ?>
		<?php endif; ?>
				 
		tl.setOption('title', '<?= $timeline['title']; ?>');
		tl.setOption(<?= $timeline['options']; ?>);
		 
		tl.draw("timeline");
		
		<?php elseif($type == 'Pie') : ?>
		
		//draw out Pie chart
		var p = new oo.Pie("<?= $id; ?>",new Date("<?= ((isset($_GET['from'])) ? $_GET['from'] :  date('m/d/Y',strtotime(getOneMonthAgo()))); ?>"), new Date("<?= ((isset($_GET['to'])) ? $_GET['to'] : date('m/d/Y')); ?>"));
		
		<?php if(isset($timeline['metrics']) AND !empty($timeline['metrics'])) : ?>
			<?php foreach($timeline['metrics'] as $metric) : ?>
				p.setMetric('<?= $metric['code']; ?>', '<?= $metric['label']; ?>');
			<?php endforeach; ?>
		<?php endif; ?>
		
		<?php if(!empty($timeline['dimensions'])) : ?>
			<?php foreach($timeline['dimensions'] as $d) : ?>
				p.setDimension('<?= $d['code']; ?>', '<?= $d['label']; ?>');
			<?php endforeach; ?>
		<?php endif; ?>
		
		//draw into timeline div
		p.draw('timeline');	
		
		<?php endif; ?>
		
		var t = new oo.Table("<?= $id; ?>",new Date("<?= ((isset($_GET['from'])) ? $_GET['from'] :  date('m/d/Y',strtotime(getOneMonthAgo()))); ?>"), new Date("<?= ((isset($_GET['to'])) ? $_GET['to'] : date('m/d/Y')); ?>"));
		<?php if(isset($table['metrics']) AND !empty($table['metrics'])) : ?>
			<?php foreach($table['metrics'] as $metric) : ?>
				t.addMetric('<?= $metric['code']; ?>', '<?= $metric['label']; ?>');
			<?php endforeach; ?>
		<?php endif; ?>
		
		<?php if(isset($table['dimensions']) AND !empty($table['dimensions'])) : ?>
			<?php foreach($table['dimensions'] as $d) : ?>
				t.addDimension('<?= $d['code']; ?>', '<?= $d['label']; ?>');
			<?php endforeach; ?>
		<?php endif; ?>
        t.setOption('page', 'enable');

        t.setOption('pageSize', 10);
		t.draw('table',tableDone());	
	});
	
	function tableDone() {
		alert('tableDone');	
	}

	function getTime(time) {
		var minutes = Math.floor(time / 60);
		var seconds = time - minutes * 60;
			seconds = Math.ceil(seconds / 10) * 10;
		var hours = Math.floor(time / 3600);
		return minutes + ':' + seconds;
	}
	
	function changeDate() {
		var web_id = '<?= $web_id; ?>';
		var to     = $('input.to').val();
		var from   = $('input.from').val();	
		
		document.location.href = '<?= base_url(); ?>reports/analytics/<?= $_GET['r']; ?>?web_id='+web_id+'&from='+from+'&to='+to+'&r=<?= $_GET['r']; ?>';
	}
	
	$( ".mydatepicker" ).datepicker({ 
		defaultDate: +7,
		autoSize: true,
		dateFormat: 'mm/dd/yy',
	});	
</script>
