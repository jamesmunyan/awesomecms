<div class="content hideTagFilter">
    <div class="title">
    	<h5>Traffic Sources</h5>
        <style type="text/css">
			div#uniform-switcher{display:none !important;}
        	table.google-visualization-table-table td{width:50% !important;}
        </style>
        <?php if(count($websites) > 1) { ?>
            <div class="websiteSwitch">
                <select name="websiteSwitch" class="chzn-select" id="switcher" placeholder="Switch Website">
                    <option value=""></option>
                    <?php foreach($websites as $site) : ?><option <?= (($website->WebID == $site->WebID) ? 'selected="selected"' : ''); ?> value="<?= $site->WebID; ?>"><?= str_replace('http://','',$site->URL); ?></option><? endforeach; ?>
                </select>
                <div class="fix"></div>
            </div>
        <?php } ?>
    </div>
    <?php notifyError(); ?>
    <?php include FCPATH . 'html/global/breadcrumb.php'; ?>
    <?php include FCPATH . 'html/theme/incl/analytics_nav.php'; ?>
    <div id="analytic_reports">
    	<div id="top" class="widget full" style="margin-top:5px">
        	<div class="head">
        		<h5 class="iChart7">Traffic Sources</h5>
                <div class="timePicker">
                    <input type="text" class="mydatepicker from" value="<?= ((isset($_GET['from'])) ? $_GET['from'] : date('m/d/Y', strtotime(getOneMonthAgo()))); ?>" />
                    <span>to</span>
                    <input type="text" class="mydatepicker to" value="<?= ((isset($_GET['to'])) ? $_GET['to'] : date('m/d/Y')); ?>" />
                    <a class="button greenBtn changeDate" href="javascript:changeDate();">Change Date</a>
                    <div class="fix"></div>
                </div>
            </div>
            <div id="pie"></div>
        </div>
    	<div id="bot" class="widget full" style="margin-top:5px;">
        	<div class="head">
        		<h5 class="iChart7">Referral Sources: Visitors directed to your site from other sources</h5>
            </div>
            <div id="table1"></div>
        </div>
    	<div id="bot2" class="widget full" style="margin-top:5px;">
        	<div class="head">
        		<h5 class="iChart7">Keywords: Search terms entered into search engines to find your site</h5>
            </div>
            <div id="table2"></div>
        </div>
    </div>
    <div class="fix"></div>
</div>
<div class="fix"></div>
<script type="text/javascript" language="javascript">
	var $ = jQuery;
	$('#switcher').change(function() {
		var web_id = $(this).val();
		var report = '<?= $_GET['r']; ?>';
		
		var from = $('input.from').val();
		var to = $('input.to').val();
		
		if(from != '' && to != '') {
			document.location.href = '<?= base_url(); ?>reports/analytics/'+report+'?web_id='+web_id+'&from='+from+'&to='+to+'&r='+report;
		}else {
			document.location.href = '<?= base_url(); ?>reports/analytics/'+report+'?web_id='+web_id+'&r='+report;
		}
	});
	
	var innerHTML = '';
	oo.setOOId("<?= ReturnOOId(); ?>");
	oo.load(function() {
		var p = new oo.Pie("<?= $id; ?>",new Date("<?= ((isset($_GET['from'])) ? $_GET['from'] :  date('m/d/Y',strtotime(getOneMonthAgo()))); ?>"), new Date("<?= ((isset($_GET['to'])) ? $_GET['to'] : date('m/d/Y')); ?>"));
		 
		p.setMetric('ga:organicSearches', 'Visits');
		p.setDimension('ga:source');
		 
		p.draw('pie');
		 
		var t1 = new oo.Table("<?= $id; ?>",new Date("<?= ((isset($_GET['from'])) ? $_GET['from'] :  date('m/d/Y',strtotime(getOneMonthAgo()))); ?>"), new Date("<?= ((isset($_GET['to'])) ? $_GET['to'] : date('m/d/Y')); ?>"));
		t1.addMetric('ga:visits', 'Visits');
		t1.addDimension('ga:source','Source');
        t1.setOption('page', 'enable');
        t1.setOption('pageSize', 20);
		t1.draw('table1');	

		
		var t2 = new oo.Table("<?= $id; ?>",new Date("<?= ((isset($_GET['from'])) ? $_GET['from'] :  date('m/d/Y',strtotime(getOneMonthAgo()))); ?>"), new Date("<?= ((isset($_GET['to'])) ? $_GET['to'] : date('m/d/Y')); ?>"));
		t2.addMetric('ga:visits', 'Visits');
		t2.addDimension('ga:keyword','Keyword');
        t2.setOption('page', 'enable');
        t2.setOption('pageSize', 20);
		t2.draw('table2');
	});
	
	function changeDate() {
		var web_id = '<?= $web_id; ?>';
		var to     = $('input.to').val();
		var from   = $('input.from').val();	
		
		document.location.href = '<?= base_url(); ?>reports/analytics/<?= $_GET['r']; ?>?web_id='+web_id+'&from='+from+'&to='+to+'&r=<?= $_GET['r']; ?>';
	}
	
	$( ".mydatepicker" ).datepicker({ 
		defaultDate: +7,
		autoSize: true,
		dateFormat: 'mm/dd/yy',
	});	
</script>
