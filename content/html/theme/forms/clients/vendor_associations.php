<style type="text/css">
	#association .fltr{top:13px;right:5px;font-size:10px;font-weight:normal;color:#999;border:none;background:transparent;}
	#association .multiple{overflow:auto;width:97% !important;}
	#association input[type=text]{width:120%;}
	#addociation button.dualBtn{font-size:10px !important;font-weight:normal !important;}
	.ui-widget button {font-size:10px;font-weight:normal}
</style>

<div class="uDialog" style="text-align:left;">
    <div class="dialog-message popper" id="association" title="Vendor Associations" style="height:390px !important;">
        <div class="uiForm">
            <div class="widget" style="margin-top:10px;padding-top:0;margin-bottom:10px;border:none;">
            	<?php
					echo form_open('/admin/clients/associate_vendors',array('id'=>'vendorAssociationForm','class' => 'mainForm formPop','style'=>'text-align:left'));
				?>
                    <fieldset>
                        <div class="rowElem noborder">
                            <label style="padding-top:0;"><span class="req">*</span>Vendors</label>
                            <div class="formRight">
                            	<select class="chzn-select validate[required] required" placeholder="Select A Vendor" name="vendor" style="width:200px;" id="VendorSelection">
                                	<option value="0">All</option>
                                    <?php foreach($vendors as $vendor) {
										echo '<option value="' . $vendor->ID . '">' . $vendor->Name . '</option>';
									}?>
                                </select>
							</div>
                            <div class="fix"></div>
                        </div>
    					<div class="rowElem dualBoxes noborder">
                                <div class="floatleft w40">
                                    <input type="text" id="box1Filter" class="boxFilter" placeholder="Filter entries..." /><button type="button" id="box1Clear" class="fltr">x</button><br />
                                    <select id="box1View" multiple="multiple" class="multiple" style="height:200px;">
                                    	<?php foreach($allVendorContacts as $vContacts) { ?>
                                        	<option value="<?= $vContacts->ContactID; ?>"><?= $vContacts->FirstName . ' ' . $vContacts->LastName; ?></option>
                                        <?php } ?>
                                    </select>
                                    <br/>
                                    <span id="box1Counter" class="countLabel"></span>
                                    
                                    <div class="displayNone"><select id="box1Storage"></select></div>
                                </div>
                                    
                                <div class="dualControl">
                                    <button id="to2" type="button" class="basicBtn dualBtn mr5 mb15">&nbsp;&gt;&nbsp;</button>
                                    <button id="allTo2" type="button" class="basicBtn dualBtn">&nbsp;&gt;&gt;&nbsp;</button><br />
                                    <button id="to1" type="button" class="basicBtn dualBtn mr5">&nbsp;&lt;&nbsp;</button>
                                    <button id="allTo1" type="button" class="basicBtn dualBtn">&nbsp;&lt;&lt;&nbsp;</button>
                                </div>
                                    
                                <div class="floatright w40">
                                    <input type="text" id="box2Filter" class="boxFilter" placeholder="Filter entries..." /><button type="button" id="box2Clear" class="fltr">x</button><br />
                                    <select name="contacts" id="box2View" multiple="multiple" class="multiple" style="height:200px;">
                                    	<?php foreach($clientContacts as $cContacts){?>
                                        	<option value="<?=$cContacts->ContactID;?>"><?= $cContacts->FirstName . ' ' . $cContacts->LastName;?></option>
                                        <?php }?>
                                    </select><br/>
                                    <span id="box2Counter" class="countLabel"></span>
                                    
                                    <div class="displayNone"><select id="box2Storage"></select></div>
                                </div>
                            <div class="fix"></div>
                    	</div>
                        <div class="submitForm">
                        	<input type="hidden" name="client_id" value="<?= $cid;?>" />
                        </div>
                    </fieldset>
               	<?= form_close(); ?>
                <div class="fix"></div>			       
            </div> <? //end widget ?>
		</div>
	</div>
</div>
<script type="text/javascript">
	//re initialize jQuery
	var $ = jQuery;
	$.configureBoxes();
	
	$('#VendorSelection').change(function() {
		var vid = $(this).val();
		$.ajax({
			type:'GET',
			url:'/admin/clients/selectOptionsForAssociationFilter?vid='+vid,
			success:function(data) {
				if(data) {
					$('#box1View').empty().html(data);
				}else {
					jAlert('No contacts are available for this vendor. Please close this box and press the add contact button to add a contact to this vendor so the system can associate.','Error');	
				}
			}
		});
	});
	
	
	$("#vendorAssociationForm").validationEngine({promptPosition : "right", scroll: true});
	
	var AllChoicesLoaded = [];
	$('select#box1View option').each(function() {
			AllChoicesLoaded.push($(this).val());
		});

	var OrigionalSelectionsLoaded = [];
	$('select#box2View option').each(function() {
		OrigionalSelectionsLoaded.push($(this).val());
	});
	
	var AllChoicesAfter = [];
	var ClientChoicesAfter = [];
	
	$('#vendorAssociationForm').submit(function(e) {
		e.preventDefault();
		var formData = $(this).serialize();
		
		
		$('select#box1View option').each(function() {
			AllChoicesAfter.push($(this).val());
		});
		
		$('select#box2View option').each(function() {
			ClientChoicesAfter.push($(this).val());
		});
		
		$.ajax({
			type:'POST',
			url:'/admin/clients/associateVendorsToClient',
			data:{loadedVendorContacts:OrigionalSelectionsLoaded,selectedVendorContacts:ClientChoicesAfter,client_id:'<?= $cid; ?>'},
			success:function(data) {
				if(data == '1') {
					document.location.href = '<?= base_url() . 'clients?trigger=' . $cid . '&active_tab=contacts'; ?>';	
				}else {
					jAlert('The Vendor associations were not made. Please try again.','Error');	
				}
			}
		});
	});
	
	$(".chzn-select").chosen();
	
	$("#association").dialog({
		minWidth:570,
		height:508,
		autoOpen: true,
		modal: true,
		buttons: [
			{
				class:'redBtn',
				text:'Save',
				click:function() {$('#vendorAssociationForm').submit();}
			},
		] 
	});
</script>
