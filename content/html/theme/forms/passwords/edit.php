<div class="uDialog" style="text-align:left;">
    <div class="dialog-message" id="editPasswords" title="Edit Password (<?= $password->Username; ?>)">
        <div class="uiForm">
			<style type="text/css">
				label{margin-top:5px;float:left;}
				.ui-dialog #editPasswords div.chzn-search input[type="text"] {width:86% !important;}
				div.formError{z-index:2000 !important;}
			</style>
            <div class="widget" style="margin-top:-10px;padding-top:0;">
            	<div class="tab_container">
            		<div id="passwordsInfo" class="tab_content">
		            	<?php
					        $form = array(
					            'name' => 'editPasswords',
					            'id' => 'editPassForm',
					            'class' => 'mainForm editPasswords',
					            'style'=>'text-align:left !important;'
					        );
			        		echo form_open('/admin/passwords/form_processor/passwords/edit',$form);
			    		?>
        			<!-- Input text fields -->
        				<fieldset>
                        <?php //print_object($password); ?>
                        	<div class="rowElem noborder">
			                    <label><span class="req">*</span>Type</label>
                                <div class="formRight">
                                    <div style="float:left;margin-left:5px;margin-top:10px;">
                                        <select id="passType" name="passwordType" class="chzn-select validate[required]" style="width:210px;" onChange="javascript:addNewCustom(1);">
                                            <option value="">Select Type</option>
                                            <?php foreach ($types as $type) { ?>
                                                <option value="<?= $type->ID; ?>" <?= (($type->Name == $password->Type) ? 'selected="selected"' : ''); ?>><?= $type->Name; ?></option>
                                            <?php }
                                            
                                            if(GateKeeper('Passwords_Add_Type',$this->user['AccessLevel'])) { ?>
                                                <option value="other">Other</option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <input id="newTypeInput" type="text" name="newType" placeholder="Add New Password Type" style="width:15em !important;display:none;margin-left:10px;">
                                </div>
                                <div class="fix"></div>
                            </div>
			                <div class="rowElem noborder">
			                    <label><span class="req">*</span>Vendor</label>
			                    <div class="formRight">
                                    <div style="float:left;margin-left:5px;margin-top:10px;">
                                        <select id="vendorSel" name="vendors" class="chzn-select validate[required]" style="margin-top:15px !important;" onChange="javascript:addNewCustom(2);">
                                            <option value="">Select Vendor</option>
                                            <?php foreach ($vendors as $vendor) { ?>
                                                <option value="<?= $vendor->ID; ?>"<?php if ($vendor->ID == $password->VendorID){ echo ' selected';} ?>><?= $vendor->Name; ?></option>
                                            <?php } 
											
											if(GateKeeper('Vendor_Add',$this->user['AccessLevel'])) { ?>
                                                <option value="other">Other</option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <input id="newVendorInput" placeholder="Add New Vendor" type="text" name="newVendor" style="width:15em !important;display:none;margin-left:10px;">
			                    </div>
			                    <div class="fix"></div>
			                </div>
			                <div class="rowElem noborder">
			                    <label><span class="req">*</span>URL</label>
			                    <div class="formRight">
			                        <?=form_input(array('name'=>'login_address','id'=>'login_address','value'=>$password->LoginAddress,'class'=>'validate[custom[url]]')); ?>
			                    </div>
			                    <div class="fix"></div>
			                </div>
			                <div class="rowElem noborder">
			                    <label><span class="req">*</span>Username</label>
			                    <div class="formRight">
			                        <?=form_input(array('class'=>'required validate[required]','name'=>'username','id'=>'username','value'=>$password->Username)); ?>
			                    </div>
			                    <div class="fix"></div>
			                </div>
			                <div class="rowElem noborder">
			                    <label><span class="req">*</span>Password</label>
			                    <div class="formRight">
			                        <?=form_input(array('class'=>'validate[required]','name'=>'password','id'=>'password','value'=>$password->Password)); ?>
			                    </div>
			                    <div class="fix"></div>
			                </div>
			                <div class="rowElem noborder">
			                    <label>Notes</label>
			                    <div class="formRight">
			                        <?=form_input(array('name'=>'notes','id'=>'notes','value'=>$password->Notes)); ?>
			                    </div>
			                    <div class="fix"></div>
			                </div>	                
			                <div class="submitForm">
			               		<input type="hidden" name="PasswordsID" value="<?=$password->ID; ?>" />
			                </div>
			                <div class="fix"></div>
			           </fieldset>
    				<?= form_close(); ?>
    				</div>
                    <div id="loader" style="display:none;"><img src="<?= base_url() . THEMEIMGS; ?>loaders/loader2.gif" /></div>
    				<div class="fix"></div>
    			</div>	
    			<div class="fix"></div>		       
            </div> <? //end widget ?>
		</div>
	</div>
</div>
<script type="text/javascript">
	jQuery(".chzn-select").chosen();
	jQuery("#editPasswords").dialog({
		minWidth:800,
		height:400,
		autoOpen: true,
		modal: false,
		buttons: [
			{
				class:'redBtn',
				text:'Save',
				click:function() {jQuery('#editPassForm').submit()}	
			},
		]
	});
	jQuery('#editPassForm').submit(function(e) {
		e.preventDefault();
		var formData = jQuery(this).serialize();
		jQuery.ajax({
			url:'/admin/passwords/process_edit?pass_id=<?=$password->ID; ?>',
			type:'POST',
			data:formData,
			success:function(data) {
				if(data == '1') {
					jAlert('Password edited successfully!','Edit Confirmation',function() {
						passListTable();
						jQuery('#editPasswords').dialog('close');
					});
				}else {
					jAlert('There was an error editing the password. Please try again.','Password Edit Failed');
				}
			}
		});
	});
	
	$('#editPassForm').validationEngine({promptPosition : "top", scroll: true});
	
	function addNewCustom(type) {
		if(type == 1) {
			if($('#passType').val() == 'other') {
				$('#newTypeInput').slideDown('fast').addClass('validate[required]');
				$('#passType').removeClass('validate[required]');
			}else {
				$('#newTypeInput').slideUp('fast').removeClass('validate[required]');
				$('#passType').addClass('validate[required]');
			}
		}else{
			if($('#vendorSel').val() == 'other') {
				$('#newVendorInput').slideDown('fast').addClass('validate[required]');
				$('#vendorSel').removeClass('validate[required]');
			}else {
				$('#newVendorInput').slideUp('fast').removeClass('validate[required]');
				$('#vendorSel').addClass('validate[required]');	
			}
		}
	}
	
</script>

