<div class="uDialog" style="text-align:left;">
    <div class="dialog-message" id="addPasswords" title="Add Password">
        <div class="uiForm">
			<style type="text/css">
				label{margin-top:5px;float:left;}
				.ui-dialog #addPasswords div.chzn-search input[type="text"] {width:86% !important;}
				div.formError{z-index:2000 !important;}
			</style>
            <div class="widget" style="margin-top:-10px;padding-top:0;border-top:none;">
            	<div class="tab_container">
            		<div id="passwordsInfo" class="tab_content">
		            	<?php
					        $form = array(
					            'name' => 'addPasswords',
					            'id' => 'addPasswordsForm',
					            'class' => 'mainForm addPasswords',
					            'style'=>'text-align:left !important;'
					        );
			        		echo form_open('/admin/passwords/process_add',$form);
			    		?>
        				<!-- Input text fields -->
        				<fieldset>
                        	<div class="rowElem noborder">
			                    <label>Type</label>
                                <div class="formRight">
                                    <div style="float:left;margin-left:5px;margin-top:10px;">
                                        <select id="passType" name="passwordType" class="chzn-select validate[required]" style="width:210px;" onChange="javascript:addNewCustom(1);">
                                            <option value="">Select Type</option>
                                            <?php foreach ($types as $type) { ?>
                                                <option value="<?= $type->ID; ?>"><?= $type->Name; ?></option>
                                            <?php }
                                            
                                            if(GateKeeper('Passwords_Add_Type',$this->user['AccessLevel'])) { ?>
                                                <option value="other">Other</option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <input id="newTypeInput" type="text" name="newType" placeholder="Add New Password Type" style="width:15em !important;display:none;margin-left:10px;">
                                </div>
                                <div class="fix"></div>
                            </div>
			                <div class="rowElem noborder">
			                    <label>Vendor</label>
			                    <div class="formRight">
                                    <div style="float:left;margin-left:5px;margin-top:10px;">
                                        <select id="vendorSel" name="vendors" class="chzn-select validate[required]" style="margin-top:15px !important;" onChange="javascript:addNewCustom(2);">
                                            <option value="">Select Vendor</option>
                                            <?php foreach ($vendors as $vendor) { ?>
                                                <option value="<?= $vendor->ID; ?>"><?= $vendor->Name; ?></option>
                                            <?php } 
											
											if(GateKeeper('Vendor_Add',$this->user['AccessLevel'])) { ?>
                                                <option value="other">Other</option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <input id="newVendorInput" placeholder="Add New Vendor" type="text" name="newVendor" style="width:15em !important;display:none;margin-left:10px;">
			                    </div>
			                    <div class="fix"></div>
			                </div>
			                <div class="rowElem noborder">
			                    <label>URL</label>
			                    <div class="formRight">
			                        <?=  form_input(array('name'=>'login_address','id'=>'login_address','value' => '','class'=>'maskUrl validate[custom[url]]')); ?>
                                    <span class="formNote" style="margin-top:-10px;">http://www.example.com</span>
			                    </div>
			                    <div class="fix"></div>
			                </div>
			                <div class="rowElem noborder">
			                    <label><span class="req">*</span>Username</label>
			                    <div class="formRight">
			                        <?=  form_input(array('class'=>'required validate[required]','name'=>'username','id'=>'username','value'=>'')); ?>
			                    </div>
			                    <div class="fix"></div>
			                </div>
			                <div class="rowElem noborder">
			                    <label><span class="req">*</span>Password</label>
			                    <div class="formRight">
			                        <?=  form_input(array('name'=>'password','id'=>'password','value'=>'','class'=>'validate[required]')); ?>
			                    </div>
			                    <div class="fix"></div>
			                </div>
			                <div class="rowElem noborder">
			                    <label>Notes</label>
			                    <div class="formRight">
			                        <?=  form_input(array('name'=>'notes','id'=>'notes','value'=>'')); ?>
			                    </div>
			                    <div class="fix"></div>
			                </div>
			                <div class="submitForm">
			               		<input type="hidden" name="ClientID" value="<?=  $clientID; ?>" />
			                </div>
			                <div class="fix"></div>
			           </fieldset>
    				<?= form_close(); ?>
    				</div>
                    <div id="loader" style="display:none;"><img src="<?= base_url() . THEMEIMGS; ?>loaders/loader2.gif" /></div>
    				<div class="fix"></div>
    			</div>	
    			<div class="fix"></div>			       
            </div> <? //end widget ?>
		</div>
	</div>
</div>

<script type="text/javascript">
	var $ = jQuery;
	//jQuery("div[class^='widget']").simpleTabs();
	$('form#addPasswordsForm').submit(function(e){
		e.preventDefault();
		var formData = $(this).serialize();
		jQuery.ajax({
			url:'/admin/passwords/process_add',
			type:'POST',
			data:formData,
			success:function(data) {
				if(data == '1') {
					jAlert('Password added successfully!','Add Confirmation',function() {
						passListTable();
						jQuery('#addPasswords').dialog('close');
					});
				}else {
					jAlert('There was an error adding the password. Please try again.','Password Add Failed');
				}
			}
		});
	});
	$(".chzn-select").chosen();
	$("#addPasswords").dialog({
		minWidth:800,
		height:400,
		autoOpen: true,
		modal: false,
		buttons: [
			{
				class:'greenBtn',
				text:'Add',
				click:function() {$('#addPasswordsForm').submit()}	
			},
		]
	});
	
	//
	$('#addPasswordsForm').validationEngine({promptPosition : "top", scroll: true});
	
	function addNewCustom(type) {
		if(type == 1) {
			if($('#passType').val() == 'other') {
				$('#newTypeInput').slideDown('fast').addClass('validate[required]');
				$('#passType').removeClass('validate[required]');
			}else {
				$('#newTypeInput').slideUp('fast').removeClass('validate[required]');
				$('#passType').addClass('validate[required]');
			}
		}else{
			if($('#vendorSel').val() == 'other') {
				$('#newVendorInput').slideDown('fast').addClass('validate[required]');
				$('#vendorSel').removeClass('validate[required]');
			}else {
				$('#newVendorInput').slideUp('fast').removeClass('validate[required]');
				$('#vendorSel').addClass('validate[required]');	
			}
		}
	}
	
</script>