<div id="editAvatarCustomPopup" style="display:none;">
    <div class="dialog-message" id="editUserCustomAvatar" title="Edit Avatar">
        <div class="uiForm">
            <p style="margin-left:15px !important;">Upload a custom Avatar to our system.</p>
            <?= form_open_multipart(base_url().'profile/avatar/upload', array('id' => 'uploadAvatar','class'=>'valid')); ?>
            	<input name="custom_avatar" placeholder="Custom Avatar" id="fileInput" class="fileInput validate[required]" type="file" size="24" style="opacity:0;" />
                <input type="hidden" name="user_id" value="<?= $user->ID; ?>" />
            <?= form_close(); ?>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?= base_url(); ?>js/avatar_upload.js"></script>
