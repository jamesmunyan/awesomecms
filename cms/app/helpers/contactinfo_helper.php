<?php
	function LoadUserPhoneNumberTable($contact,$view = false) { ?>
		<script type="text/javascript" src="<?= base_url(); ?>js/contact_info_phone.js"></script>
		<?php if(!empty($contact->Phones)) { ?>
            <div style="margin-top:10px;margin-bottom:30px;">
                <div class="head"><h5 class="iPhone">Phone Numbers</h5></div>
                <div id="phone_numbers_table">
                    <table cellpadding="0" cellspacing="0" width="100%" class="tableStatic" style="border:1px solid #d5d5d5;">
                        <thead>
                            <tr>
                                <td width="10%" style="text-align:center;padding-left:10px;">Primary</td>
                                <td width="10%" style="text-align:center;padding-left:10px;">Type</td>
                                <td style="text-align:left;padding-left:10px;">Phone Number</td>
                                <?php if(!$view) { ?>
                                <td class="actionsCol" style="text-align:center;">Actions</td>
                                <?php } ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($contact->Phones as $phone) { ?>
                                <tr>
                                    <td width="10%" style="text-align:center;">
                                    	<?php if(!$view) { ?>
                                        <input 
                                            type="radio" 
                                            name="phone" 
                                            onclick="javascript:primaryPhone('<?=$phone->PHONE_ID;?>','<?=$phone->OWNER_ID;?>','<?=$phone->PHONE_Primary;?>')"
                                            value="<?= $phone->PHONE_Number; ?>" 
                                            <?= ($phone->PHONE_Primary != 0) ? 'checked' : ''; ?> class="change_primary_phone" />
                                        <?php }else { ?>
                                        	<?= ($phone->PHONE_Primary != 0) ? 'Primary' : ''; ?>
                                        <?php } ?>
                                    </td>
                                    <td style="text-transform:capitalize;"><?= $phone->PHONE_Type;?></td>
                                    <td width="80%"><?= $phone->PHONE_Number; ?></td>
                                    <?php if(!$view) { ?>
                                    <td width="10%" class="actionsCol">
                                    	<a title="Edit Contact Phone" href="javascript:editPhone('<?= $phone->PHONE_ID; ?>');" class="actions_link">
                                            <img src="<?= base_url() . THEMEIMGS; ?>icons/color/pencil.png" alt="" />
                                        </a>
                                    </td>
                                    <?php } ?>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <div class="fix"></div>
                </div>
            </div>
        <?php }else { ?>
            <div style="margin-top:10px;margin-bottom:0px;">
                <div class="head"><h5 class="iPhone">Phone Numbers</h5></div>
                <p class="noData" style="text-align:center;"><?= (($contact->OwnerType == 3) ? 'No phone numbers found for this user.' : 'No phone numbers found for this contact.'); ?></p>
            </div>
        <?php } ?>
        <?php if(!$view) {?>
            	<a href="javascript:addPhone('<?= $contact->ContactID; ?>','<?= $contact->OwnerType; ?>');" class="greenBtn floatRight button" style="margin-top:-20px;">Add New Phone Number</a>
        <? } ?>
        <div id="phonePopups"></div>
	<? }
	
	function LoadUserEmailAddresses($contact,$view = false) { ?>
		<script type="text/javascript" src="<?= base_url(); ?>js/contact_info_email.js"></script>
		<?php if(!empty($contact->Emails)) { ?>
            <div style="margin-top:10px;margin-bottom:0px;">
                <div class="head"><h5 class="iMail">Email Addresses</h5></div>
                <table cellpadding="0" cellspacing="0" width="100%" class="tableStatic" style="border:1px solid #d5d5d5;">
                    <thead>
                        <tr>
                            <td width="10%" style="text-align:center;">Primary</td>
                            <td width="80%" style="text-align:left;padding-left:10px;">Email Address</td>
                            <?php if(!$view) { ?>
                            <td width="10%" style="text-align:center;">Actions</td>
                            <?php } ?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($contact->Emails as $email) { ?>
                            <tr>
                                <td width="10%" style="text-align:center;">
                                	<?php if(!$view) { ?>
                                    <input 
                                        type="radio" 
                                        name="email" 
                                        onclick="javascript:primaryEmail('<?= $email->EMAIL_ID;?>','<?= $email->OWNER_ID;?>','<?= $email->EMAIL_Primary;?>')"
                                        value="<?= $email->EMAIL_Address; ?>" 
                                        <?= ($email->EMAIL_Primary != 0) ? 'checked' : ''; ?> class="change_primary_email" />
                                    <?php } else { ?>
                                    	<?= (($email->EMAIL_Primary != 0) ? 'Primary' : ''); ?>
                                    <?php } ?>
                                </td>
                                <td width="80%"><?= $email->EMAIL_Address; ?></td>
                                <?php if(!$view) { ?>
                                <td width="10%" style="text-align:center;"><a title="Edit User Email" href="javascript:editEmail('<?= $email->EMAIL_ID; ?>');" class="actions_link"><img src="<?= base_url() . THEMEIMGS; ?>icons/color/pencil.png" alt="" /></a></td>
                                <?php } ?>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        <?php }else { ?>
            <div style="margin-top:10px;margin-bottom:60px;">
                <div class="head"><h5 class="iPhone">Email Addressess</h5></div>
                <p class="noData" style="text-align:center"><?= (($contact->OwnerType == 3) ? 'No email addresses found for this user.' : 'No email addresses found for this contact.'); ?></p>
            </div>
        <?php } ?>
        <?php if(!$view) { ?>
        	<!-- (did,uid,type)  -->
            	<a href="javascript:addEmail('<?= $contact->ContactID; ?>','<?= $contact->OwnerType; ?>');" class="greenBtn floatRight button" style="margin-top:10px;">Add New Email</a>
        <?php } ?>
        <div id="emailPopups"></div>
	<?php }
?>