<?php

	/* 
		============================================================
		Write default report dashboard for website
		============================================================
		@param $profile_id = Google Analytics Profile ID found in the Admin pane of the website properties inside Google Analytics dashboard.
	*/
	function getDefaultReports($profile_id) {
		$ci &= get_instance();
		$ci->load->model('domanalytics');
		
		$default_reports = $ci->domanalytics->getDefaultReports();
		return $default_reports;
	}
	
	/* 
		============================================================
		Write default report dashboard for websites
		============================================================
	*/
	function buildWebsiteTable() { ?>
		<table cellspacing="0" cellpadding="0" id="example" class="display analytics_table" width="100%">
        	<thead>
        		<tr>
            		<th>Hosting Provider</th>
                    <th>CMS</th>
                    <th>URL</th>
                    <th>Actions</th>
            	</tr>
            </thead>
            <tbody>
            	<tr>
                	<td>Test</td>
                    <td>Test</td>
                    <td>Test</td>
                    <td>Test</td>
                </tr>
            </tbody>
        </table>
	<?php }
	
	function getOneMonthAgo() {
		// set your date here
		$mydate = date('m/d/Y', time());
		
		/* strtotime accepts two parameters.
		The first parameter tells what it should compute.
		The second parameter defines what source date it should use. */
		$lastyear = strtotime("-1 month", strtotime($mydate));
		
		// format and display the computed date
		return date("m/d/Y", $lastyear);
	}
	
?>