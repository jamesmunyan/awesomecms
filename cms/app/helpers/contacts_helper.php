<?php

/*
	CONTACT TABLE
		* Change from the norm to associate vendor contacts from the VendorClientContactLinkage table along with other normal contacts.
		* Also has different buttons. this will only show up in the popups.
*/

function ClientContactsMainTable($cid) {
	?><script type="text/javascript" src="<?= base_url(); ?>js/client_contacts.js"></script><?php
	$ci =& get_instance();
	$ci->load->model('system_contacts','domcontacts');
	$userPermissionLevel = $ci->user['AccessLevel'];
	//Grab the users privilages from the session.
	$level 	     = $ci->user['DropdownDefault']->LevelType;
	$addPriv 	 = GateKeeper('Contact_Add',$userPermissionLevel);
	$editPriv 	 = GateKeeper('Contact_Edit',$userPermissionLevel);
	$disablePriv = GateKeeper('Contact_Disable_Enable',$userPermissionLevel);
	$listingPriv = GateKeeper('Contact_List',$userPermissionLevel);
	
	$contacts = $ci->domcontacts->getMyClientContacts($cid);
	
	if($listingPriv and !empty($contacts)) { ?>
    	<table cellpadding="0" cellspacing="0" width="100%" class="tableStatic" id="ClientContacts" style="border:1px solid #d5d5d5;">
        	<thead>
            	<tr>
                    <td>Company</td>
                    <td>Title</td>
                    <td>Name</td>
                    <td>Primary Email</td>
                    <td>Primary Phone</td>
					<td>Actions</td>
                </tr>
            </thead>
            <tbody>
            	<?php foreach($contacts as $contact) : ?>
                	<tr>
                    <td style="width:75px;"><?= ((!empty($contact->CompanyName)) ? $contact->CompanyName : '...'); ?></td>
                	<td style="width:75px;white-space:nowrap;overflow:hidden;"><?= ((!empty($contact->JobTitle)) ? $contact->JobTitle : '...'); ?></td>
                    <td><?= $contact->FirstName . ' ' . $contact->LastName; ?></td>
                    <td><?= ((!empty($contact->EmailAddress)) ? $contact->EmailAddress : '...'); ?></td>
                    <td style="width:150px;"><?= ((!empty($contact->PhoneNumber)) ? $contact->PhoneNumber : '...'); ?></td>
                    <td style="width:50px;text-align:center !important;">
                    	<?php if($editPriv) { ?>
                        	<a title="Edit Contact" href="javascript:editClientContact('<?= $contact->ContactID; ?>');" class="actions_link">
                           		<img src="<?= base_url();?>imgs/icons/color/pencil.png" alt="" />
                            </a>
                        <?php } ?>
                            <a title="View Contact" href="javascript:viewVendorContact('<?= $contact->ContactID; ?>','<?= $contact->OwnerType; ?>');" class="actions_link"><img src="<?= base_url(); ?>imgs/icons/color/cards-address.png" alt="" /></a>
                    </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php }else { ?>
    	<p class="noData">No Client Contacts Found</p>
    <?php }
}

function ClientContactsMainTableWithoutActions($cid) {
	?><script type="text/javascript" src="<?= base_url(); ?>js/client_contacts.js"></script><?php
	$ci =& get_instance();
	$ci->load->model('system_contacts','domcontacts');
	$userPermissionLevel = $ci->user['AccessLevel'];
	//Grab the users privilages from the session.
	$level 	     = $ci->user['DropdownDefault']->LevelType;
	$listingPriv = GateKeeper('Contact_List',$userPermissionLevel);
	
	$contacts = $ci->domcontacts->getMyClientContacts($cid);
	
	if($listingPriv and !empty($contacts)) { ?>
    	<table cellpadding="0" cellspacing="0" width="100%" class="tableStatic" id="ClientContacts" style="border:1px solid #d5d5d5;">
        	<thead>
            	<tr>
                    <td>Company</td>
                    <td>Title</td>
                    <td>Name</td>
                    <td>Primary Email</td>
                    <td>Primary Phone</td>
                </tr>
            </thead>
            <tbody>
            	<?php foreach($contacts as $contact) : ?>
                	<tr>
                    <td><?= $contact->CompanyName; ?></td>
                	<td><?= $contact->JobTitle; ?></td>
                    <td><?= $contact->FirstName . ' ' . $contact->LastName; ?></td>
                    <td><?= $contact->EmailAddress; ?></td>
                    <td><?= $contact->PhoneNumber; ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php }else { ?>
    	<p class="noData">No Client Contacts Found</p>
    <?php }
}

function ContactsMainTable($actions_off = false,$tab = false, $id = false,$isVendor = false,$type = 3) {
	?><script type="text/javascript" src="<?= base_url(); ?>js/contact_popups.js"></script><?php
	$ci =& get_instance();
	$ci->load->model('system_contacts','domcontacts');
	$level 				 = $ci->user['DropdownDefault']->LevelType;
	$userPermissionLevel = $ci->user['AccessLevel'];
	$addPriv     		 = GateKeeper('Contact_Add',$userPermissionLevel);
	$editPriv    		 = GateKeeper('Contact_Edit',$userPermissionLevel);
	$disablePriv 		 = GateKeeper('Contact_Disable_Enable',$userPermissionLevel);
	$listingPriv 		 = GateKeeper('Contact_List',$userPermissionLevel);
	
	if(!$isVendor) {
		$contacts = $ci->domcontacts->buildContactTable($id,false);
	}else {
		$contacts = $ci->domcontacts->buildContactTable($id,true,2);
	}
		
	if($addPriv AND !$tab) { ?>
		<a href="javascript:addContact(0,'<?= LASTVISITEDPAGE; ?>');" class="greenBtn floatRight button addButtonTop">Add New Contact</a>	
	<?php }
	
	if($contacts AND $listingPriv) { ?>
   		<table cellpadding="0" cellspacing="0" border="0" class="<?= ((!$tab) ? 'display' : 'tableStatic'); ?> contacts" id="example" width="100%;" <?= (($tab) ? 'style="border:1px solid #d5d5d5;"' : ''); ?>>
			<thead>
            	<tr>
                	<?php if(!$tab) { ?>
                        <th style="width:50px;">Team</th>
                        <th>Type</th>
                        <th>Client/Vendor Name</th>
                        <th>Name</th>
                        <th>Title</th>
                        <th>Primary Email</th>
                        <th>Primary Phone</th>
                        <?php if(!$actions_off) { ?>
                        	<th class="actionCol noSort" style="width:50px; text-align:center !important;">Actions</th>
                        <?php } ?>
                    <?php }else { ?>
                        <td>Name</td>
                    	<td>Title</td>
                        <td>Primary Email</td>
                        <td>Primary Phone</td>
                        <?php if(!$actions_off) { ?>
                        	<td class="actionCol noSort" style="width:50px; text-align:center !important;">Actions</td>
                        <?php } ?>
                    <?php } ?>
                </tr>
            </thead>
            <tbody>
            	<?php foreach($contacts as $contact) { ?>
                    <tr <?= ((!$tab) ? 'class="tagElement ' .  $contact->ClassName . '"' : ''); ?>>
                    	  <?php if(!$tab) { ?><td class="tags"><div class="<?= $contact->ClassName; ?>">&nbsp;</div><span style="display:none;"><?= $contact->ClassName; ?></span></td><?php } ?>
                          <?php if(!$tab) { ?><td class="alignLeft">
                          	<?php
								switch($contact->OwnerType) :
									case '1' : echo 'Client';break;
									case '2' : echo 'Vendor';break;
									case '3' : echo 'User';break;
									case '4' : echo 'General Contact';break;
									case '5' : echo 'Website';break;
									case '6' : echo 'Contact';break;
									case '7' : echo 'Agency';break;
									case '8' : echo 'Group';break;
								endswitch;
							?>
                          </td>
                          <?php } ?>
                          <?php if(!$tab) { ?><td><?= (!empty($contact->Owner) ? $contact->Owner : '...'); ?></td><?php } ?>
                          <td><a href="javascript:viewContact('<?= $contact->ContactID; ?>');"><?= $contact->FirstName . ' ' . $contact->LastName; ?></a></td>
                          <td><?= (!empty($contact->JobTitle) ? $contact->JobTitle : '...'); ?></td>
                          <td class="">
                          	<?php if(!empty($contact->Emails)) { ?>
                            	<?php foreach($contact->Emails as $email) { ?>
                                	<?php if($email->EMAIL_Primary == 1) { ?>
                                    	<a target="_blank" href="mailto:<?= $email->EMAIL_Address; ?>"><?= $email->EMAIL_Address; ?></a>
                                    <?php } ?>
                                <?php } ?>
                            <?php }else {  ?>
                            	<span>...</span>
                            <?php } ?>
                          </td>
                          <td>
                          	<?php if(!empty($contact->Phones)) { ?>
                            	<?php foreach($contact->Phones as $phone) { ?>
                                	<?php if($phone->PHONE_Primary == 1) { ?>
                                    	<?= $phone->PHONE_Number; ?>
                                    <?php } ?>
                                <?php } ?>
                            <?php }else { ?>
								<span>...</span>
							<?php }	 ?>
                          </td>
                          <?php if(!$actions_off) { ?>
                              <td class="actionsCol noSort">
                                <?php if($editPriv) { ?><a title="Edit Contact" href="javascript:editContact('<?= $contact->ContactID; ?>');" class="actions_link">
                                    <img src="<?= base_url();?>imgs/icons/color/pencil.png" alt="" />
                                </a><?php } ?>
                                <a title="View Contact" href="javascript:viewContact('<?= $contact->ContactID; ?>','<?= $contact->OwnerType;?>');" class="actions_link"><img src="<?= base_url(); ?>imgs/icons/color/cards-address.png" alt="" /></a>
                              </td>
                          <?php } ?>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    <?php }else { ?>
    	<p class="noData">No Contacts Found</p>
    <?php } 
	if($addPriv AND !$actions_off AND !$tab) { ?>
		<a href="javascript:addContact(0,'<?= LASTVISITEDPAGE; ?>');" class="greenBtn floatRight button addButtonBottom">Add New Contact</a>	
	<?php }
}

?>