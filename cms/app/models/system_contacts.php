<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * =======================================================
 * 	 CONTACT TYPE LEGEND
 * =======================================================
 * @CID:  = Clients
 * @VID:  = Vendors
 * @UID:  = Users
 * @GID:  = Group Contact
 * @WID:  = Website Contact
 * @DID:  = General Contact (makes no sense but ok)
 */



class System_contacts extends DOM_Model {

	var $level;

    function __construct() {
        // Call the Model constructor
        parent::__construct();
		$this->load->model('administration','adminQueries');
    }
	
	public function getJobTitles() {
		$query = $this->db->select('TITLE_ID as Id,TITLE_Name as Name')->from('xTitles')->order_by('TITLE_Name','asc')->get();
		return ($query) ? $query->result() : FALSE;	
	}
	
	public function getJobTitleText($id) {
		$sql = 'SELECT TITLE_Name as Name FROM xTitles WHERE TITLE_ID = "' . $id . '"';
		$query = $this->db->query($sql);
		return ($query) ? $query->row()->Name : FALSE;	
	}
	
	public function updatePrimaryAddress($address_id,$data) {
		$this->db->where('ADDRESS_ID',$address_id);
		return ($this->db->where('DirectoryAddresses',$data)) ? TRUE : FALSE;
	}
	
	public function addContact($data) {
		
		$check = array();
		
		if(isset($data['PhoneNumbers'])) {
			$add_phone = $data['PhoneNumbers'];
		}
		
		if(isset($data['EmailAddresses'])) {
			$add_email = $data['EmailAddresses'];
		}else {
			$skip = true;	
		}
		
		if(isset($data['DirectoryAddresses'])) {
			$add_address = $data['DirectoryAddresses'];
		}
		
		$directory = $data['Directories'];
		
		$add_directory_entry_id = (($this->db->insert('Directories',$directory)) ? $this->db->insert_id() : FALSE);
		
		if($add_directory_entry_id) {
			array_push($check,$add_directory_entry_id);
			if(isset($add_phone)) {
				$add_phone['DIRECTORY_ID'] = $add_directory_entry_id;
				$insert_phone = $this->db->insert('PhoneNumbers',$add_phone);
				array_push($check,$insert_phone);
			}
			
			if(isset($add_address)) {
				$add_address['DIRECTORY_ID'] = $add_directory_entry_id;
				$insert_address = $this->db->insert('DirectoryAddresses',$add_address);
				array_push($check,$insert_address);
			}
		
			if(isset($add_email)) { 	
				$add_email['DIRECTORY_ID'] = $add_directory_entry_id;
				$insert_email = $this->db->insert('EmailAddresses',$add_email);
				array_push($check,$insert_email);
			}
			
			return $check;
		}else {
			return FALSE;	
		}
		
	}
	
	public function getMyClientContacts($cid) {
		//collection array
		$contacts = array();
		//get normal contacts like users etc
		$sql = 'SELECT 
				d.DIRECTORY_ID as ContactID,
				d.OWNER_ID as OwnerID,
				(SELECT CLIENT_Name FROM Clients WHERE CLIENT_ID = d.OWNER_ID) as CompanyName,
				d.JobTitle as JobTitle,
				(SELECT TYPE_Name FROM xContactType WHERE TYPE_ID = d.DIRECTORY_Type) as TypeLabel,
				d.DIRECTORY_Type as OwnerType,
				d.DIRECTORY_FirstName as FirstName,
				d.DIRECTORY_LastName as LastName,
				(SELECT PHONE_Number FROM PhoneNumbers WHERE DIRECTORY_ID = d.DIRECTORY_ID AND PHONE_Primary = 1 AND PHONE_Active = 1 ORDER BY PHONE_Created DESC LIMIT 1) AS PhoneNumber,
				(SELECT EMAIL_Address FROM EmailAddresses WHERE DIRECTORY_ID = d.DIRECTORY_ID AND EMAIL_Primary = 1 AND EMAIL_Active = 1 ORDER BY EMAIL_Created DESC LIMIT 1) As EmailAddress
				FROM Directories d
				WHERE d.DIRECTORY_Type = 1 AND d.OWNER_ID = "' . $cid . '";';
		$clientQuery = $this->db->query($sql);
		
		if($clientQuery) {
			$clientContacts = $clientQuery->result();
			foreach($clientContacts as $contact) {
				//collect the contacts in our collection array $contacts();
				array_push($contacts,$contact);
			}
		}
		
		$uSql = 'SELECT 
					u.USER_ID as UserID,
					ui.CLIENT_ID as ClientID,
					d.DIRECTORY_ID as ContactID,
					d.OWNER_ID as OwnerID,
					(SELECT CLIENT_Name FROM Clients WHERE CLIENT_ID = ui.CLIENT_ID) as CompanyName,
					d.JobTitle as JobTitle,
					(SELECT TYPE_Name FROM xContactType WHERE TYPE_ID = d.DIRECTORY_Type) as TypeLabel,
					d.DIRECTORY_Type as OwnerType,
					d.DIRECTORY_FirstName as FirstName,
					d.DIRECTORY_LastName as LastName,
					(SELECT PHONE_Number FROM PhoneNumbers WHERE DIRECTORY_ID = d.DIRECTORY_ID AND PHONE_Primary = 1 AND PHONE_Active = 1 ORDER BY PHONE_Created DESC LIMIT 1) AS PhoneNumber,
					(SELECT EMAIL_Address FROM EmailAddresses WHERE DIRECTORY_ID = d.DIRECTORY_ID AND EMAIL_Primary = 1 AND EMAIL_Active = 1 ORDER BY EMAIL_Created DESC LIMIT 1) As EmailAddress
					FROM Users_Info ui
					RIGHT JOIN Users u ON ui.USER_ID = u.USER_ID
					RIGHT JOIN Directories d ON d.DIRECTORY_ID = ui.DIRECTORY_ID
					WHERE ui.CLIENT_ID = "' . $cid . '"';
		$userQuery = $this->db->query($uSql);
		
		if($userQuery) {
			$userContacts = $userQuery->result();
			foreach($userContacts as $contact) {
				array_push($contacts,$contact);	
			}
		}
				
		$vsql = 'SELECT 
				  	vccl.VENDOR_ID as VID,
				  	vccl.CLIENT_ID as CID,
				  	vccl.DIRECTORY_ID as DID,
					d.DIRECTORY_ID as ContactID,
					d.OWNER_ID as OwnerID,
					d.JobTitle as JobTitle,
					(SELECT TYPE_Name FROM xContactType WHERE TYPE_ID = d.DIRECTORY_Type) as TypeLabel,
					d.DIRECTORY_Type as OwnerType,
					d.DIRECTORY_FirstName as FirstName,
					d.DIRECTORY_LastName as LastName,
					v.VENDOR_Name as VendorName,
					v.VENDOR_Name as CompanyName,
					(SELECT PHONE_Number FROM PhoneNumbers WHERE DIRECTORY_ID = d.DIRECTORY_ID AND PHONE_Primary = 1 AND PHONE_Active = 1 ORDER BY PHONE_Created DESC LIMIT 1) AS PhoneNumber,
					(SELECT EMAIL_Address FROM EmailAddresses WHERE DIRECTORY_ID = d.DIRECTORY_ID AND EMAIL_Primary = 1 AND EMAIL_Active = 1 ORDER BY EMAIL_Created DESC LIMIT 1) As EmailAddress
				 	FROM VendorClientContactLinkage vccl
				 	RIGHT JOIN Directories d ON d.DIRECTORY_ID = vccl.DIRECTORY_ID
				 	RIGHT JOIN Vendors v ON v.VENDOR_ID = vccl.VENDOR_ID
				 	WHERE vccl.CLIENT_ID = "' . $cid . '"';
		$vendorContactQuery = $this->db->query($vsql);
		
		if($vendorContactQuery) {
			$vendorContacts = $vendorContactQuery->result();
			foreach($vendorContacts as $vendorContact) {
				//push vendor contacts to collection
				array_push($contacts,$vendorContact);
			}
		}
		
		//GRAB GROUP QUERY
		//get the clients parent group
		$gid = $this->db->select('GROUP_ID as GID')->from('Clients')->where('CLIENT_ID',$cid)->get();
		if($gid) {
			$gid = $gid->row()->GID;
			$gContacts = $this->getGroupLevelContacts($gid);
			if($gContacts) {
				foreach($gContacts as $gUnit) {
					$gUnit->CompanyName = $this->getGroupContactOwner($gid);
					array_push($contacts,$gUnit);	
				}
			}
		}
		
		
		if(!empty($contacts)) {
			return $contacts;
		}else {
			return FALSE;
		}
	}
	
	public function removeAllClientVendorContacts($cid) {
		$this->db->where('CLIENT_ID',$cid);
		return ($this->db->delete('VendorClientContactLinkage')) ? TRUE : FALSE; 
	}
	
	public function addClientVendorAssociations($data) {
		return ($this->db->insert_batch('VendorClientContactLinkage',$data)) ? TRUE : FALSE;
	}
	
	public function getVendorOwnerFromDirectories($did) {
		$query = $this->db->select('OWNER_ID as OID')->from('Directories')->where('DIRECTORY_Type',2)->where('DIRECTORY_ID',$did)->get();
		return ($query) ? $query->row()->OID : FALSE;
	}
	
	public function getAllVendorsForFilter() {
		$sql = 'SELECT VENDOR_Name as Name,VENDOR_ID as ID FROM Vendors ORDER BY Vendor_Name ASC';
		$query = $this->db->query($sql);
		return ($query) ? $query->result() : FALSE;	
	}
	
	public function getVendorFilterOptions($vid) {
		$options = '';
		if($vid > 0) {
			$contacts = $this->filterVendorContactsByVendorID($vid);
		}else {
			$contacts = $this->getAllVendorContacts();	
		}
		
		if(!empty($contacts)) {
			foreach($contacts as $contact) {
				$options .= '<option value="' . $contact->ContactID . '">' . $contact->FirstName . ' ' . $contact->LastName . '</option>';	
			}
		}
		
		return $options;
	}
	
	
	public function filterVendorContactsByVendorID($vid) {
		$sql = 'SELECT 
				d.DIRECTORY_ID as ContactID,
				d.OWNER_ID as OwnerID,
				d.JobTitle as JobTitle,
				(SELECT TYPE_Name FROM xContactType WHERE TYPE_ID = d.DIRECTORY_Type) as TypeLabel,
				d.DIRECTORY_Type as OwnerType,
				d.DIRECTORY_FirstName as FirstName,
				d.DIRECTORY_LastName as LastName,
				(SELECT PHONE_Number FROM PhoneNumbers WHERE DIRECTORY_ID = d.DIRECTORY_ID AND PHONE_Primary = 1 AND PHONE_Active = 1 ORDER BY PHONE_Created DESC LIMIT 1) AS PhoneNumber,
				(SELECT EMAIL_Address FROM EmailAddresses WHERE DIRECTORY_ID = d.DIRECTORY_ID AND EMAIL_Primary = 1 AND EMAIL_Active = 1 ORDER BY EMAIL_Created DESC LIMIT 1) As EmailAddress
				FROM Directories d
				WHERE d.DIRECTORY_Type = "' . 2 . '" AND d.OWNER_ID = "' . $vid . '"';
				
		$query = $this->db->query($sql);
		return ($query) ? $query->result() : FALSE;
	}
	
	public function getAllVendorContacts() {
		$sql = 'SELECT 
				d.DIRECTORY_ID as ContactID,
				d.OWNER_ID as OwnerID,
				d.JobTitle as JobTitle,
				(SELECT TYPE_Name FROM xContactType WHERE TYPE_ID = d.DIRECTORY_Type) as TypeLabel,
				d.DIRECTORY_Type as OwnerType,
				d.DIRECTORY_FirstName as FirstName,
				d.DIRECTORY_LastName as LastName,
				(SELECT PHONE_Number FROM PhoneNumbers WHERE DIRECTORY_ID = d.DIRECTORY_ID AND PHONE_Primary = 1 AND PHONE_Active = 1 ORDER BY PHONE_Created DESC LIMIT 1) AS PhoneNumber,
				(SELECT EMAIL_Address FROM EmailAddresses WHERE DIRECTORY_ID = d.DIRECTORY_ID AND EMAIL_Primary = 1 AND EMAIL_Active = 1 ORDER BY EMAIL_Created DESC LIMIT 1) As EmailAddress
				FROM Directories d
				WHERE d.DIRECTORY_Type = "' . 2 . '"';
				
		$query = $this->db->query($sql);
		return ($query) ? $query->result() : FALSE;
	}
	
	public function getContactType($did) {
		$query = $this->db->select('DIRECTORY_Type as Type')->from('Directories')->where('DIRECTORY_ID',$did)->get();
		return ($query) ? $query->row()->Type : FALSE;	
	}
	
	public function updateDirectoryInformation($did,$data) {
		$sql = 'UPDATE Directories SET DIRECTORY_FirstName = "' . $data['DIRECTORY_FirstName'] . '",DIRECTORY_LastName="' . $data['DIRECTORY_LastName'] . '",DIRECTORY_Type = "' . $data['DIRECTORY_Type'] . '",OWNER_ID = "' . $data['OWNER_ID'] . '",TITLE_ID = "' . $data['TITLE_ID'] . '", JobTitle = "' . $data['JobTitle'] . '",DIRECTORY_Notes = "' . $data['DIRECTORY_Notes'] . '" WHERE DIRECTORY_ID = "' . $did . '"';
		$query = $this->db->query($sql);
		return ($query) ? TRUE : FALSE;	
	}
	
	public function checkIfContactPhoneExists($did,$oid,$type) {
		$query = $this->db->select('*')->from('PhoneNumbers')->where('DIRECTORY_ID',$did)->where('OWNER_ID',$oid)->where('OWNER_Type',$type)->get();
		return ($query) ? TRUE : FALSE;	
	}
	
	public function getOwnerIDByDID($did) {
		$query = $this->db->select('OWNER_ID as OID')->from('Directories')->where('DIRECTORY_ID',$did)->get();
		return ($query) ? $query->row()->OID : FALSE;	
	}
	
	public function getOwnerTypeBYDID($did) {
		$query = $this->db->select('DIRECTORY_Type as Type')->from('Directories')->where('DIRECTORY_ID',$did)->get();
		return ($query) ? $query->row()->Type : FALSE;	
	}
	
	private function _directoryColumns() {
		return 'd.DIRECTORY_ID as ContactID,
				d.OWNER_ID as OwnerID,
				d.TITLE_ID as TitleID,
				ti.TITLE_Name as TitleName,
				d.JobTitle as JobTitle,
				d.DIRECTORY_Type as OwnerType,
				d.DIRECTORY_FirstName as FirstName,
				d.DIRECTORY_LastName as LastName,
				d.DIRECTORY_Notes as Notes,
				t.TAG_ClassName as ClassName';
	}
	
	public function getDirectoryInformation($type = false,$id = false,$did = false) {
		$select = 'd.DIRECTORY_ID as ContactID,
				   d.OWNER_ID as OwnerID,
				   d.TITLE_ID as TitleID,
				   ti.TITLE_Name as TitleName,
				   d.JobTitle as JobTitle,
				   d.DIRECTORY_Type as OwnerType,
				   d.DIRECTORY_FirstName as FirstName,
				   d.DIRECTORY_LastName as LastName,
				   d.DIRECTORY_Notes as Notes,
				   t.TAG_ClassName as ClassName';
				   
		$this->db->select($select)->from('Directories d')->join('xTitles ti','d.TITLE_ID = ti.TITLE_ID')->join('xTags t','d.DIRECTORY_Tag = t.TAG_ID');
		
		if(!$did) {
			
			if($type == 2) { 
				$this->db->where('d.DIRECTORY_Type',2)->where('d.OWNER_ID',$id);
			}elseif($type == 1) {
				$this->db->where('d.DIRECTORY_Type',1)->where('d.OWNER_ID',$id);
			}else {
				if($type == 3) {
					$this->db->where('d.DIRECTORY_Type',3)->where('d.OWNER_ID',$id);
				}else {
					$this->db->where('d.DIRECTORY_Type',4)->where('d.OWNER_ID',$id);	
				}
			}
		}else {
			$this->db->where('d.DIRECTORY_ID',$did);	
		}
		
		$query = $this->db->get();
		return ($query) ? $query->result() : FALSE;
	}
	
	public function getDirectoryInfoForEmail($did) {
		$query = $this->db->select('*')->from('Directories')->where('DIRECTORY_ID',$did)->get();
		return ($query) ? $query->row() : FALSE;
	}
	
	public function getStraitContact($did) {
		$sql = 'SELECT d.DIRECTORY_ID as ContactID,
				   d.OWNER_ID as OwnerID,
				   d.TITLE_ID as TitleID,
				   d.JobTitle as JobTitle,
				   d.DIRECTORY_Type as OwnerType,
				   d.DIRECTORY_FirstName as FirstName,
				   d.DIRECTORY_LastName as LastName,
				   d.DIRECTORY_Notes as Notes,
				   FROM Directories d WHERE d.DIRECTORY_ID = "' . $did . '"';
		$contact_query = $this->db->query($sql);
		if($contact_query) {
			$contact = $contact_query->row();
			
			/*
			//get phone numbers
			$phone_sql = 'SELECT * FROM PhoneNumbers WHERE DIRECTORY_ID = "' . $did . '"';
			$phone_query = $this->db->query($phone_sql);
			if($phone_query) {
				$contact->Phones = $phone_query->result();	
			}
			
			$email_sql = 'SELECT * FROM EmailAddresses WHERE DIRECTORY_ID = "' . $did . '"';
			$email_query = $this->db->query($email_sql);
			if($email_query) {
				$contact->Emails = $email_query->result();	
			}
			
			$physical_address_sql = 'SELECT * FROM DirectoryAddresses WHERE DIRECTORY_ID = "' . $did . '"';
			$physical_address_query = $this->db->query($physical_address_sql);
			if($physical_address_query) {
				$contact->Addresses = $physical_address_query->result();	
			}
			
			*/
			
			return $contact;
		}else {
			return FALSE;	
		}
	}
	
	public function preparePopupInfo($did,$type = false,$owner = false) {
		
		$directory = $this->getDirectoryInformation((($type) ? $type : false),(($owner) ? $owner : false),$did);
		
		//were only expecting one, but it returns as an array so we need to push the one to the contact array.
		foreach($directory as $myContact) :
			if($myContact->OwnerType > 2 OR $did > 0) {
				$myContact->Phones = $this->getContactPhoneNumbers(false,false,$myContact->ContactID);
				$myContact->Emails = $this->getContactEmailAddresses(false,false,$myContact->ContactID);
				$myContact->Addresses = $this->getContactPhysicalAddresses(false,false,$myContact->ContactID);
			}else {
				$myContact->Phones = $this->getContactPhoneNumbers($myContact->OwnerID,$myContact->OwnerType);	
				$myContact->Emails = $this->getContactEmailAddresses($myContact->OwnerID,$myContact->OwnerType);
				$myContact->Addresses = $this->getContactPhysicalAddresses($myContact->OwnerID,$myContact->OwnerType);
			}
		endforeach;
		
		//were only expecting one result, but it returns as an array, so just return the first index. which is always 0
		if(count($directory) > 0) {
			return $directory[0];
		}else {
			return $directory;	
		}
	}
	
	function getContactsForTable($owner_type = false,$owner_id = false) {
		
	}
	
	function getAllClientsInAgency($aid) {
		$clients = array();
		$aQuery = $this->db->select('GROUP_ID as ID')->from('Groups')->where('AGENCY_ID',$aid)->get();
		if($aQuery) {
			$groups = $aQuery->result();
			foreach($groups as $group) {
				$cQuery = $this->db->select('CLIENT_ID as ID,GROUP_ID as GID')->from('Clients')->where('GROUP_ID',$group->ID)->get();
				if($cQuery) {
					$clientGroup = $cQuery->result();	
					foreach($clientGroup as $client) {
						array_push($clients,$client);	
					}
				}
			}
		}
		
		if(!empty($clients)) {
			return $clients;	
		}else {
			return FALSE;	
		}
	}
	
	public function insertBlankAddress($oid,$otype,$did) {
		$data = array(
			'DIRECTORY_ID'=>$did,
			'OWNER_ID' => $oid,
			'OWNER_Type'=>$otype,
			'ADDRESS_Primary'=>1,
			'ADDRESS_Created'=>date('Y-m-d H:i:s'),
			'ADDRESS_Type'=>'Work'
		);
		return ($this->db->insert('DirectoryAddresses',$data)) ? $this->db->insert_id() : FALSE;
	}
	
	public function getAllClientsInGroup($gid) {
		$clients = array();
		$gQuery = $this->db->select('CLIENT_ID as ID,GROUP_ID as GID')->from('Clients')->where('GROUP_ID',$gid)->get();
		if($gQuery) {
			$clientsGroup = $gQuery->result();
			foreach($clientsGroup as $client) {
				array_push($clients,$client);	
			}
		}
		
		if(!empty($clients)) {
			return $clients;
		}else {
			return FALSE;	
		}
			
	}
	
	public function getClientInfo($cid) {
		$query = $this->db->select('CLIENT_ID as ID,GROUP_ID as GID')->from('Clients')->where('CLIENT_ID',$cid)->get();	
		if($query) {
			return $query->result();	
		}else {
			return FALSE;	
		}
	}
		
	public function queryChangerForAnythingOtherThanClientLevel($id = false) {
		if(!$id) :
			$_level_type = $this->user['DropdownDefault']->LevelType;
			switch($_level_type) :
				case 'a' : $query = $this->getAllClientsInAgency($this->user['DropdownDefault']->SelectedAgency); break;
				case 'g' : $query = $this->getAllClientsInGroup($this->user['DropdownDefault']->SelectedGroup); break;
				default : $query = $this->getClientInfo($this->user['DropdownDefault']->SelectedClient); break;
			endswitch;
		else :
			$query = $this->getClientInfo($id);
		endif;	
		
		return $query;
	}
	
	public function getUsersAsContacts($cid) {
		$query = $this->db->select('USER_ID as ID')->from('Users_Info')->where('CLIENT_ID',$cid)->get();
		return ($query) ? $query->result() : FALSE;
	}
	
	public function getVendorsFromClientWebsite($cid) {
		$query = $this->db->select('WEB_Vendor as ID')->from('Websites')->where('OWNER_ID',$cid)->where('OWNER_Type',1)->get();
		return ($query) ? $query->result() : FALSE;
	}
	
	public function getVendorsByID($vid) {
		$query = $this->db->select('VENDOR_ID as ID,VENDOR_Name as VendorName')->from('Vendors')->where('VENDOR_ID',$vid)->get();
		return ($query) ? $query->result() : FALSE;	
	}
	
	public function getDealershipName($cid) {
		$query = $this->db->select('CLIENT_Name as Dealership')->from('Clients')->where('CLIENT_ID',$cid)->get()->row();
		return ($query) ? $query : FALSE;	
	}
	
	public function getVendorName($vid) {
		$query = $this->db->select('VENDOR_Name as Vendor')->from('Vendors')->where('VENDOR_ID',$vid)->get();
		return ($query) ? $query->result() : FALSE;	
	}
	
	public function getVendorNameAsText($vid) {
		$sql = 'SELECT VENDOR_Name as Name FROM Vendors WHERE VENDOR_ID = "' . $vid . '"';
		$query = $this->db->query($sql);
		return ($query) ? $query->row()->Name : FALSE;	
	}
	
	public function getGroupLevelContacts($gid) {
		$groupContacts = $this->db->select($this->_directoryColumns())->from('Directories d')->join('xTitles ti','d.TITLE_ID = ti.TITLE_ID')->join('xTags t','d.DIRECTORY_Tag = t.TAG_ID')->where('DIRECTORY_Type',8)->where('OWNER_ID',$gid)->order_by('DIRECTORY_LastName','asc')->get();
		return ($groupContacts) ? $groupContacts->result() : FALSE;
	}
	
	public function getAgencyLevelGroupContacts($aid) {
		$myContacts = array();
		$agencies = $this->db->select('GROUP_ID as GID')->from('Groups')->where('AGENCY_ID',$aid)->get();
		if($agencies) {
			$a = $agencies->result();
			foreach($a as $agents) {
				$contacts = $this->getGroupLevelContacts($agents->GID);
				foreach($contacts as $contact) {
					array_push($myContacts,$contact);	
				}
			}
		}
		return $myContacts;
	}
	
	public function _getGroupLevelContacts($cid = false) {
		$level = $this->user['DropdownDefault']->LevelType;
		switch($level) {
			case 'g' : 
				return $this->getGroupLevelContacts($this->user['DropdownDefault']->SelectedGroup);
			break;
			case 'a' :
				return $this->getAgencyLevelGroupContacts($this->user['DropdownDefault']->SelectedAgency);
			break;
			default:
				return $this->getGroupLevelContacts($this->user['DropdownDefault']->SelectedGroup);
			break;	
		}
	}
	
	public function getGroupContactOwner($gid) {
		$query = $this->db->select('GROUP_Name as Name')->from('Groups')->where('GROUP_ID',$gid)->get();
		return ($query) ? $query->row()->Name : FALSE;	
	}
	
	
	
	//get websites
	public function buildContactTable($id = false, $is_vendor = false) {
		$contacts = array();
		if(!$is_vendor) { 
			$parent = $this->queryChangerForAnythingOtherThanClientLevel($id);
			if(!empty($parent)) {
				foreach($parent as $owner) {
					$sys = $this->getSystemContacts(1,$owner->ID);
					$users = $this->getUsersAsContacts($owner->ID);
					$vendors = $this->getVendorsFromClientWebsite($owner->ID);
					if(!empty($users)) {
						foreach($users as $user) {
							$uContact = $this->getSystemContacts(3,$user->ID);	
							if($uContact) {
								foreach($uContact as $userContact) {
									$userContact->Owner = $this->getDealershipName($owner->ID)->Dealership;
									array_push($contacts,$userContact);	
								}
							}
						}
					}
					if(!empty($vendors)) {
						foreach($vendors as $vendor) {
							$vContact = $this->getSystemContacts(2,$vendor->ID);	
							if($vContact) {
								foreach($vContact as $vendorContact) {
									$vendorContact->Owner = $this->getVendorNameAsText($vendorContact->OwnerID);
									array_push($contacts,$vendorContact);	
								}
							}
						}
					}
					if(!empty($sys)) {
						foreach($sys as $c) {
							$c->Owner = $this->getDealershipName($owner->ID)->Dealership;
							array_push($contacts,$c);	
						}
					}
				}
				$groupContacts = $this->_getGroupLevelContacts();
				foreach($groupContacts as $gUnit) {
					$gUnit->Owner = $this->getGroupContactOwner($gUnit->OwnerID);
					array_push($contacts,$gUnit);
				}
			}
		}else {
			$vendors = $this->getVendorsByID($id);
			if(!empty($vendors)) {
				foreach($vendors as $vendor) {
					$vContact = $this->getSystemContacts(2,$vendor->ID);
					if($vContact) {
						foreach($vContact as $vendorContact) {
							$vendorContact->Owner = $vendor->VendorName;
							array_push($contacts,$vendorContact);	
						}
					}
				}
			}
		}
			
		if(!empty($contacts)) { return $contacts; }else { return FALSE; }
	}	
	
	//Should cover all contacts
	public function getSystemContacts($type,$id) {
		$directories = $this->getDirectoryInformation($type,$id);
		if($directories) { 
			foreach($directories as $directory) {
				//oid,otype,did
				$directory->Phones = $this->getContactPhoneNumbers($directory->OwnerID,$directory->OwnerType,$directory->ContactID);	
				$directory->Emails = $this->getContactEmailAddresses($directory->OwnerID,$directory->OwnerType,$directory->ContactID);
				$directory->Addresses = $this->getContactPhysicalAddresses($directory->OwnerID,$directory->OwnerType,$directory->ContactID);
			}
		}
		return $directories;
	}
		
	//get user information from directory
	function getUserDirectoryInfo($uid) {		
		//query using active record codeigniter method
		$query = $this->db->select('d.DIRECTORY_ID as did,d.DIRECTORY_FirstName as FirstName,d.DIRECTORY_LastName as LastName,d.DIRECTORY_Tag as tid,d.OWNER_ID as OwnerID,d.DIRECTORY_Type as OwnerType,t.TAG_ClassName as ClassName')->
				 from('Directories d')->
				 join('xTags t','d.DIRECTORY_Tag = t.TAG_ID')->
				 where('d.OWNER_ID',$uid)->
				 where('d.DIRECTORY_Type',3)->
				 get();
		
		//we should only find one result because the user id is unique to the user so we only return the row()
		//if we are returned a match we return the object back to the caller.
		return ($query) ? $query->row() : FALSE;
	}
	
	function getUserContactInfo($uid) {
		//collection array
		$contact_info = array();
		
		//directory information for the user
		$directory_info = $this->getUserDirectoryInfo($uid);
		
		//if the directory information is found and results are returned
		if(!empty($directory_info)) {
			//users contact numbers
			$phoneNumbers   = (($this->getContactPhoneNumbers($directory_info->OwnerID,3))      ? $this->getContactPhoneNumbers($directory_info->OwnerID,3)         : FALSE);
			//users email addresses
			$emails 	    = (($this->getContactEmailAddresses($directory_info->OwnerID,3))    ? $this->getContactEmailAddresses($directory_info->OwnerID,3)    : FALSE);
			//users physical addresses
			$address 		= (($this->getContactPhysicalAddresses($directory_info->OwnerID,3)) ? $this->getContactPhysicalAddresses($directory_info->OwnerID,3) : FALSE);
			
			$contact_info['directory'] = $directory_info;
			$contact_info['phones']    = (!empty($phoneNumbers)) ? $phoneNumbers : FALSE;
			$contact_info['address']   = (!empty($address))      ? $address      : FALSE;
			$contact_info['emails']    = (!empty($emails))       ? $emails       : FALSE;
			
			//return the info back to the caller
			return (!empty($contact_info)) ? $contact_info : FALSE;
		}else {
			return FALSE;
		}
	}
	
	function getVendorDirectoryInfo($vid) {
		//the where array, multiple where statements require the arguments to be presented from an array.
		$where = array('OWNER_ID'=>$vid,'DIRECTORY_Type',2);
		
		//query using the active record codeigniter method
		$query = $this->db->select('d.DIRECTORY_FirstName as FirstName,d.DIRECTORY_LastName as LastName,d.DIRECTORY_Tag as tid')->where($where)->get();
		
		//we should only find one result because the vendor id is unique to the vendor so we only return the row()
		//if we are returned a match we return the object back to the caller.
		return ($query) ? $query->row() : FALSE;
	}
	
	//get contact phone numbers as object (could be multiple, could be none or one)
	function getContactPhoneNumbers($oid,$otype,$did = false) {
		//query using the active record codeigniter method
		$this->db->select('*')->from('PhoneNumbers');
		
			$this->db->where('PHONE_Active',1);
			
			if($did AND $did > 0) {
				$this->db->where('DIRECTORY_ID',$did);	
			}else {
				$this->db->where('OWNER_ID',$oid);
				$this->db->where('OWNER_Type',$otype);
				$this->db->where('DIRECTORY_ID',0);
			}
		
		$query = $this->db->get();
		
		//we return the results back to the caller...if found return the object, if not return FALSE		 
		return ($query) ? $query->result() : FALSE;
	}
	
	//get contact phone numbers as object (could be multiple, could be none or one)
	function getSingleContactPhoneNumber($pid) {
		//query using the active record codeigniter method
		$query = $this->db->select('*')->
				 from('PhoneNumbers')->
				 where('PHONE_ID',$pid)->
				 get();
		
		//we return the results back to the caller...if found return the object, if not return FALSE		 
		return ($query) ? $query->row() : FALSE;
	}
	
	//update the phone number in question with the changes the user made from the front end form.
	function updateSingleContactPhoneNumber($pid,$data) {
		$this->db->where('PHONE_ID',$pid);
		return ($this->db->update('PhoneNumbers',$data)) ? TRUE : FALSE;
	}
	
	//update the phone number in question with the changes the user made from the front end form.
	function addSingleContactPhoneNumber($data) {
		return ($this->db->insert('PhoneNumbers',$data)) ? TRUE : FALSE;
	}
	
	function addSingleEmailAddress($data) {
		return ($this->db->insert('EmailAddresses',$data)) ? TRUE : FALSE;	
	}
	
	//update the phone number in question with the changes the user made from the front end form.
	function updateSingleEmailAddress($eid,$data) {
		$this->db->where('EMAIL_ID',$eid);
		return ($this->db->update('EmailAddresses',$data)) ? TRUE : FALSE;
	}
	
	//get contact email addresses as object (could be multiple, could be none or one)
	function getContactEmailAddresses($oid,$otype,$did = false) {
		$this->db->select('*')->from('EmailAddresses');
		if(!$did) {
			$this->db->where('OWNER_ID',$oid);
			$this->db->where('OWNER_Type',$otype);
			$this->db->where('EMAIL_Active',1);
		}else {
			$this->db->where('DIRECTORY_ID',$did);	
		}
		
		$query = $this->db->get();	
				 
		return ($query) ? $query->result() : FALSE;
	}
	
	//get contact email addresses as object (could be multiple, could be none or one)
	function getSingleContactEmailAddress($eid) {
		$query = $this->db->select('*')->
				 from('EmailAddresses')->
				 where('EMAIL_ID',$eid)->
				 where('EMAIL_Active',1)->
				 get();	
				 
		return ($query) ? $query->row() : FALSE;
	}
	
	//get contact physical address as object (could be multiple, could be none or one)
	function getContactPhysicalAddresses($oid,$otype,$did = false) {
		$this->db->select('*')->from('DirectoryAddresses');
		if(!$did) {
			$this->db->where('OWNER_ID',$oid);
			$this->db->where('OWNER_Type',$otype);
			$this->db->where('ADDRESS_Active',1);
		}else {
			$this->db->where('DIRECTORY_ID',$did);	
		}
		$query = $this->db->get();
				 
		return ($query) ? $query->result() : FALSE;	
	}
	
	function getDefaultContacts() {
		//empty container array
		$contacts = array();
		
		//client level contacts
		$clientLevel = $this->getClientContacts();
		
		//as long as the clientLevel didnt return an empty result we should run this loop
		if(!empty($clientLevel)) {
			//loop through client contacts and toss them into the container array
			foreach($clientLevel as $contact) { array_push($contacts,$contact); }
		}
		
		//vendor contacts
		$vendorLevel = $this->getVendorContacts($client->ClientID);
		
		//as long as the vendors didnt return an empty result we shoud run this loop.
		if(!empty($vendorLevel)) {
			//loop through vendor contacts and toss them into the container array
			foreach($vendorLevel as $vendor) { array_push($contacts,$vendor); }
		}
		
		//return all contacts
		return $contacts;
	}
	
	function getUserContacts() {
		$userTypeID = 3;
	}
	
	function getVendorContacts($cid) {
		//keep this at the top incase it changes, we wont have to go searching through code.
		$vendorTypeID = 2;
		
		$websites = $this->getOwnerWebsites($cid,$vendorTypeID);		
		//contact collection array
		$contacts = array();
		
		foreach($websites as $website):
			$vendorQuery = $this->buildDirectory(2,$website->VendorID);
			
			//if the websites has results from the db
			if($vendorQuery) {
				$vendorContacts = $vendorQuery->result();
				
				//loop through each vendor and collect them in our contacts array
				foreach($vendorContacts as $vendorContact) : $vendorContact->ClientORVendorName = $vendorContact->VendorName; array_push($clientContacts,$vendorContact); endforeach;
			}
		endforeach;
		
		//return all contacts from function.
		return $contacts;
	}
	
	//All contacts in the system based on the dealer dropdown
	function getContacts($type = false,$specific_owner_id = false) { 
	
		//instead of querying strings we can just query a table and look for an id. i know that the id for CID: is 1
		if(!$type) {
			$TypeID = 1;
		}else {
			$TypeID = $type;	
		}
	
		//this gets all clients from agencies,groups, or clients no matter what level your on
		//if your on agency, it gets every group in that agency along with every client in that group
		$myParentContainer = $this->adminQueries->getClientsByDDLevel();
		
		//empty array for all contacts to return 
		$myContacts = array();
		
		//loop through each client and get the contacts associated to the client from the db.
		//we have to grab them by the contact type. each contact has a column type string (ex:CID:1, VID:1)
		foreach($myParentContainer as $parent) :
			
			//get each contact on the client level (CID);
			$levelContacts = array();
			
			$myQuery = $this->buildDirectory($TypeID,($specific_owner_id) ? $specific_owner_id : 1);					 
			if($myQuery) {
				$contacts = $myQuery->result();
				
				foreach($contacts as $contact) : 
					$contact->ClientORVendorName = $myParentContainer->Name ; 
					array_push($contacts,$contact); 
				endforeach;
				
				//as long as the clientContacts didnt return empty, we push it anyway.
				if(!empty($contacts)) {
					array_push($myContacts,$contacts);	
				}
			}
			
		endforeach;
		
		//return contacts to the system
		return $contacts;
	}
	
	function buildDirectory($type,$owner) {
		$where = array('OWNER_ID'=>$ownder,'TYPE_ID'=>$type);
		$select = 'cl.DIRECTORY_ID as DirectoryID,
				   d.DIRECTORY_Type as ContactType,
				   d.DIRECTORY_FirstName as FirstName,
				   d.Directory_LastName as LastName,
				   d.DIRECTORY_Primary_Email as PrimaryEmail,
				   d.DIRECTORY_Primary_Phone as PrimaryPhone,
				   t.TITLE_Name as JobTitle,
				   type.TYPE_Name as ContactType';
				   
		$sql = $this->db->select($select)->
			   from('ContactLinkage cl')->
			   join('Directories d','d.DIRECTORY_ID = ci.DIRECTORY_ID')->
			   join('xTitles t','d.TITLE_ID = t.TITLE_ID')->
			   join('xContactType type','cl.TYPE_ID = type.TYPE_ID')->
			   where($where)->
			   get();
			   
		return ($sql) ? $sql->result() : FALSE;
	}
	
	
	//we need to get the websites associated to the parent to get the vendors contacts
	function getOwnerWebsites($oid,$type) {
		$sql = 'SELECT WEB_Vendor as VendorID FROM Websites WHERE OWNER_Type = "' . $type . '" AND OWNER_ID = "' . $oid . '";';
		$query = $this->db->query($sql);
		return ($query) ? $query->result() : FALSE;
	}
	
	function updatePrimaryPhone($pid,$did,$primary) {
		
		$all_update = array(
			'PHONE_Primary'=>0,
		);
		
		$new_primary = array(
			'PHONE_Primary'=>1
		);
		
		$directory_id = $did;
		
		$get_did = $this->db->select('DIRECTORY_ID')->from('PhoneNumbers')->where('PHONE_ID',$pid)->get();
		if($get_did) {
			$directory_id = $get_did->row()->DIRECTORY_ID;	
		}
		
		$this->db->where('DIRECTORY_ID',$directory_id);
		$reset_primaries = $this->db->update('PhoneNumbers',$all_update);
		
		if($reset_primaries) {
			$this->db->where('PHONE_ID',$pid);
			return ($this->db->update('PhoneNumbers',$new_primary)) ? TRUE : FALSE;
		}else {
			return FALSE;	
		}
	}
	
	function updatePrimaryEmail($eid,$did,$primary) {
		$reset_primary = 'UPDATE EmailAddresses SET EMAIL_Primary = "0" WHERE OWNER_ID = "' . $did . '"';
		$reset_query = $this->db->query($reset_primary);
		if($reset_query) {
			$sql = 'UPDATE EmailAddresses SET EMAIL_Primary = "' . $primary . '" WHERE EMAIL_ID = "' . $eid . '"';
			$query = $this->db->query($sql);
			if($query) {
				return TRUE;	
			}
		}else {
			return FALSE;	
		}
	}
	
	//used only for users
	function managePrimaryPhysicalAddress($did,$owner_id,$type,$data) {
		$checkIfExists = $this->db->select('ADDRESS_ID')->from('DirectoryAddresses')->where('DIRECTORY_ID',$did)->where('ADDRESS_Primary',1)->get();
		if($checkIfExists) {
			$this->db->where('DIRECTORY_ID',$did)->where('OWNER_ID',$owner_id)->where('OWNER_Type',$type);
			return ($this->db->update('DirectoryAddresses',$data)) ? TRUE : FALSE;	
		}else {
			$data['OWNER_ID'] = $owner_id;
			$data['DIRECTORY_ID'] = $did;
			$data['OWNER_Type'] = $type;
			$data['ADDRESS_Primary'] = 1;
			$data['ADDRESS_Created'] = date('Y-m-d H:i:s');
			return ($this->db->insert('DirectoryAddresses',$data)) ? TRUE : FALSE;
		}
	}
}
	
