<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Domanalytics extends DOM_Model {
	
	var $client_id;
	var $report_data;
	
	public function __construct() {
		// Call the Model constructor
		parent::__construct();
		$this->load->helper('analytics');
	}
	
	public function checkWebID($wid,$cid) {
		$query = $this->db->select('WEB_ID')->from('Websites')->where('OWNER_ID',$cid)->where('OWNER_Type',1)->where('WEB_ID',$wid)->get();
		return ($query) ? $query->row() : FALSE;
	}
	
	public function getClientFirstWebsite($cid) {
		$query = $this->db->select('WEB_ID')->from('Websites')->where('OWNER_ID',$cid)->where('OWNER_Type',1)->where('WEB_Active',1)->order_by('WEB_ID','asc')->limit(1)->get();
		return ($query) ? $query->row()->WEB_ID : FALSE;		
	}
	
	public function getAllWebsites($web_id = false) {
		$select = 'w.WEB_ID as WebID,
				   w.WEB_GoogleProfileID as AnalyticsProfileID,
				   w.WEB_Url as URL';
		if($web_id) {
			$query = $this->db->select($select)->from('Websites w')->where('WEB_ID',$web_id)->get();
			return ($query) ? $query->row() : FALSE;
		}else {
			$query = $this->db->select($select)->from('Websites w')->where('OWNER_ID',$this->user['DropdownDefault']->SelectedClient)->where('OWNER_Type',1)->where('WEB_GoogleProfileID >',0)->get();
			return ($query) ? $query->result() : FALSE;
		}
	}
	
	/*GET ALL WEBSITES FROM THE CLIENT*/
	public function getClientWebsite($web_id,$limit = true) {
		$select = 'w.WEB_ID as WebID,
				   w.WEB_GoogleProfileID as AnalyticsProfileID,
				   w.WEB_Url as URL,
				   ml.CMS_Vendor_Link,
				   cmsv.VENDOR_Name as CMS,
				   v.VENDOR_Name as HostingProvider';
		
		if(!$web_id) :   
			$where_clause = "OWNER_ID = '" . $this->user['DropdownDefault']->SelectedClient . "' AND OWNER_Type = '1' AND WEB_Active = '1' AND WEB_GoogleProfileID > '0'";
		else :
			$where_clause = "WEB_ID = '" . $web_id . "'";
		endif;
		
		if($limit) {
			$web_query = $this->db->select($select)->
						 from('Websites w')->
						 join('Vendors v','w.WEB_Vendor = v.VENDOR_ID')->
						 join('MasterList ml','ml.WEB_ID = w.WEB_ID')->
						 join('Vendors cmsv','ml.CMS_Vendor_ID = cmsv.VENDOR_ID')->
						 where($where_clause)->
						 limit(1)->
						 get();
		}else {
			$web_query = $this->db->select($select)->
						 from('Websites w')->
						 join('Vendors v','w.WEB_Vendor = v.VENDOR_ID')->
						 join('MasterList ml','ml.WEB_ID = w.WEB_ID')->
						 join('Vendors cmsv','ml.CMS_Vendor_ID = cmsv.VENDOR_ID')->
						 where($where_clause)->
						 get();
		}
		
		//return the web results if the website has analytics hooked up.
		//if the website doesnt have analytics, it will not return the website.
		//if the site needs analytics and isnt returning, make sure the GoogleProfileID is
		//filled for the client in question. The return feeds off of this, Notice the where
		//statement. (WEB_GoogleProfileID > 0) the default is 0, no website will have the
		//default of zero listed that has google analytics. It will look like a 
		//6 digit number you have access to in the Google Analytic dashboard.
		//Under admin/settings.
		return ($web_query) ? $web_query->row() : FALSE;
	}
	
	public function buildTable() {
		$selected_client = $this->user['DropdownDefault']->SelectedClient;
		//GRAB THE CLIENTS WEBSITES
		$where_clause = "OWNER_ID = '" . $selected_client . "' AND OWNER_Type = '1' AND WEB_Active = '1' AND WEB_GoogleProfileID > '0'";
		$web_query = $this->db->select('WEB_ID')->from('Websites')->where($where_clause)->get();	
	}
	
	/*
		USED TO POPULATE DROPDOWNS FOR CHART BUILDING
	 */
	public function getAllDimensions() {
		$query = $this->db->select('*')->from('xGoogleDimensions')->where('DIMENSION_Active',1)->get();
		return ($query) ? $query->result() : FALSE;
	}
	
	/*
		USED TO POPULATE DROPDOWNS FOR CHART BUILDING
	 */
	public function getAllMetrics() {
		$query = $this->db->select('*')->from('xGoogleMetrics')->where('METRIC_Active',1)->get();
		return ($query) ? $query->result() : FALSE;
	}
	
	/*
		USED TO POPULATE DROPDOWNS FOR CHART BUILDING
	 */
	public function getAllCategories() {
		$query = $this->db->select('*')->from('xGoogleAnalyticCategories')->where('CAT_Active',1)->get();
	}
	
	/*
		GETS ALL THE DEFAULT REPORTS OF THE SYSTEM
	 */
	public function getDefaultReports($google_profile_id = false) {
		$query = $this->db->select('*')->from('DefaultAnalytics')->where('REPORT_Active',1)->order_by('REPORT_Order','asc')->get();
		if($query) {
			$reports = $query->result();
			foreach($reports as $report) :
				$metrics   = $this->getReportMetricsFromString($report->REPORT_Metric);
				$dimension = $this->getReportDimensionsFromString($report->REPORT_Dimension);
				$report->REPORT_Metric    = (!empty($metrics)) ? $metrics : FALSE;
				$report->REPORT_Dimension = (!empty($dimension)) ? $dimension : FALSE;
				$report->REPORT_Category  = $this->getReportCategoryFromString($report->REPORT_Category);
				$report->REPORT_StartDate = (($report->REPORT_StartDate == '0000-00-00') ? getOneMonthAgo() : $report->REPORT_StartDate);
				$report->REPORT_EndDate   = (($report->REPORT_EndDate == '0000-00-00') ? date('m/d/Y') : $report->REPORT_EndDate);
			endforeach;
		}
		return $reports;
	}
	
	/*
		PASS A STRING OF CATEGORY IDS AND GRAB THE CATEGORY NAMES
	 */
	 public function getReportCategoryFromString($str) {
		$str2arr = explode(',',$str); 
		//empty code
		$code = array();
		
		//loop through each category 
		foreach($str2arr as $arr) {
			$db_code = $this->getSingleCategoryName($arr);
			array_push($code,$db_code);	
		}
		
		//return the array
		return $code;
	 }
	
	/*
		PASS A STRING OF METRIC IDS AND GRAB THE CODE 
		TOSS THE CODE TO AN ARRAY AND RETURN THE ARRAY
	 */
	public function getReportMetricsFromString($str) {
		if($str != 'NULL') {
			$str2arr = explode(',',$str);
			//empty code
			$code = array();
			
			//loop through each metric in string and push code to array
			foreach($str2arr as $arr) {
				$db_code = $this->getSingleMetricCodeAndLabel($arr);
				array_push($code,$db_code);
			}
			
			//return the array
			return $code;
		}
	}
	
	/*
		PASS A STRING OF DIMENSION IDS AND GRAB THE CODE 
		TOSS THE CODE TO AN ARRAY AND RETURN THE ARRAY
	 */
	public function getReportDimensionsFromString($str) {
		if($str != 'NULL') {
			$str2arr = explode(',',$str);
			//empty code
			$code = array();
			
			//loop through each metric in string and push code to array
			foreach($str2arr as $arr) {
				$db_code = $this->getSingleDimensionCodeAndLabel($arr);
				array_push($code,$db_code);
			}
			
			//return the array
			return $code;
		}
	}
	
	/*
		GRABS THE GACODE FROM THE DB TO GENERATE A SINGLE REPORT BY THE ID 
	 */
	public function getSingleMetricCodeAndLabel($mid) {
		$query = $this->db->select('METRIC_Code as Code,METRIC_Name as Label')->from('xGoogleMetrics')->where('METRIC_ID',$mid)->where('METRIC_Active',1)->get();
		return ($query) ?  $query->row() : FALSE;
	}
	
	/*
		GRABS THE GACODE FROM THE DB TO GENERATE A SINGLE REPORT BY THE ID 
	 */
	public function getSingleDimensionCodeAndLabel($did) {
		$query = $this->db->select('DIMENSION_Code as Code,DIMENSION_Name as Label')->from('xGoogleDimensions')->where('DIMENSION_ID',$did)->where('DIMENSION_Active',1)->get();
		return ($query) ? $query->row() : FALSE;
	}
	
	/*
		GRABS THE GA Category FROM THE DB TO GENERATE A Header for the report 
	 */
	public function getSingleCategoryName($cid) {
		$query = $this->db->select('CAT_Name as Heading')->from('xGoogleAnalyticCategories')->where('CAT_ID',$cid)->where('CAT_Active',1)->get();
		return ($query) ? $query->row()->Heading : FALSE;
	}
	
	/*
		GET CLIENTS SAVED REPORTS OUT OF THE DB
	 */
	 public function getClientsCustomReports($client_id) {
		 
	 }
	 
	 /*
		SAVE CUSTOM REPORT
	  */
	  public function saveCustomReport($cliend_id,$report_data) {
		  			  
	  }
	  
}