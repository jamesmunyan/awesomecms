<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Google_analytics extends DOM_Model {
	
	public function __construct() {
		parent::__construct();	
	}
	
	public function get_api_credentials($cid) {
		$query = $this->db->select('*')->from('xGoogleAPIAccess')->where('CLIENT_ID',$cid)->get();
		return ($query) ? $query->row() : FALSE;	
	}
}