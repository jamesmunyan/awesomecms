<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Domheatmap extends DOM_Model {
	
	var $client_id;
	var $web_id;
		
	public function __construct() {
		// Call the Model constructor
		parent::__construct();
		$this->load->helper('heatmaping');
	}
	
	public function checkForHeatmap($web_id) {
		$query = $this->db->select('CLICK_ID')->from('HeatMapping')->where('WEB_ID',$web_id)->get();
		return ($query) ? $query->result() : FALSE;	
	}
	
	public function getGroupPaths($web_id) {
		$query = $this->db->select('CLICK_GroupName as PATH')->from('HeatMapping')->where('WEB_ID',$web_id)->group_by('CLICK_GroupName')->get();
		return ($query) ? $query->result() : FALSE;	
	}
	
	public function getClientWebsites($cid) {
		$query = $this->db->select('WEB_ID,WEB_Url')->from('Websites')->where('OWNER_ID',$cid)->where('OWNER_Type',1)->where('WEB_Active',1)->get();
		return ($query) ? $query->result() : FALSE;
	}
	
	public function getSingleSiteURL($web_id) {
		$query = $this->db->select('WEB_Url')->from('Websites')->where("WEB_ID",$web_id)->get();
		return ($query) ? $query->row()->WEB_Url : FALSE;	
	}
	
	public function log_click($data) {
		return (($this->db->insert('HeatMapping',$data)) ? TRUE : FALSE);	
	}
	
	public function getMaxY($web_id,$group) {
		$max = $this->db->select('MAX(CLICK_Y) as MaxY')->from('HeatMapping')->where('WEB_ID',$web_id)->where('CLICK_GroupName',$group)->get();
		return ($max) ? $max->row()->MaxY : FALSE;	
	}
	
	public function getMaxX($web_id,$group) {
		$max = $this->db->select('MAX(CLICK_X) as MaxX')->from('HeatMapping')->where('WEB_ID',$web_id)->where('CLICK_GroupName',$group)->get();	
	}
	
	public function getBaseURLFromString($str) {
		return parse_url($str);	
	}
	
	public function getWebID($url) {
		$query = $this->db->select('WEB_ID')->from('Websites')->like('WEB_URL',$url,'both')->get();
		return (($query) ? $query->row() : FALSE);	
	}
	
	public function getClicks($web_id) {
		$query = $this->db->select('*')->from('HeatMapping')->where('WEB_ID',$web_id)->get();
		return ($query) ? $query->result() : FALSE;
	}
	
	
}