<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Domwebsites extends DOM_Model {
    public function __construct() {
        // Call the Model constructor
        parent::__construct();
    }
	
	public function getUserWebsites($uid) {
		$query = $this->db->select('WEB_Url as URL')->from('Websites')->where('OWNER_ID',$uid)->where('OWNER_Type',3)->get();
		return ($query) ? $query->result() : FALSE;	
	}
	
	public function getWebsite($web_id) {
		$columns = 'WEB_ID as ID, 
					OWNER_ID as OwnerID,
					OWNER_Type as OwnerType,
					WEB_Url as URL,
					WEB_Notes as Notes,
					WEB_GoogleUACode as GoogleUACode,
					WEB_GoogleUA_Universal as UniversalCode,
					WEB_GoogleProfileID as AnalyticsProfileID,
					WEB_GoogleWebToolsMetaCode as GoogleWebToolsMetaCode,
					WEB_GooglePlusCode as GooglePlusCode,
					WEB_BingCode as BingCode,
					WEB_YahooCode as YahooCode,
					WEB_GlobalScript as GlobalScript,
					WEB_CallTrackingCode as CallTrackingCode,
					WEB_GoogleUA_Universal as GoogleUAUniversal,
					WEB_GoogleUA_Universal_Name as GoogleUAUniversalName,
					WEB_GoogleProfileID as GoogleProfileID,
					WEB_Vendor as VendorID';
		$query = $this->db->select($columns)->from('Websites')->where('WEB_ID',$web_id)->get();
		return ($query) ? $query->row() : FALSE;
	}
	
	//This is to build the listing tables
	public function getWebsites($oid = false,$oType = false, $did = false, $web_id = false) {
		//select statement
		$select = 'w.WEB_ID as ID, w.OWNER_ID as OwnerID,w.OWNER_Type as OwnerType,v.VENDOR_Name as VendorName,v.VENDOR_ID as VendorID,w.WEB_Url as URL,w.WEB_Notes as Notes';
		
		if($web_id) {
			//using the active record method, select and join the correct tables.
			$query = $this->db->select($select)->from('Websites w')->
					 join('Vendors v','w.WEB_Vendor = v.VENDOR_ID')->
					 where('w.WEB_ID',$web_id)->get();	
						
			return ($query) ? $query->row() : FALSE;
		}else {
			//using the active record method, select and join the correct tables.
			$query = $this->db->select($select)->from('Websites w')->
					 join('Vendors v','w.WEB_Vendor = v.VENDOR_ID')->
					 where('w.OWNER_ID',$oid)->
					 where('w.OWNER_Type',$oType)->
					 where('w.DIRECTORY_ID',$did)->get();	
						
			return ($query) ? $query->result() : FALSE;
		}
	}
	
	function fillDropdownVendors() {
		$query = $this->db->select('VENDOR_Name as VendorName,VENDOR_ID as VendorID')->from('Vendors')->where('VENDOR_Active',1)->get();
		return ($query) ? $query->result() : FALSE;	
	}
	
	public function addWebsite($formdata,$owner_id,$owner_type) {
		$data = array(
			'OWNER_ID' => $owner_id,
			'DIRECTORY_ID' => $formdata['directory_id'],
			'OWNER_Type'=>$owner_type,
			'WEB_Vendor' => $formdata['vendor'],
			'WEB_GoogleUACode'=>(($formdata['ua_type'] <= 0) ? $formdata['ua_code'] : ''),
			'WEB_GoogleUA_Universal'=>(($formdata['ua_type'] >= 1) ? $formdata['ua_code'] : ''),
			'WEB_GoogleProfileID'=>(($formdata['profile_id'] != '') ? $formdata['profile_id'] : ''),
			'WEB_GoogleWebToolsMetaCode'=>$formdata['meta_code_number'],
			'WEB_GooglePlusCode'=>$formdata['gplus_code'],
			'WEB_BingCode'=>$formdata['bing_code'],
			'WEB_YahooCode'=>$formdata['yahoo_code'],
			'WEB_GlobalScript'=>$formdata['global_code'],
			'WEB_Url'=>$formdata['url'],
			'WEB_Active'=>1,
			'WEB_Notes'=>$formdata['notes'],
			'WEB_Created'=>date(FULL_MILITARY_DATETIME)
		);
		
		
		$add_website = $this->db->insert('Websites',$data);
		
		if($add_website) {
			$web_id = $this->db->insert_id();
			$master_list = array(
				'WEB_ID'=>$web_id,
				'CLIENT_ID'=>$owner_id,
				'CMS_Vendor_ID'=>$formdata['vendor']
			);
			
			$crazyEgg = array(
				'WEB_ID'=>$web_id
			);
			
			$add_ce = $this->db->insert('WebsiteOptions',$crazyEgg);
			$add_ml = $this->db->insert('MasterList',$master_list);
			
			if($add_ml AND $add_ce) {
				return TRUE;	
			}
		}else {
			return FALSE;	
		}
		
	}
	
	public function updateWebsite($data,$web_id) {
		$this->db->where('WEB_ID',$web_id);
		return ($this->db->update('Websites',$data)) ? TRUE : FALSE;	
	}
}
