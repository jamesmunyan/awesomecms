<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Websites_model extends DOM_Model {
    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }
	
	//This is to build the listing tables
	function getWebsites($oid,$oType) {
		$query = $this->db->select('w.WEB_ID as ID, w.OWNER_ID as OwnerID,w.OWNER_Type as OwnerType, v.VENDOR_Name as VendorName')->from('Websites w')->join('Vendors v','w.WEB_Vendor = v.VENDOR_ID')->where('w.OWNER_ID',$oid)->where('w.OWNER_Type',$oType)->get();
		
		return ($query) ? $query->result() : FALSE;
	}
}
