<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
require_once (APPPATH . 'libraries/heatmap.php');
require_once (APPPATH . 'libraries/heatmapfromdatabase.php');
class Heat_map extends DOM_Controller {

	var $client_id;
	var $base_url;
	var $activeSiteURL;
	var $web_id;
	var $iFrame;
	var $websites;
	var $allSites;
	var $maxY;
	var $maxX;
	var $groupPaths;
	var $paths;

    public function __construct() {
        parent::__construct();
		
		//load the analytics model for easy querying.
		$this->load->model('domanalytics');
		$this->load->helper('analytics');
		
		//The analytic module is only viewable on the client level. 
		//If the level is anything other than the client level, redirect it back to the dashboard.
		//else keep on with the controller process.
		if($this->user['DropdownDefault']->LevelType != 'c' OR ((is_numeric($this->user['DropdownDefault']->LevelType) AND $this->user['DropdownDefault']->LevelType < 3))) {
			redirect('/','refresh');
		}
		
		//The selected client ID. The Client will always be selected at this level.
		$this->client_id = $this->user['DropdownDefault']->SelectedClient;
		
		//turns on the active subnavigation
		$this->activeNav='reports';
		
		//Load the heatmap queries
		$this->load->model('domheatmap');
		
		//tells us the id of the selected client from the dealer dropdown
		$this->client_id = $this->user['DropdownDefault']->SelectedClient;
		
		//empty array for websites
		$this->websites = array();
		
		//gives us all websites for the selected client
		$this->allSites = $this->domheatmap->getClientWebsites($this->user['DropdownDefault']->SelectedClient);
		
		//sense the selected website over the default website
		$this->selected_website = ((isset($_GET['site_id'])) ? $_GET['side_id'] : FALSE);
		
		//if the selected client has websites enabled.
		if(!empty($this->allSites)) :
		
			//check to see if the website has active heatmaping data.
			//if the site has data in the heatmapping table
			//if the site has heatmapping, we push the website to the collection array
			//else if the site does not have heatmapping data
			//skip the website from collection;
			foreach($this->allSites as $website) : 
				//check heatmapping table against the web_id
				$hasHeatMappingData = $this->domheatmap->checkForHeatmap($website->WEB_ID);
				if($hasHeatMappingData)
					array_push($this->websites,$website);
			endforeach;
			
			$group = ((isset($_GET['p'])) ? $_GET['p'] : '/');
		
			if(!empty($this->websites)) :
				$this->activeSiteURL = (($this->selected_website) ? $this->domheatmap->getSingleSiteURL($this->selected_website) : $this->websites[0]->WEB_Url);
				$this->web_id = (($this->selected_website) ? $this->selected_website : $this->websites[0]->WEB_ID);
				$this->iFrame = $this->activeSiteURL;
				$this->MaxY = $this->domheatmap->getMaxY($this->web_id,$group);
				$this->MaxX = $this->domheatmap->getMaxX($this->web_id,$group);
				$this->groupPaths = $this->domheatmap->getGroupPaths($this->web_id);
			endif;
			
		endif;
		//print_object($this->groupPaths);
		$this->load->helper('analytics');
    }
	
	public function Index() {
		$data = array(
			'maxY' => (($this->MaxY) ? $this->MaxY : 768),
			'maxX' => $this->MaxX,
			'iFrame' => $this->iFrame,
			'web_id'=>$this->web_id,
			'activeSiteURL'=>$this->activeSiteURL,
			'websites'=>$this->websites,
			'groups'=>$this->groupPaths
		);
		$this->LoadTemplate('pages/reports/heatmap',$data);
	}
	
	public function generate() {
		$width = $_GET['w'];
		$height = $this->MaxY;
		$p = ((isset($_GET['p'])) ? $_GET['p'] : '/');
		$this->web_id = $_GET['web_id'];
		$iframe = $this->_iframe($p,$width,$height);
		$imgs = $this->_image($p,$width,$height);
		
		echo $iframe . $imgs;
	}
	
	private function _iframe($page,$width,$height) {
		return '<iframe src="' . $this->activeSiteURL . $page . '" width="' . $width . '" height="' . $height . '" scrolling="no"></iframe>';	
	}
	
	private function _image($page,$width,$height) {
		$heatmap = new HeatmapFromDatabase($this->activeSiteURL,$this->web_id,$page);
		
		/** Temporary files */
		$heatmap->cache = FCPATH . 'cache';
		
		/** Generated files */
		$heatmap->path = FCPATH . 'logs';

		/** Final file */
		$heatmap->file = $this->web_id . '_' . $this->base_url . '_' . '%d.png';
		
		/** Forcing final height (take care of the memory consumption in such case!) **/
		$images = $heatmap->generate($width, $height);
		$imgs = '';
		for ($i = 0; $i < $images['count']; $i++) {
			$imgs .= '<img src="' . base_url() . 'logs/' . $images['filenames'][$i] . '" width="' . $images['width'] . '" height="' . $images['height'] . '" alt="" style="position:absolute;top:0;left:0;opacity:0.7" />';
		}
		
		return $imgs;
	}
}