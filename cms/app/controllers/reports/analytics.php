<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Analytics extends DOM_Controller {

	public $client_id;
	public $web_id;
	public $websites;
	
    public function __construct() {
        parent::__construct();
		
		//load the analytics model for easy querying.
		$this->load->model('domanalytics');
		$this->load->helper('analytics');
		
		//The analytic module is only viewable on the client level. 
		//If the level is anything other than the client level, redirect it back to the dashboard.
		//else keep on with the controller process.
		if($this->user['DropdownDefault']->LevelType != 'c' OR ((is_numeric($this->user['DropdownDefault']->LevelType) AND $this->user['DropdownDefault']->LevelType < 3))) {
			redirect('/','refresh');
		}
		
		//The selected client ID. The Client will always be selected at this level.
		$this->client_id = $this->user['DropdownDefault']->SelectedClient;
		
		if(isset($_GET['web_id'])) {
			//need to check if this web id is in the selected clients list of websites.
			$check_web_id = $this->domanalytics->checkWebID($_GET['web_id'], $this->client_id);
			if(!$check_web_id) {
				//the website doesnt belog to this client
				$this->web_id = $this->domanalytics->getClientFirstWebsite($this->client_id);
				
			}else {
				$this->web_id = $_GET['web_id'];	
			}
		}else {
			$this->web_id = $this->domanalytics->getClientFirstWebsite($this->client_id);	
		}
		
		$this->websites = $this->domanalytics->getAllWebsites($this->web_id);
		
		
		if($this->websites->AnalyticsProfileID == 0) {
			redirect('/','refresh');	
			throwError(newError("Analytics data", 1, 'An error has occured with fetching data from Google Analytics. Please contact support.', 1, ''));
		}
		
		$this->activeNav='reports';
    }
	
	public function No_data() {
		$data = array(
			'websites' => $this->domanalytics->getAllWebsites(),
			'web_id' => $this->web_id,
		);
		echo 'dude';
		//$this->LoadTemplate('pages/reports/no_data',$data);	
	}
	
	public function Index() {
		if(isset($_GET['web_id'])) { 
			$websites = $this->domanalytics->getAllWebsites($this->web_id);
		}else {
			$websites = $this->domanalytics->getClientWebsite(false);
		}
		//grab all websites for the selected client
		$webAndReports = $this->getSingleReportData($websites);
		$data = array(
			'websites' => $this->domanalytics->getAllWebsites(),
			'report' => $webAndReports,
			'web_id' => ((isset($_GET['web_id'])) ? $_GET['web_id'] : $webAndReports->WebID),
			'id'=>$webAndReports->AnalyticsProfileID
		);
		$this->LoadTemplate('pages/reports/analytics_dashboard',$data);	
	}
	
	private function getReportData($web) {
		$reports = array();
		foreach($web as $website) {
			$website->Reports = $this->domanalytics->getDefaultReports($website->AnalyticsProfileID);
			array_push($reports,$website);
		}
		return $reports;
	}
	
	private function getSingleReportData($website) {
		$website->Reports = $this->domanalytics->getDefaultReports($website->AnalyticsProfileID);
		return $website;
	}
	
	public function query_by_website() {
		$website = $this->domanalytics->getClientWebsites($this->user['DropdownDefault']->SelectedClient);
		$mySite = array();
		foreach($website as $website) {
			if($website->WebID == $this->web_id) {
				array_push($mySite,$website);
			}
		}
		$webAndReports = $this->getSingleReportData($website);
		$data = array(
			'report' => $webAndReports
		);	
		
		$this->load->dom_view('pages/reports/analytic_query', $this->theme_settings['ThemeViews'], $data);
	}
		
	public function Visitors() {
		if(isset($_GET['web_id'])) { 
			$websites = $this->domanalytics->getAllWebsites($this->web_id);
		}
		
		if(isset($_GET['r'])) { 
			$report = $_GET['r'];
		}
		
		$data = array(
			'websites' => $this->domanalytics->getAllWebsites(),
			'website'=> $websites,
			'web_id' => $this->web_id,
			'id'=>$websites->AnalyticsProfileID,
			'pageTitle' => 'Visitors',
			'tableTitle'=>'',
			'type'=>'Visitors: Total number of visits to your site including repeats, number of unduplicated unique visitors, and percentage of new first-time visitors'
		);
	
		$this->LoadTemplate('pages/reports/default_reports/visitors',$data);
	}
	
	public function Traffic_sources() {
		if(isset($_GET['web_id'])) { 
			$websites = $this->domanalytics->getAllWebsites($this->web_id);
		}
		
		if(isset($_GET['r'])) { 
			$report = $_GET['r'];
		}
		
		$data = array(
			'websites' => $this->domanalytics->getAllWebsites(),
			'website'=> $websites,
			'web_id' => $this->web_id,
			'id'=>$websites->AnalyticsProfileID,
			'pageTitle' => 'Traffic Sources',
			'tableTitle'=>'',
			'type'=>''
		);
	
	$this->LoadTemplate('pages/reports/default_reports/traffic_sources',$data);
	}
	
	public function Bounce_rate() {
		if(isset($_GET['web_id'])) { 
			$websites = $this->domanalytics->getAllWebsites($this->web_id);
		}
		
		if(isset($_GET['r'])) { 
			$report = $_GET['r'];
		}
		
		$data = array(
			'websites' => $this->domanalytics->getAllWebsites(),
			'website'=> $websites,
			'web_id' => $this->web_id,
			'id'=>$websites->AnalyticsProfileID,
			'pageTitle' => 'Bounce Rate',
			'tableTitle'=>'',
			'type'=>''
		);
	
	$this->LoadTemplate('pages/reports/default_reports/bounce_rate',$data);
	}
	
	public function Page_views() {
		if(isset($_GET['web_id'])) { 
			$websites = $this->domanalytics->getAllWebsites($this->web_id);
		}
		
		if(isset($_GET['r'])) { 
			$report = $_GET['r'];
		}
		
		$data = array(
			'websites' => $this->domanalytics->getAllWebsites(),
			'website'=> $websites,
			'web_id' => $this->web_id,
			'id'=>$websites->AnalyticsProfileID,
			'pageTitle' => 'Page Views',
			'tableTitle'=>'',
			'type'=>''
		);
	
	$this->LoadTemplate('pages/reports/default_reports/page_views',$data);
	}
	
	public function Average_time_on_page() {
		if(isset($_GET['web_id'])) { 
			$websites = $this->domanalytics->getAllWebsites($this->web_id);
		}
		
		if(isset($_GET['r'])) { 
			$report = $_GET['r'];
		}
		
		$data = array(
			'websites' => $this->domanalytics->getAllWebsites(),
			'website'=> $websites,
			'web_id' => $this->web_id,
			'id'=>$websites->AnalyticsProfileID,
			'pageTitle' => 'Average Time On Page',
			'tableTitle'=>'',
			'type'=>''
		);
	
	$this->LoadTemplate('pages/reports/default_reports/average_time_on_page',$data);
	}
	
	public function Entry_pages() {
		if(isset($_GET['web_id'])) { 
			$websites = $this->domanalytics->getAllWebsites($this->web_id);
		}
		
		if(isset($_GET['r'])) { 
			$report = $_GET['r'];
		}
		
		$data = array(
			'websites' => $this->domanalytics->getAllWebsites(),
			'website'=> $websites,
			'web_id' => $this->web_id,
			'id'=>$websites->AnalyticsProfileID,
			'pageTitle' => 'Entry Pages',
			'tableTitle'=>'',
			'type'=>''
		);
	
	$this->LoadTemplate('pages/reports/default_reports/entry_pages',$data);
	}
	
	public function Exit_pages() {
		if(isset($_GET['web_id'])) { 
			$websites = $this->domanalytics->getAllWebsites($this->web_id);
		}
		
		if(isset($_GET['r'])) { 
			$report = $_GET['r'];
		}
		
		$data = array(
			'websites' => $this->domanalytics->getAllWebsites(),
			'website'=> $websites,
			'web_id' => $this->web_id,
			'id'=>$websites->AnalyticsProfileID,
			'pageTitle' => 'Exit Pages',
			'tableTitle'=>'',
			'type'=>''
		);
	
	$this->LoadTemplate('pages/reports/default_reports/exit_pages',$data);
	}
	
	public function Browser() {
		if(isset($_GET['web_id'])) { 
			$websites = $this->domanalytics->getAllWebsites($this->web_id);
		}
		
		if(isset($_GET['r'])) { 
			$report = $_GET['r'];
		}
		
		$data = array(
			'websites' => $this->domanalytics->getAllWebsites(),
			'website'=> $websites,
			'web_id' => $this->web_id,
			'id'=>$websites->AnalyticsProfileID,
			'pageTitle' => 'browser',
			'tableTitle'=>'',
			'type'=>''
		);
	
	$this->LoadTemplate('pages/reports/default_reports/browser',$data);
	}
	
	public function Operating_system() {
		if(isset($_GET['web_id'])) { 
			$websites = $this->domanalytics->getAllWebsites($this->web_id);
		}
		
		if(isset($_GET['r'])) { 
			$report = $_GET['r'];
		}
		
		$data = array(
			'websites' => $this->domanalytics->getAllWebsites(),
			'website'=> $websites,
			'web_id' => $this->web_id,
			'id'=>$websites->AnalyticsProfileID,
			'pageTitle' => 'Operating System',
			'tableTitle'=>'',
			'type'=>''
		);
	
	$this->LoadTemplate('pages/reports/default_reports/operating_system',$data);
	}
	
	public function Mobile_devices() {
		if(isset($_GET['web_id'])) { 
			$websites = $this->domanalytics->getAllWebsites($this->web_id);
		}
		
		if(isset($_GET['r'])) { 
			$report = $_GET['r'];
		}
		
		$data = array(
			'websites' => $this->domanalytics->getAllWebsites(),
			'website'=> $websites,
			'web_id' => $this->web_id,
			'id'=>$websites->AnalyticsProfileID,
			'pageTitle' => 'Operating System',
			'tableTitle'=>'',
			'type'=>''
		);
	
	$this->LoadTemplate('pages/reports/default_reports/mobile_devices',$data);
	}
}
