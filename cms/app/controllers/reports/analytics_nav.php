        <div class="hor_nav nav">
            <ul>
                <li><a href="javascript:void(0);">Visitors</a>
                	<ul class="fallback">
                    	<li><a href="<?= base_url(); ?>reports/analytics/new_visitors?id=<?= $id; ?>">New Visitors</a></li>
                        <li><a href="<?= base_url(); ?>reports/analytics/unique_visitors?id=<?= $id; ?>">Unique Visitors</a></li>
                    </ul>
                </li>
                <li><a href="javascript:void(0);">Pages</a>
                        <ul class="fallback">
                            <li><a href="<?= base_url(); ?>reports/analytics/page_views?id=<?= $id; ?>">Page Views</a></li>
                            <li><a href="<?= base_url(); ?>reports/analytics/unique_page_views?id=<?= $id; ?>">Unique Page Views</a></li>
                            <li><a href="<?= base_url(); ?>reports/analytics/unique_page_views?id=<?= $id; ?>">Average Time On Page</a></li>
                            <li><a href="<?= base_url(); ?>reports/analytics/video_engagement?id=<?= $id; ?>">Video Engagement</a></li>
                            <li><a href="<?= base_url(); ?>reports/analytics/entry_pages?id=<?= $id; ?>">Entry Pages</a></li>
                            <li><a href="<?= base_url(); ?>reports/analytics/exit_pages?id=<?= $id; ?>">Exit Pages</a></li>
                        </ul>
                </li>
                <li><a href="#">Platform/Device</a></li>
                <li><a href="#">Custom Reports</a>
                        <ul class="fallback">
                            <li><a href="#">View Existing</a></li>
                            <li><a href="#">Create Custom Report</a></li>
                        </ul>
                </li>
            </ul>
            <div class="fix"></div>
        </div>        
