<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends DOM_Controller {

	public $client_id;

    public function __construct() {
        parent::__construct();
		
		//load the analytics model for easy querying.
		$this->load->model('domanalytics');
		$this->load->helper('analytics');
		
		//The analytic module is only viewable on the client level. 
		//If the level is anything other than the client level, redirect it back to the dashboard.
		//else keep on with the controller process.
		if($this->user['DropdownDefault']->LevelType != 'c' OR ((is_numeric($this->user['DropdownDefault']->LevelType) AND $this->user['DropdownDefault']->LevelType < 3))) {
			redirect('/','refresh');
		}
		
		//The selected client ID. The Client will always be selected at this level.
		$this->client_id = $this->user['DropdownDefault']->SelectedClient;
		
		$this->activeNav='reports';
    }
	
	public function Index() {
		//grab all websites for the selected client
		$websites = $this->domanalytics->getClientWebsites($this->user['DropdownDefault']->SelectedClient);
		$webAndReports = $this->getReportData($websites);
		$data = array(
			'defaultReports' => $this->getDefaultReports(),
			'reports' => $webAndReports
		);
		$this->LoadTemplate('pages/reports/admin',$data);	
	}
	private function getReportData($web) {
		$reports = array();
		foreach($web as $website) {
			$website->DefaultReports = $this->domanalytics->getDefaultReports($website->AnalyticsProfileID);
			array_push($reports,$website);
		}
		return $reports;
	}
	
	private function getDefaultReports() {
		$reports = $this->domanalytics->getDefaultReports();
		return $reports;	
	}
}
