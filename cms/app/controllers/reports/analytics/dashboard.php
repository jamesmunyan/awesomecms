<?php 

/*
 | GOOGLE ANALYTICS REPORTING DASHBOARD
 | PULLS CLIENT DATA IN FROM GOOGLE ANALYTICS 
 | main account: webmaster@dealeronlinemarketing.com
 | FILE CREATED 5/28/13
 */
 
//BLOCK DIRECT ACCESS TO SCRIPT
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends DOM_Controller {
	
	public $client_id;
	
	public function __construct() {
		parent::__construct();
	}
	
	public function index() {
		print_object($this->api_access);
	}
}