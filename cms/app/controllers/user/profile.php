<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends DOM_Controller {

	public $user_id;

    public function __construct() {
        parent::__construct();
		$this->load->helper(array('form','url','pass','file','template'));
		$this->load->model(array('members','administration'));
		$this->user_id = ((isset($_GET['uid'])) ? $_GET['uid'] : FALSE);
				$this->activeNav = 'admin';
    }

    public function index() {
		$this->View();
    }

	public function View($msg = false) {
		$this->load->model('domwebsites');
		if($this->input->post('user_id')) {
			$user_id = $this->input->post('user_id');
		}else {
			$user_id = $this->user['UserID'];
		}
		
		$user = $this->administration->getMyUser($user_id);
		$user->Modules = ParseModulesInReadableArray($user->Modules);
		$avatar = $this->members->get_user_avatar($user->ID);
		
		$data = array(
			'user'=>$user,
			'avatar'=>$avatar,
			'allMods'=>$this->administration->getAllModules(),
			'websites'=>$this->domwebsites->getUserWebsites($user->ID),
			'edit'=>(($user->ID == $this->user['UserID'] || $this->user['AccessLevel'] >= 600000) ? TRUE : FALSE),
			//'contact'=>$user,
			//'contactInfo'=>$this->syscontacts->getUserContactInfo($uid),
		);

		$this->LoadTemplate('pages/users/profile',$data);
	}
	
	public function edit_user_details() {
		$user = $this->administration->getMyUser($this->user_id);
		$data = array(
			'user'=>$user
		);
		$this->load->dom_view('forms/userProfile/edit', $this->theme_settings['ThemeViews'], $data);
	}
	
	public function Upload_avatar() {
		$user = $this->administration->getMyUser($this->input->post('user_id'));
		$profile_url = base_url() . 'profile/' . strtolower($user->FirstName . $user->LastName);
		//print_object($this->input->post());
		$rootpath = $_SERVER['DOCUMENT_ROOT'];
		
		$config['upload_path'] = $rootpath . '/uploads/avatars/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = '1000';
		$config['max_width'] = '640';
		$config['max_height'] = '480';
		$config['file_name'] = $this->user['UserID'] . '_' . strtolower($user->FirstName) . '_' . strtolower($user->LastName) . "_" . createRandomString(30,"ALPHANUM") . '.jpg';
		
		$this->load->library('upload',$config);
		$avatar = 'custom_avatar';
		if(!$this->upload->do_upload($avatar)) {
			$error = $this->upload->display_errors();
			redirect(LASTVISITEDPAGE);
		}else {
			$data = array('upload_data' => $this->upload->data());
			$filename = $data['upload_data']['file_name'];
			// the current logged in users profile url
			//log the url to the database
			$updateAvatar = $this->members->avatar_update($user->ID,$filename);
			// if the query was successfull, redirect back to the profile.
			if($updateAvatar) :
				redirect(LASTVISITEDPAGE);
			else :
				redirect(LASTVISITEDPAGE);
			endif;
			
		}
	}
	
	public function Reset_password() {
		$this->load->model('members');
		$this->load->helper('pass');
		$new_pass = createRandomString(10,'ALPHANUMSYM');
		$email = $this->input->post('userEmail');
		
		//echo $new_pass;
		if($new_pass) {
			$reset_password = $this->members->manual_reset_pass($email,$new_pass);
			if($reset_password) {
				echo $new_pass;
			} else {
				echo '0';
			}
		}else {
			echo '0';
		}
	}
	
	public function Edit() {
		if(isset($_GET['UID'])) {
			$uid = $_GET['UID'];
		}
		
		$user = $this->administration->getMyUser($uid);
		$user->ContactID = $user->DirectoryID;
		$user->Address = mod_parser($user->Address);
		$user->CompanyAddress = mod_parser($user->CompanyAddress);
		$user->Email = mod_parser($user->Emails,false,true);
		$user->Phone = mod_parser($user->Phones,false,true);
		$user->Modules = ParseModulesInReadableArray($user->Modules);
		$avatar = $this->members->get_user_avatar($user->ID);
		$data = array(
			'user'=>$user,
			'avatar'=>$avatar,
			'allMods'=>$this->administration->getAllModules(),
			'websites'=>WebsiteListingTable($uid, 'UID'),
			'contact'=>$user,
			'contactInfo'=>ContactInfoListingTable($user, 'UID', true),
		);
		$this->load->dom_view('forms/userProfile/edit_add_view', $this->theme_settings['ThemeViews'],$data);
	}
	
	public function load_custom_avatar_form() {
		$data = array(
			'uid'=>$_GET['uid']
		);	
		$this->load->dom_view('forms/users/edit_avatar', $this->theme_settings['ThemeViews'],$data);
	}
}
