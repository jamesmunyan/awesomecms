<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Contacts extends DOM_Controller {

	public $agency_id;
	public $contact_id;
	public $directory_id;

    public function __construct() {
        parent::__construct();
		$this->load->model('administration');
		$this->load->model('system_contacts');
		$this->load->helper('websites');

        $this->agency_id = $this->user['DropdownDefault']->SelectedAgency;
		$this->group_id = $this->user['DropdownDefault']->SelectedGroup;
		$this->client_id = $this->user['DropdownDefault']->SelectedClient;
        $this->level     = $this->user['DropdownDefault']->LevelType;
		$this->activeNav = 'admin';
		
		$this->contact_id = ((isset($_GET['did'])) ? $_GET['did'] : FALSE);
		$this->directory_id = ((isset($_GET['did'])) ? $_GET['did'] : FALSE);
    }
	
    public function index() {
		$this->load->helper('contacts');
		$this->LoadTemplate('pages/contacts/listing');
    }
	
	public function load_table() {
		$this->load->helper('contacts');
		$this->load->dom_view('pages/contacts/table', $this->theme_settings['ThemeViews']);
	}
	
	public function View() {
		$this->load->model('system_contacts','domcontacts');
		$contact = $this->domcontacts->getDirectoryInfoForEmail($this->directory_id);
		$this->load->model('system_contacts','domcontacts');
		$this->load->helper('contactinfo');
		$data = array(
			'clients'=>$this->administration->getAllClientsInAgency($this->user['DropdownDefault']->SelectedAgency),
			'vendors'=>$this->administration->getVendors(),
			'contact'=>$this->domcontacts->preparePopupInfo($contact->DIRECTORY_ID,$contact->DIRECTORY_Type,$contact->OWNER_ID),
			'jobtitles'=>$this->domcontacts->getJobTitles()
		);
		$this->load->dom_view('forms/contacts/view',$this->theme_settings['ThemeViews'],$data);
	}
	
	private function _getClientsByDropdownLevel() {
		$level = $this->user['DropdownDefault']->LevelType;	
		switch($level) {
			case 1  : return $this->administration->getAllClientsInAgency($this->user['DropdownDefault']->SelectedAgency); break;
			case 2  : return $this->administration->getAllClientsInGroup($this->user['DropdownDefault']->SelectedGroup);   break;
			case 3  : return $this->administration->getClientByID($this->user['DropdownDefault']->SelectedClient);         break;
			case 'a': return $this->administration->getAllClientsInAgency($this->user['DropdownDefault']->SelectedAgency); break;
			case 'g': return $this->administration->getAllClientsInGroup($this->user['DropdownDefault']->SelectedGroup);   break;
			case 'c': return $this->administration->getClientByID($this->user['DropdownDefault']->SelectedClient);         break;	
			default : return $this->administration->getAllClientsInAgency($this->user['DropdownDefault']->SelectedAgency); break;
		}
	}
	
	private function _getGroupsByDropdownLevel() {
		$level = $this->user['DropdownDefault']->LevelType;
		switch($level) {
			case 'a' : return $this->administration->getAllGroupsInAgency($this->user['DropdownDefault']->SelectedAgency);       break;
			default  : return $this->administration->getAllGroupsInAgency(false,$this->user['DropdownDefault']->SelectedGroup);  break;	
		}
	}
	
	public function Add() {
		$this->load->model('system_contacts','domcontacts');
		$preset = array();
		
		if(isset($_GET['oid'])) {
			$owner_id = $_GET['oid'];	
		}
		
		if(isset($_GET['otype'])) {
			$owner_type = $_GET['otype'];	
		}
		
		$data = array(
			'owner_type'    => ((isset($owner_type)) ? $owner_type : FALSE),
			'owner_label'   => ((isset($owner_type)) ? $this->administration->getOwnerTypeName($owner_type) : FALSE),
			'clientContact' => ((isset($owner_type) AND $owner_type == '1') ? TRUE : FALSE),
			'vendorContact' => ((isset($owner_type) AND $owner_type == '2') ? TRUE : FALSE),
			'groupContact'  => ((isset($owner_type) AND $owner_type == '8') ? TRUE : FALSE),
			'agencyContact' => ((isset($owner_type) AND $owner_type == '7') ? TRUE : FALSE),
			'clients'       => ((isset($owner_type) AND $owner_type == '1') ? 
								$this->administration->getClientByID(((isset($owner_id)) ? $owner_id : $this->user['DropdownDefault']->SelectedClient)) : 
								$this->_getClientsByDropdownLevel()),
			'vendors'       => ((isset($owner_type) AND $owner_type == '2') ? 
								$this->administration->getVendors($_GET['oid']) : 
								$this->administration->getVendors(false,false)),
			'contactTypes'  => $this->administration->getContactTypes(),
			'types'         => $this->administration->getTypeList(),
			'jobtitles'     => $this->domcontacts->getJobTitles(),
			'groups'        => $this->_getGroupsByDropdownLevel(),
		);
				
		$this->load->dom_view('forms/contacts/edit_add', $this->theme_settings['ThemeViews'], $data);
	}
	
	public function Edit() {
		$this->load->model('system_contacts','domcontacts');
		$contact = $this->domcontacts->getDirectoryInfoForEmail($this->directory_id);
		$this->load->model('system_contacts','domcontacts');
		$this->load->helper('contactinfo');
		$data = array(
			'clients'=>$this->_getClientsByDropdownLevel(),
			'vendors'=>$this->administration->getVendors(),
			'contact'=>$this->domcontacts->preparePopupInfo($contact->DIRECTORY_ID,$contact->DIRECTORY_Type,$contact->OWNER_ID),
			'jobtitles'=>$this->domcontacts->getJobTitles()
		);
		$this->load->dom_view('forms/contacts/edit',$this->theme_settings['ThemeViews'],$data);
	}
		
	public function Process_add() {
		$this->load->model('system_contacts','domcontacts');
		
		//the post of the form vars
		$form = $this->input->post();
		
		if($form['owner_type'] == 2) {
			$owner_id = $form['vendor_id'];	
		}
		
		if($form['owner_type'] == 1) {
			$owner_id = $form['client_id'];	
		}
		
		if($form['owner_type'] == 8) {
			$owner_id = $form['group_id'];	
		}
		
		//prepare the directories table
		$directory_add = array(
			'TITLE_ID' 				=> $form['job_title'],
			'JobTitle' 				=> $this->domcontacts->getJobTitleText($form['job_title']),
			'DIRECTORY_Type' 		=> $form['owner_type'],
			'OWNER_ID' 				=> $owner_id,
			'DIRECTORY_FirstName' 	=> $form['firstname'],
			'DIRECTORY_LastName' 	=> $form['lastname'],
			'DIRECTORY_Notes' 		=> $form['notes'],
		    'DIRECTORY_Created' 	=> date('Y-m-d H:i:s'),
		);
		
		if($form['email'] != '') {
			$email_add = array(
				'OWNER_ID' 		=> $directory_add['OWNER_ID'],
				'OWNER_Type'	=> $form['owner_type'],
				'EMAIL_Address' => $form['email'],
				'EMAIL_Type'	=> $form['email_type'],
				'EMAIL_Primary' => 1,
				'EMAIL_Active' => 1,
				'EMAIL_Created'	=> date('Y-m-d H:i:s')
			);	
		}
		
		//prepare the phone table
		$phone_add = array(
			'OWNER_ID'				=> $directory_add['OWNER_ID'],
			'OWNER_Type'			=> $form['owner_type'],
			'PHONE_Number'			=> $form['phone'],
			'PHONE_Type'			=> $form['phone_type'],
			'PHONE_Primary'			=> 1,
			'PHONE_Active'			=> 1,
			'PHONE_Created'			=> date('Y-m-d H:i:s')
		);	
		
		//prepare the address table
		$address_add = array(
			'OWNER_ID'				=> $directory_add['OWNER_ID'],
			'OWNER_Type'			=> $form['owner_type'],
			'ADDRESS_Street'		=> $form['street'],
			'ADDRESS_City'			=> $form['city'],
			'ADDRESS_State'			=> $form['state'],
			'ADDRESS_Zip'			=> $form['zip'],
			'ADDRESS_Type'			=> 'work',
			'ADDRESS_Active'		=> 1,
			'ADDRESS_Primary'		=> 1,
			'ADDRESS_Created'		=> date('Y-m-d H:i:s')
		);
		
		if(isset($phone_add)) {
			$data['PhoneNumbers'] = $phone_add;
		}
		
		if(isset($address_ad)) {
			$data['DirectoryAddresses'] = $address_add;	
		}
		
		if(isset($directory_add)) {
			$data['Directories'] = $directory_add;	
		}
		
		if(isset($email_add)) {
			$data['EmailAddresses'] = $email_add;	
		}
		
		$add = $this->domcontacts->addContact($data);
		$checker = false;
		if(is_array($add)) {
			foreach($add as $item) {
				if(!$item) {
					$checker = false;
					break;
				}else {
					$checker = true;	
				}
			}
			if($checker) {
				echo '1';
			}else {
				echo '0';	
			}
		}else {
			if($add) {
				echo '1';	
			}else {
				echo '0';	
			}
		}
	}
	
	public function Process_edit() {
		$this->load->model('system_contacts','domcontacts');
		$form = $this->input->post();
		
		$address_id   = (($form['address_id'] != '-1') ? $form['address_id'] : $this->domcontacts->insertBlankAddress($form['owner_id'],$form['owner_type'],$form['directory_id']));
		$directory_id = $form['directory_id'];
		
		$address = array(
			'ADDRESS_Street' 	  => (($form['street'] != '') ? $form['street'] : '...'),
			'ADDRESS_City' 		  => (($form['city'] != '') ? $form['city'] : '...'),
			'ADDRESS_State' 	  => (($form['state'] != '') ? $form['state'] : ''),
			'ADDRESS_Zip' 		  => (($form['zip'] != '') ? $form['zip'] : ''),
			'ADDRESS_Active' 	  => 1,
			'ADDRESS_Primary'	  => 1,
		);
			//$did,$owner_id,$type,$data
		$update_address = $this->domcontacts->managePrimaryPhysicalAddress($directory_id,$form['owner_id'],$form['owner_type'],$address);
		
		$directory_info = array(
			'DIRECTORY_FirstName' => (string)$form['firstname'],
			'DIRECTORY_LastName'  => (string)$form['lastname'],
			'DIRECTORY_Type'      => (int)$form['owner_type'],
			'OWNER_ID'            => (int)(($form['owner_type'] <= 2) ? (($form['owner_type'] == 1) ? $form['client_id'] : $form['vendor_id']) : $form['owner_id']),
			'TITLE_ID'            => (int)$form['job_title'],
			'JobTitle'            => (string)$this->domcontacts->getJobTitleText($form['job_title']),
			'DIRECTORY_Notes'     => (string)$form['notes'],
			'DIRECTORY_Tag'			=> (($form['owner_type'] <= 2) ? $this->administration->getOwnerTagID($form['owner_type'],(($form['owner_type'] == 1) ? $form['client_id'] : $form['vendor_id'])) : 5),
		);
		
		$updateInfo 	= $this->domcontacts->updateDirectoryInformation($directory_id,$directory_info);
		
		//echo 'Did the address update? ' . (($update_address) ? 'Yes!' : 'No :(') . ' Did the Directory Information update? ' . (($updateInfo) ? 'Yes!' : ' No :(');		
		if((isset($update_address) AND $update_address)  AND $updateInfo) {
			echo '1';	
		}else {
			echo '0';	
		}
	}
	
}
