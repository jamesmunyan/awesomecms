<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Clients extends DOM_Controller {

	public $agency_id;
	public $group_id;
	public $client_id;

    public function __construct() {
        parent::__construct();
        $this->load->helper('form');
        $this->load->helper('formwriter');
        $this->load->library('table');
        $this->load->helper('html');
        $this->load->helper('msg');
        $this->load->helper('template');
        $this->load->helper('string_parser');
		$this->load->model('administration');
		$this->load->helper('websites');

        $this->agency_id = $this->user['DropdownDefault']->SelectedAgency;
        $this->group_id = $this->user['DropdownDefault']->SelectedGroup;
		$this->activeNav = 'admin';
		
		if(isset($_GET['cid'])) {
			$this->client_id = $_GET['cid'];
		}
    }
	
	private function _getGroupsByDropdownLevel() {
		$level = $this->user['DropdownDefault']->LevelType;
		switch($level) {
			case 'a' : return $this->administration->getAllGroupsInAgency($this->user['DropdownDefault']->SelectedAgency);       break;
			default  : return $this->administration->getAllGroupsInAgency(false,$this->user['DropdownDefault']->SelectedGroup);  break;	
		}
	}
	
	public function Edit_contact() {
		$this->load->model('system_contacts','domcontacts');
		$contact = $this->domcontacts->getDirectoryInfoForEmail($_GET['did']);
		$this->load->model('system_contacts','domcontacts');
		$this->load->helper('contactinfo');
		
		$preset = array();
		
		//print_object($contact);
		
		$owner_id = $contact->OWNER_ID;	
		$owner_type = $contact->DIRECTORY_Type;	
		
		$data = array(
			'owner_type'    => ((isset($owner_type)) ? $owner_type : FALSE),
			'owner_label'   => ((isset($owner_type)) ? $this->administration->getOwnerTypeName($owner_type) : FALSE),
			'clientContact' => ((isset($owner_type) AND $owner_type == '1') ? TRUE : FALSE),
			'vendorContact' => ((isset($owner_type) AND $owner_type == '2') ? TRUE : FALSE),
			'groupContact'  => ((isset($owner_type) AND $owner_type == '8') ? TRUE : FALSE),
			'agencyContact' => ((isset($owner_type) AND $owner_type == '7') ? TRUE : FALSE),
			'clients'	    => $this->administration->getAllClientsInAgency($this->user['DropdownDefault']->SelectedAgency),
			'vendors'		=> $this->administration->getVendors(),
			'contact'		=> $this->domcontacts->preparePopupInfo($contact->DIRECTORY_ID,$contact->DIRECTORY_Type,$contact->OWNER_ID),
			'jobtitles'		=> $this->domcontacts->getJobTitles(),
			'groups'        => $this->_getGroupsByDropdownLevel(),

		);
		$this->load->dom_view('forms/contacts/edit',$this->theme_settings['ThemeViews'],$data);
	}
	
	public function View_contact() {
		$this->load->model('system_contacts','domcontacts');
		$contact = $this->domcontacts->getDirectoryInfoForEmail($_GET['did']);
		$this->load->model('system_contacts','domcontacts');
		$this->load->helper('contactinfo');
		$data = array(
			'clients'=>$this->administration->getAllClientsInAgency($this->user['DropdownDefault']->SelectedAgency),
			'vendors'=>$this->administration->getVendors(),
			'contact'=>$this->domcontacts->preparePopupInfo($contact->DIRECTORY_ID,$contact->DIRECTORY_Type,$contact->OWNER_ID),
			'jobtitles'=>$this->domcontacts->getJobTitles()
		);
		$this->load->dom_view('forms/contacts/view',$this->theme_settings['ThemeViews'],$data);
	}
	
	public function selectOptionsForAssociationFilter() {
		$this->load->model('system_contacts','domcontacts');
		if(!isset($_GET['vid'])) {
			$vendorOptions = $this->domcontacts->getVendorFilterOptions(0);	
			echo $vendorOptions;
		}else {
			$vendorOptions = $this->domcontacts->getVendorFilterOptions($_GET['vid']);	
			echo $vendorOptions;
		}
	}
	
	public function Associate_vendors_form() {
		$this->load->model('system_contacts','domcontacts');
		$vendors = array();
		$vendorContacts = $this->domcontacts->getAllVendorContacts();
		$clientContacts = $this->domcontacts->getMyClientContacts($this->client_id);
		
		$sortedContacts = array();
		$systemContacts = array();
		$push = false;
		foreach($vendorContacts as $stock) {
			if(!empty($clientContacts)) {
				foreach($clientContacts as $myContacts) {
					if($stock->ContactID == $myContacts->ContactID) {
						array_push($sortedContacts,$myContacts);	
						$push = false;
					}else {
						$push = true;
					}
				}
			}
		}
		
		if(!empty($vendorContacts)) {
			foreach($vendorContacts as $elementKey => $element) {
				if(!empty($sortedContacts)) {
					foreach($sortedContacts as $valueKey => $value) {
						if(isset($vendorContacts[$elementKey]) AND isset($sortedContacts[$valueKey])) {
							if($vendorContacts[$elementKey]->ContactID == $sortedContacts[$valueKey]->ContactID) {
								unset($vendorContacts[$elementKey]);	
							}
						}
					}
				}
			}
		}
		
		$data = array(
			'vendors'=>$this->domcontacts->getAllVendorsForFilter(),
			'allVendorContacts'=> $vendorContacts,
			'clientContacts'=>$sortedContacts,
			'cid'=>$this->client_id
		);
		$this->load->dom_view('forms/clients/vendor_associations',$this->theme_settings['ThemeViews'],$data);
	}
	
	public function associateVendorsToClient() {
		$this->load->model('system_contacts','domcontacts');
		$form = $this->input->post();
		
		//so we know what client were working with
		$this->client_id = $form['client_id'];
		
		$loadedVendorContacts = ((!empty($form['loadedVendorContacts'])) ? $form['loadedVendorContacts'] : FALSE);
		
		$insertBatch = array();
		
		if(!empty($form['selectedVendorContacts'])) {
			foreach($form['selectedVendorContacts'] as $selection) {
				$selectedContacts = array(
					'CLIENT_ID'=>$this->client_id,
					'DIRECTORY_ID'=>$selection,
					'VENDOR_ID'=>$this->domcontacts->getVendorOwnerFromDirectories($selection)
				);
				
				array_push($insertBatch,$selectedContacts);
			}
		}		
		
		
		if(!empty($loadedVendorContacts)) {
			//we remove all the vendor associations and just add the ones submitted.
			$removeAllLoaded = $this->domcontacts->removeAllClientVendorContacts($this->client_id,$loadedVendorContacts);
		}
		
		if(!empty($insertBatch)) {
			//add the ones from the right side box back in.
			$addVendorAss = $this->domcontacts->addClientVendorAssociations($insertBatch);
		}
		
		if(isset($addVendorAss)) {
			if($addVendorAss) {
				echo '1';	
			}else {
				echo '0';	
			}
		}else {
			if(isset($removeAllLoaded)) {
				if($removeAllLoaded) {
					echo '1';
				}else {
					echo '0';	
				}
			}else {
				echo '1';	
			}
		}
	}
	
	public function changeClientStatus() {
		$cid = $_POST['cid'];
		$status = $_POST['status'];
		
		$change = $this->administration->updateClientStatus($cid,$status);
		if($change) {
			echo '1';	
		}else {
			echo '0';
				
		}
	}
	
	public function load_table($return = false) {
		$this->load->helper('template');
		$html = '';
		$level = $this->user['DropdownDefault']->LevelType;
		$clients = $this->_getClientsByDropdownLevel($level);		
		
		$data = array(
			'clients'=>$clients
		);
		
		$this->load->dom_view('pages/clients/table', $this->theme_settings['ThemeViews'], $data);
	}
	
	private function _getClientsByDropdownLevel($level = false) {
		switch($level) {
			case 1:
				return $this->administration->getAllClientsInAgency($this->agency_id);
			break;
			case 2:
				return $this->administration->getAllClientsInGroup($this->group_id);
			break;
			case 3:
				return $this->administration->getClientByID($this->user['DropdownDefault']->SelectedClient);
			break;
			case 'a':
				return $this->administration->getAllClientsInAgency($this->agency_id);
			break;
			case 'g':
				return $this->administration->getAllClientsInGroup($this->group_id);
			break;
			case 'c':
				return $this->administration->getClientByID($this->user['DropdownDefault']->SelectedClient);
			break;	
			default:
				return $this->administration->getAllClientsInAgency($this->agency_id);
			break;
		}
	}

    public function index() {
    	$this->load->helper('template');
		$level = $this->user['DropdownDefault']->LevelType;
		$clients = $this->_getClientsByDropdownLevel($level);		

		$data = array(
			'clients'=>$clients			
		);
		
		//LOAD THE TEMPLATE
		$this->LoadTemplate('pages/clients/listing', $data);
    }
	
	public function form() {
		$address_data = array(
			'ADDRESS_Street'=>$this->security->xss_clean($this->input->post('street')),
			'ADDRESS_State'=>$this->security->xss_clean($this->input->post('state')),
			'ADDRESS_City'=>$this->security->xss_clean($this->input->post('city')),
			'ADDRESS_Zip'=>$this->security->xss_clean($this->input->post('zip')),
			'ADDRESS_Type'=>'Work',
			'ADDRESS_Primary'=>1
		);
		
		$phone_data = array(
			'PHONE_Number'=>$this->security->xss_clean($this->input->post('phone')),
			'PHONE_Primary'=>1,
			'PHONE_Type'=>'Work'
		);
		
		$client_data = array(
			'CLIENT_Name'=>$this->security->xss_clean($this->input->post('ClientName')),
			'CLIENT_Notes'=>$this->security->xss_clean($this->input->post('Notes')),
			'CLIENT_Code'=>$this->security->xss_clean($this->input->post('ClientCode')),
			'CLIENT_Tag'=>$this->security->xss_clean($this->input->post('tags')),
			'GROUP_ID'=>$this->security->xss_clean($this->input->post('Group')),
		);		
		
		if(isset($_POST['ClientID'])) {
			$client_data['CLIENT_ID'] = $this->security->xss_clean($_POST['ClientID']);	
			
			if(isset($_POST['phone_id'])) {
				$phone_data['PHONE_ID'] = $this->security->xss_clean($this->input->post('phone_id'));
			}else {
				$phone_data['PHONE_Created'] = date(FULL_MILITARY_DATETIME);	
			}
			
			if(isset($_POST['address_id'])) {
				
				$address_data['ADDRESS_ID'] = $this->security->xss_clean($this->input->post('address_id'));
			}else {
				$address_data['ADDRESS_Created'] = date(FULL_MILITARY_DATETIME);	
			}
			
		}else {
			$client_data['CLIENT_Created'] = date(FULL_MILITARY_DATETIME);	
			$address_data['ADDRESS_Created'] = date(FULL_MILITARY_DATETIME);
			$phone_data['PHONE_Created'] = date(FULL_MILITARY_DATETIME);
		}
		
		$data = array(
			'DirectoryAddresses' => $address_data,
			'PhoneNumbers'=>$phone_data,
			'Clients'=>$client_data
		);
		
		if(isset($_POST['ClientID'])) {
			$update = $this->administration->updateClient($_POST['ClientID'],$data);
			if($update) {
				echo '1';	
			}else {
				echo '0';	
			}
		}else {
			$add = $this->administration->addClient($data);
			if($add) {
				echo '1';	
			}else {
				echo '0';	
			}
		}
	}
	
	public function Add() {
		$html = '';
		$tags = $this->administration->getAllTags();  
		$groups = $this->administration->getAllGroupsInAgency($this->user['DropdownDefault']->SelectedAgency);
		$data = array(
			'html' => $html,
			'tags' => $tags,
			'groups'=>$groups
		);

		$this->LoadTemplate('forms/clients/edit_add',$data);
	}
    
    public function add_form() {
		$tags = $this->administration->getAllTags();    
		$groups = $this->administration->getGroupsForDropdown($this->user['DropdownDefault']->SelectedAgency);

		$data = array(
			'client'=>false,
			'tags'=>$tags,
			'groups'=>$groups,
			'active_tab' => ((isset($_GET['active_tab'])) ? $_GET['active_tab'] : FALSE)
		);
		
		$this->load->dom_view('forms/clients/edit_add', $this->theme_settings['ThemeViews'], $data);
    }
	
	public function Edit() {
		$this->load->helper('contacts');
		$this->load->helper('template');
		$level = $this->user['DropdownDefault']->LevelType;
		
		$html = '';
	
		if(isset($_POST['client_id'])) {
			$client_id = $this->input->post('client_id');
		}elseif(isset($_GET['cid'])) {
			$client_id = $this->input->get('cid');
		}else {
			$client_id = $this->user['DropdownDefault']->SelectedClient;
		}
	
		 //WE POST WHAT AGENCY WERE EDITING, THIS IS THE ID IN THE DB.
		//$client_id = ($this->input->post('client_id'))?$this->input->post('client_id'):$this->user['DropdownDefault']->SelectedClient;
		$this->load->model('administration');
		$client = $this->administration->getSelectedClient($client_id);
		$client->ContactID = $client_id;
		$tags = $this->administration->getAllTags();  
		$groups = $this->administration->getAllGroupsInAgency($this->user['DropdownDefault']->SelectedAgency);
		
		if($client) {
			$client->Addresses = $this->administration->getSelectedClientAddress($client_id);
			$client->Phones = $this->administration->getSelectedClientPhones($client_id);
			
			/*
			$client->Reviews = array(
				'Google'   => ($this->administration->getSelectedClientsReviews($client_id,1)) ? $this->administration->getSelectedClientsReviews($client_id,1)->URL : FALSE,
				'GoogleID' => ($this->administration->getSelectedClientsReviews($client_id,1)) ? $this->administration->getSelectedClientsReviews($client_id,1)->ID  : FALSE,
				'Yelp'     => ($this->administration->getSelectedClientsReviews($client_id,2)) ? $this->administration->getSelectedClientsReviews($client_id,2)->URL : FALSE,
				'YelpID'   => ($this->administration->getSelectedClientsReviews($client_id,2)) ? $this->administration->getSelectedClientsReviews($client_id,2)->ID  : FALSE,
				'Yahoo'    => ($this->administration->getSelectedClientsReviews($client_id,3)) ? $this->administration->getSelectedClientsReviews($client_id,3)->URL : FALSE,
				'YahooID'  => ($this->administration->getSelectedClientsReviews($client_id,3)) ? $this->administration->getSelectedClientsReviews($client_id,3)->ID  : FALSE
			);
			*/
			
			$data = array(
				'groups' => $groups,
				'client' => $client,
				'client_id'=>$client->ClientID,
				'html' => $html,
				'tags'=>$tags,
				'websites'=>true,
				'active_tab' => ((isset($_GET['active_tab'])) ? $_GET['active_tab'] : FALSE),
			);
			
			//THIS IS THE DEFAULT VIEW FOR ANY BASIC FORM.
			$this->load->dom_view('forms/clients/edit_add', $this->theme_settings['ThemeViews'], $data);		
		}else {
			//this returns nothing to the ajax call....therefor the ajax call knows to show a popup error.
			print 0;
		}
	}
	
	
	public function View() {	
		$this->load->helper('contacts');		
		$this->load->helper('template');
		$level = $this->user['DropdownDefault']->LevelType;
		
		$html = '';
	
		if(isset($_POST['client_id'])) {
			$client_id = $this->input->post('client_id');
		}elseif(isset($_GET['cid'])) {
			$client_id = $this->input->get('cid');
		}else {
			$client_id = $this->user['DropdownDefault']->SelectedClient;
		}
	
		 //WE POST WHAT AGENCY WERE EDITING, THIS IS THE ID IN THE DB.
		//$client_id = ($this->input->post('client_id'))?$this->input->post('client_id'):$this->user['DropdownDefault']->SelectedClient;
		$this->load->model('administration');
		$client = $this->administration->getSelectedClient($client_id);
		$client->ContactID = $client_id;
		$tags = $this->administration->getAllTags();    
		$groups = $this->administration->getAllGroupsInAgency($this->user['DropdownDefault']->SelectedAgency);
		
		$client->TypeCode = 'CID';
		$client->TypeID = $client_id;
		
		if($client) {
			$client->Addresses = $this->administration->getSelectedClientAddress($client_id);
			$client->Phones = $this->administration->getSelectedClientPhones($client_id);
			
			/*
			$client->Reviews = array(
				'Google'   => ($this->administration->getSelectedClientsReviews($client_id,1)) ? $this->administration->getSelectedClientsReviews($client_id,1)->URL : FALSE,
				'GoogleID' => ($this->administration->getSelectedClientsReviews($client_id,1)) ? $this->administration->getSelectedClientsReviews($client_id,1)->ID  : FALSE,
				'Yelp'     => ($this->administration->getSelectedClientsReviews($client_id,2)) ? $this->administration->getSelectedClientsReviews($client_id,2)->URL : FALSE,
				'YelpID'   => ($this->administration->getSelectedClientsReviews($client_id,2)) ? $this->administration->getSelectedClientsReviews($client_id,2)->ID  : FALSE,
				'Yahoo'    => ($this->administration->getSelectedClientsReviews($client_id,3)) ? $this->administration->getSelectedClientsReviews($client_id,3)->URL : FALSE,
				'YahooID'  => ($this->administration->getSelectedClientsReviews($client_id,3)) ? $this->administration->getSelectedClientsReviews($client_id,3)->ID  : FALSE
			);
			
			*/
			$data = array(
				'view'=>true,
				'groups' => $groups,
				'client' => $client,
				'html' => $html,
				'tags'=>$tags,
				'websites'=>true,
				'contacts'=>true,
			);
			//
      		$this->load->dom_view('forms/clients/edit_add', $this->theme_settings['ThemeViews'], $data);
		}
	}

}
