<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Click extends CI_Controller {

	public $client_id;

    public function __construct() {
		parent::__construct();
		$this->load->model('domheatmap');
	}
	
	public function log_click() {
		//http://jeremy.com/clickheat/click?s=The%20Dev%20Ninjas&url=http://devninjas.net//&g=%2F&x=1701&y=358&w=1903&b=firefox&c=1&random=Mon%20Jun%2024%202013%2017:30:39%20GMT-0400%20%28Eastern%20Standard%20Time%29
		$s = $_GET['s'];
		$g = $_GET['g'];
		$rand = $_GET['random'];
		$x = $_GET['x'];
		$y = $_GET['y'];
		$url = substr($_GET['url'], 0, -1);
		$browser = $_GET['b'];
		$w = $_GET['w'];
		
		$base_url = $this->domheatmap->getBaseURLFromString(substr($url,0,-1));
		$base_url = $base_url['scheme'] . '://' . $base_url['host'];
		
		$data = array(
			'CLICK_X'=>$x,
			'CLICK_Y'=>$y,
			'CLICK_SiteName'=>$s,
			'CLICK_GroupName'=>$g,
			'CLICK_URL'=>$url,
			'WEB_ID'=> (($this->domheatmap->getWebID($base_url)) ? $this->domheatmap->getWebID($base_url)->WEB_ID : 0),
			'CLICK_Browser'=>$browser,
			'CLICK_Width'=>$w,
			'CLICK_Random'=>$rand
		);
		
		$log = $this->domheatmap->log_click($data);
		if($log) :
			echo '1';
		else :
			echo '0';
		endif;
		
		//print_object($data);
		
	}
	
}
